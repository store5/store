-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 18, 2020 at 10:14 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `f_correlativo` (IN `p_tabla` VARCHAR(50))  BEGIN

DECLARE v_numero varchar(50);

select numero+1 into v_numero from correlativo where tabla=p_tabla;

    IF v_numero IS NOT NULL THEN
        SELECT v_numero as numero;
    ELSE
        SELECT -1;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_inicio_sesion` (IN `p_email` VARCHAR(50), IN `p_password` CHAR(32))  BEGIN

DECLARE v_email varchar(50);

select email into v_email
from usuario
where password = p_password and email = p_email and estado = 'A';

    IF v_email IS NOT NULL THEN
        SELECT
                200 as estado,
               '' as mensaje,
                v_email as email;
    ELSE
        SELECT 500 as estado,'EROR... REVISE SU USUARIO Y/O CONTRASEÑA SEAN CORRECTA' as mensaje, '' as email;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_buscar_compra` (IN `p_nombre` VARCHAR(100))  BEGIN

DECLARE v_id_producto varchar(100);

select id_producto into v_id_producto from producto where UPPER(nombre)=UPPER(p_nombre);

    IF v_id_producto IS NOT NULL THEN
        SELECT v_id_producto as numero;
    ELSE
        SELECT -1 as numero;
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(1, 'JUEGOS Y JUGUETES'),
(2, 'COMPUTACIÓN'),
(3, 'CELULARES Y TABLETS'),
(21, 'CONSOLA Y VIDEOJUEGOS');

-- --------------------------------------------------------

--
-- Table structure for table `compra`
--

CREATE TABLE `compra` (
  `id_compra` int UNSIGNED NOT NULL,
  `lote` int UNSIGNED NOT NULL,
  `productos_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_total_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'suma de todos los envios de cada producto',
  `precio_real_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio real del envio',
  `precio_tarjeta` decimal(14,2) UNSIGNED NOT NULL,
  `precio_envio_unidad` decimal(14,2) UNSIGNED NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_compra` int UNSIGNED NOT NULL,
  `id_compra_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED NOT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio_base` decimal(14,2) UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio_base +''precio_envio_unidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `correlativo`
--

CREATE TABLE `correlativo` (
  `id_correlativo` int UNSIGNED NOT NULL,
  `tabla` varchar(100) NOT NULL,
  `numero` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correlativo`
--

INSERT INTO `correlativo` (`id_correlativo`, `tabla`, `numero`) VALUES
(1, 'compra', 0),
(2, 'venta', 0),
(3, 'producto', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_producto` int UNSIGNED NOT NULL,
  `id_imagenes` int NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(900) DEFAULT NULL,
  `precio` decimal(14,2) UNSIGNED NOT NULL,
  `descuento` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `estado` char(1) NOT NULL DEFAULT 'A',
  `id_subcategoria` int UNSIGNED DEFAULT NULL,
  `id_tipo` int UNSIGNED DEFAULT NULL,
  `id_proveedor` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `url`) VALUES
(3, '1212', 'asdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_categoria` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre`, `id_categoria`) VALUES
(5, 'aa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int UNSIGNED NOT NULL,
  `nombre_es` varchar(100) NOT NULL,
  `nombre_en` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre_es`, `nombre_en`) VALUES
(4, 'ss', 'fdf');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `estado`) VALUES
('christianjcd2512@hotmail.com', '00c9e9fcba35b05178246d24f85aaf20', 'A'),
('silviopd01@gmail.com', '05ce1b8ccca10743785becff19eb43e1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id_venta` int UNSIGNED NOT NULL,
  `producto_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_venta` int UNSIGNED NOT NULL,
  `id_venta_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED DEFAULT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `fk_compra_usuario` (`email`);

--
-- Indexes for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_compra`,`id_compra_detalle`),
  ADD KEY `fk_compra_detalle_producto_0` (`id_producto`);

--
-- Indexes for table `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`id_correlativo`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_producto`,`id_imagenes`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_subcategoria` (`id_subcategoria`),
  ADD KEY `fk_producto_tipo` (`id_tipo`),
  ADD KEY `fk_producto_proveedor_0` (`id_proveedor`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `fk_subcategoria_categoria` (`id_categoria`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_venta_usuario` (`email`);

--
-- Indexes for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_venta`,`id_venta_detalle`),
  ADD KEY `fk_venta_detalle_producto_0` (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `id_correlativo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_detalle_compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`),
  ADD CONSTRAINT `fk_compra_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagenes_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_proveedor_0` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`),
  ADD CONSTRAINT `fk_producto_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`);

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_venta_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_venta_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
