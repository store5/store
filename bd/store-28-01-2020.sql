-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 29, 2020 at 04:41 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto` (IN `p_nombre` VARCHAR(100))  BEGIN
    SELECT p.id_producto,
           p.nombre as nombre_producto,
           p.cantidad,
           p.descripcion,
           p.precio,
           p.descuento,
           p.estado,
           p.id_subcategoria,
           p.id_tipo,
           p.id_proveedor,
           s.nombre as nombre_subcategoria,
           c.nombre as nombre_categoria,
           t.nombre_es as nombre_es_tipo,
           t.nombre_en as nombre_en_tipo,
           p1.nombre as nombre_proveedor,
           p1.url
    FROM store.producto p
             INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
             INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
             INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
             INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
    WHERE p.nombre like concat('%%',p_nombre,'%%');
    END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto_foto` (IN `p_nombre` VARCHAR(100))  BEGIN
   SELECT p.id_producto,
       p.nombre as nombre_producto,
       p.cantidad,
       p.descripcion,
       p.precio,
       p.descuento,
       p.estado,
       p.id_subcategoria,
       p.id_tipo,
       p.id_proveedor,
       s.nombre as nombre_subcategoria,
       c.nombre as nombre_categoria,
       t.nombre_es as nombre_es_tipo,
       t.nombre_en as nombre_en_tipo,
       p1.nombre as nombre_proveedor,
       p1.url,
       IFNULL((select i.id_imagenes from imagenes i where i.id_producto=p.id_producto order by 1 desc limit 1),  0) as id_imagenes
FROM store.producto p
	INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
		INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
	INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
	INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
WHERE p.nombre like concat('%%',p_nombre,'%%');
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto_por_nombre` (IN `p_nombre` VARCHAR(100))  BEGIN
    select * from producto where nombre like p_nombre;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_correlativo` (IN `p_tabla` VARCHAR(50))  BEGIN

DECLARE v_numero varchar(50);

select numero+1 into v_numero from correlativo where tabla=p_tabla;

    IF v_numero IS NOT NULL THEN
        SELECT v_numero as numero;
    ELSE
        SELECT -1;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_inicio_sesion` (IN `p_email` VARCHAR(50), IN `p_password` CHAR(32))  BEGIN

DECLARE v_email varchar(50);

select email into v_email
from usuario
where password = p_password and email = p_email and estado = 'A';

    IF v_email IS NOT NULL THEN
        SELECT
                200 as estado,
               '' as mensaje,
                v_email as email;
    ELSE
        SELECT 500 as estado,'EROR... REVISE SU USUARIO Y/O CONTRASEÑA SEAN CORRECTA' as mensaje, '' as email;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_buscar_compra` (IN `p_nombre` VARCHAR(100))  BEGIN

DECLARE v_id_producto varchar(100);

select id_producto into v_id_producto from producto where UPPER(nombre)=UPPER(p_nombre);

    IF v_id_producto IS NOT NULL THEN
        SELECT v_id_producto as numero;
    ELSE
        SELECT -1 as numero;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_relativo` (IN `p_id_producto` INT)  BEGIN

DECLARE v_tipo varchar(100);
DECLARE v_subcategoria varchar(100);
DECLARE v_return int;

select s.nombre , t.nombre_en into v_subcategoria,v_tipo
from producto p
    inner join tipo t on p.id_tipo = t.id_tipo
    inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
where p.id_producto=p_id_producto;

select count(*) into v_return
from producto p
    inner join tipo t on p.id_tipo = t.id_tipo
    inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
where t.nombre_en like v_tipo and p.id_producto<>p_id_producto;

        if v_return = 10 then
            select p.foto,p.id_producto,p.nombre,p.precio,s.nombre as nombre_subcategoria, t.nombre_es as nombre_es, t.nombre_en as nombre_en, p.estado, p.precio from producto p inner join tipo t on p.id_tipo = t.id_tipo inner join subcategoria s on p.id_subcategoria = s.id_subcategoria where t.nombre_en like v_tipo and p.id_producto<>p_id_producto order by precio desc limit 0,10;
        else
            ((select p.foto,p.id_producto,p.nombre,p.precio,s.nombre as nombre_subcategoria, t.nombre_es as nombre_es, t.nombre_en as nombre_en, p.estado, p.precio
            from producto p
                inner join tipo t on p.id_tipo = t.id_tipo
                inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
            where t.nombre_en like v_tipo and p.id_producto<>p_id_producto
            order by precio desc)
            union distinct
            (select p.foto,p.id_producto,p.nombre,p.precio,s.nombre as nombre_subcategoria, t.nombre_es as nombre_es, t.nombre_en as nombre_en, p.estado, p.precio
            from producto p
                inner join tipo t on p.id_tipo = t.id_tipo
                inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
            where s.nombre like v_subcategoria and p.id_producto<>p_id_producto
            order by precio desc)) limit 0,10;
        end if;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(42, 'JUEGOS Y JUGUETES'),
(43, 'COMPUTACIÓN'),
(44, 'CELULARES Y TABLETS'),
(45, 'CONSOLA Y VIDEOJUEGOS');

-- --------------------------------------------------------

--
-- Table structure for table `compra`
--

CREATE TABLE `compra` (
  `id_compra` int UNSIGNED NOT NULL,
  `lote` int UNSIGNED NOT NULL,
  `productos_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio real del envio',
  `precio_tarjeta` decimal(14,2) UNSIGNED NOT NULL,
  `precio_envio_unidad` decimal(14,2) UNSIGNED NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL,
  `precio_total_envio` decimal(14,2) UNSIGNED NOT NULL,
  `estado` char(1) DEFAULT 'P'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra`
--

INSERT INTO `compra` (`id_compra`, `lote`, `productos_total`, `sub_total`, `total`, `precio_real_envio`, `precio_tarjeta`, `precio_envio_unidad`, `fecha_hora`, `email`, `precio_total_envio`, `estado`) VALUES
(1, 3, 29, '197.75', '501.87', '97.97', '8.73', '3.68', '2020-01-20 23:28:07', 'silviopd01@gmail.com', '304.12', 'P'),
(2, 2, 34, '196.14', '533.20', '103.32', '8.84', '3.30', '2020-01-24 21:53:32', 'silviopd01@gmail.com', '337.06', 'P'),
(3, 1, 33, '191.66', '438.14', '103.32', '8.71', '3.39', '2020-01-25 02:08:27', 'silviopd01@gmail.com', '246.48', 'P'),
(4, 4, 37, '117.42', '177.50', '0.08', '0.00', '0.00', '2020-01-25 23:00:55', 'silviopd01@gmail.com', '18.21', 'P'),
(5, 5, 14, '147.49', '174.87', '3.00', '0.00', '1.96', '2020-01-25 23:14:43', 'silviopd01@gmail.com', '27.38', 'P'),
(6, 6, 13, '183.79', '218.79', '26.68', '0.00', '2.69', '2020-01-25 23:23:10', 'silviopd01@gmail.com', '35.00', 'P'),
(7, 7, 6, '84.89', '102.68', '6.35', '0.00', '2.97', '2020-01-25 23:28:28', 'silviopd01@gmail.com', '17.79', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_compra` int UNSIGNED NOT NULL,
  `id_compra_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED NOT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio_base` decimal(14,2) UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio_base +''precio_envio_unidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_compra`, `id_compra_detalle`, `id_producto`, `cantidad`, `precio_base`, `sub_total`, `envio`, `total`, `precio_real`) VALUES
(1, 1, 1, 1, '3.34', '3.34', '10.55', '13.89', '7.02'),
(1, 2, 2, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 3, 3, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 4, 4, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 5, 5, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 6, 6, 1, '4.17', '4.17', '10.55', '14.72', '7.85'),
(1, 7, 7, 1, '5.00', '5.00', '10.55', '15.55', '8.68'),
(1, 8, 8, 1, '5.34', '5.34', '10.55', '15.89', '9.02'),
(1, 9, 9, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 10, 10, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 11, 11, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 12, 12, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 13, 13, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 14, 14, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 15, 15, 1, '6.17', '6.17', '10.55', '16.72', '9.85'),
(1, 16, 16, 2, '6.67', '13.34', '10.20', '23.54', '10.35'),
(1, 17, 17, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 18, 18, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 19, 19, 1, '8.34', '8.34', '18.30', '26.64', '12.02'),
(1, 20, 20, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 21, 21, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 22, 22, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 23, 23, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 24, 24, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 25, 25, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 26, 26, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 27, 27, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 28, 28, 1, '10.99', '10.99', '10.55', '21.54', '14.67'),
(2, 1, 29, 1, '3.84', '3.84', '10.55', '14.39', '7.14'),
(2, 2, 30, 1, '6.67', '6.67', '10.20', '16.87', '9.97'),
(2, 3, 31, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 4, 32, 1, '9.17', '9.17', '10.55', '19.72', '12.47'),
(2, 5, 33, 1, '3.84', '3.84', '10.20', '14.04', '7.14'),
(2, 6, 34, 1, '9.17', '9.17', '10.20', '19.37', '12.47'),
(2, 7, 35, 1, '5.84', '5.84', '10.55', '16.39', '9.14'),
(2, 8, 36, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 9, 37, 1, '7.50', '7.50', '10.20', '17.70', '10.80'),
(2, 10, 38, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 11, 39, 1, '9.17', '9.17', '10.10', '19.27', '12.47'),
(2, 12, 40, 1, '4.17', '4.17', '16.13', '20.30', '7.47'),
(2, 13, 41, 1, '4.17', '4.17', '16.13', '20.30', '7.47'),
(2, 14, 42, 1, '3.34', '3.34', '10.55', '13.89', '6.64'),
(2, 15, 43, 1, '5.67', '5.67', '10.20', '15.87', '8.97'),
(2, 16, 44, 1, '4.67', '4.67', '10.20', '14.87', '7.97'),
(2, 17, 45, 1, '4.67', '4.67', '10.20', '14.87', '7.97'),
(2, 18, 46, 1, '6.84', '6.84', '10.55', '17.39', '10.14'),
(2, 19, 47, 1, '9.17', '9.17', '10.20', '19.37', '12.47'),
(2, 20, 48, 1, '9.17', '9.17', '10.20', '19.37', '12.47'),
(2, 21, 49, 1, '5.84', '5.84', '10.55', '16.39', '9.14'),
(2, 22, 50, 1, '4.50', '4.50', '10.20', '14.70', '7.80'),
(2, 23, 51, 1, '5.00', '5.00', '10.20', '15.20', '8.30'),
(2, 24, 52, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 25, 53, 1, '3.00', '3.00', '22.08', '25.08', '6.30'),
(2, 26, 54, 1, '3.00', '3.00', '22.08', '25.08', '6.30'),
(2, 27, 55, 1, '3.00', '3.00', '22.08', '25.08', '6.30'),
(2, 28, 56, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 29, 57, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 30, 58, 1, '7.50', '7.50', '10.20', '17.70', '10.80'),
(2, 31, 59, 1, '5.84', '5.84', '10.20', '16.04', '9.14'),
(2, 32, 60, 1, '5.84', '5.84', '10.55', '16.39', '9.14'),
(2, 33, 61, 1, '5.84', '5.84', '10.55', '16.39', '9.14'),
(2, 34, 62, 1, '4.67', '4.67', '10.55', '15.22', '7.97'),
(3, 1, 63, 1, '6.84', '6.84', '22.52', '29.36', '10.23'),
(3, 2, 64, 1, '6.84', '6.84', '22.52', '29.36', '10.23'),
(3, 3, 65, 1, '6.84', '6.84', '22.52', '29.36', '10.23'),
(3, 4, 66, 1, '4.17', '4.17', '10.55', '14.72', '7.56'),
(3, 5, 67, 1, '2.50', '2.50', '10.55', '13.05', '5.89'),
(3, 6, 95, 1, '5.00', '5.00', '10.20', '15.20', '8.39'),
(3, 7, 68, 1, '5.00', '5.00', '10.20', '15.20', '8.39'),
(3, 8, 69, 1, '10.00', '10.00', '10.20', '20.20', '13.39'),
(3, 9, 70, 1, '4.67', '4.67', '10.55', '15.22', '8.06'),
(3, 10, 71, 1, '5.34', '5.34', '22.52', '27.86', '8.73'),
(3, 11, 72, 1, '5.34', '5.34', '22.52', '27.86', '8.73'),
(3, 12, 73, 1, '5.34', '5.34', '22.52', '27.86', '8.73'),
(3, 13, 74, 1, '5.84', '5.84', '10.55', '16.39', '9.23'),
(3, 14, 75, 1, '4.67', '4.67', '10.20', '14.87', '8.06'),
(3, 15, 76, 1, '5.00', '5.00', '10.55', '15.55', '8.39'),
(3, 16, 77, 1, '4.17', '4.17', '10.55', '14.72', '7.56'),
(3, 17, 78, 1, '5.84', '5.84', '10.28', '16.12', '9.23'),
(3, 18, 79, 1, '5.84', '5.84', '22.52', '28.36', '9.23'),
(3, 19, 80, 1, '5.84', '5.84', '22.52', '28.36', '9.23'),
(3, 20, 81, 1, '5.84', '5.84', '22.52', '28.36', '9.23'),
(3, 21, 82, 1, '5.84', '5.84', '22.52', '28.36', '9.23'),
(3, 22, 83, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 23, 84, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 24, 85, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 25, 86, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 26, 87, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 27, 88, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 28, 89, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 29, 90, 1, '5.84', '5.84', '33.24', '39.08', '9.23'),
(3, 30, 91, 1, '9.17', '9.17', '10.55', '19.72', '12.56'),
(3, 31, 92, 1, '6.67', '6.67', '10.55', '17.22', '10.06'),
(3, 32, 93, 1, '6.17', '6.17', '10.20', '16.37', '9.56'),
(3, 33, 94, 1, '6.17', '6.17', '10.20', '16.37', '9.56'),
(4, 1, 96, 1, '8.04', '8.04', '0.00', '8.04', '8.04'),
(4, 2, 97, 1, '7.80', '7.80', '0.00', '7.80', '7.80'),
(4, 3, 98, 1, '7.68', '7.68', '0.00', '7.68', '7.68'),
(4, 4, 99, 1, '11.63', '11.63', '0.00', '11.63', '11.63'),
(4, 5, 100, 1, '11.63', '11.63', '0.00', '11.63', '11.63'),
(4, 6, 101, 1, '9.22', '9.22', '0.75', '9.97', '9.22'),
(4, 7, 102, 1, '9.22', '9.22', '0.75', '9.97', '9.22'),
(4, 8, 103, 1, '8.80', '8.80', '2.40', '11.20', '8.80'),
(4, 9, 104, 1, '13.90', '13.90', '3.15', '17.05', '13.90'),
(4, 10, 105, 1, '10.08', '10.08', '0.00', '10.08', '10.08'),
(4, 11, 106, 1, '10.16', '10.16', '0.00', '10.16', '10.16'),
(4, 12, 107, 1, '10.16', '10.16', '0.00', '10.16', '10.16'),
(4, 13, 108, 1, '10.68', '10.68', '0.00', '10.68', '10.68'),
(4, 14, 109, 6, '2.77', '16.62', '3.58', '20.20', '2.77'),
(4, 15, 110, 6, '2.26', '13.56', '3.58', '17.14', '2.26'),
(4, 16, 111, 12, '1.52', '18.24', '4.00', '22.24', '1.52'),
(5, 1, 112, 6, '11.55', '69.30', '4.41', '73.71', '13.51'),
(5, 2, 113, 4, '4.28', '17.12', '2.76', '19.88', '6.24'),
(5, 3, 114, 2, '8.60', '17.20', '3.17', '20.37', '10.56'),
(5, 4, 115, 2, '11.46', '22.92', '6.11', '29.03', '13.42'),
(5, 5, 116, 1, '4.44', '4.44', '4.86', '9.30', '6.40'),
(5, 6, 117, 1, '13.89', '13.89', '8.91', '22.80', '15.85'),
(5, 7, 118, 1, '1.35', '1.35', '0.16', '1.51', '3.31'),
(5, 8, 119, 1, '1.27', '1.27', '0.00', '1.27', '3.23'),
(6, 1, 120, 1, '27.01', '27.01', '9.32', '36.33', '29.70'),
(6, 2, 121, 1, '26.22', '26.22', '9.32', '35.54', '28.91'),
(6, 3, 122, 2, '8.97', '17.94', '4.63', '22.57', '11.66'),
(6, 4, 123, 1, '12.73', '12.73', '4.63', '17.36', '15.42'),
(6, 5, 124, 1, '13.08', '13.08', '4.63', '17.71', '15.77'),
(6, 6, 125, 1, '13.36', '13.36', '4.63', '17.99', '16.05'),
(6, 7, 126, 1, '6.49', '6.49', '6.63', '13.12', '9.18'),
(6, 8, 127, 1, '6.49', '6.49', '6.63', '13.12', '9.18'),
(6, 9, 128, 1, '14.84', '14.84', '6.63', '21.47', '17.53'),
(6, 10, 129, 1, '14.84', '14.84', '6.63', '21.47', '17.53'),
(6, 11, 130, 1, '15.39', '15.39', '6.63', '22.02', '18.08'),
(6, 12, 131, 1, '15.39', '15.39', '6.63', '22.02', '18.08'),
(7, 1, 132, 1, '28.58', '28.58', '5.06', '33.64', '31.55'),
(7, 2, 133, 1, '12.70', '12.70', '4.63', '17.33', '15.67'),
(7, 3, 134, 1, '13.80', '13.80', '5.23', '19.03', '16.77'),
(7, 4, 135, 1, '8.64', '8.64', '6.93', '15.57', '11.61'),
(7, 5, 136, 1, '10.58', '10.58', '2.29', '12.87', '13.55'),
(7, 6, 137, 1, '10.58', '10.58', '2.29', '12.87', '13.55');

-- --------------------------------------------------------

--
-- Table structure for table `correlativo`
--

CREATE TABLE `correlativo` (
  `id_correlativo` int UNSIGNED NOT NULL,
  `tabla` varchar(100) NOT NULL,
  `numero` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correlativo`
--

INSERT INTO `correlativo` (`id_correlativo`, `tabla`, `numero`) VALUES
(1, 'compra', 7),
(2, 'venta', 0),
(3, 'producto', 137);

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_producto` int UNSIGNED NOT NULL,
  `id_imagenes` int NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id_producto`, `id_imagenes`, `url`) VALUES
(1, 1, '1.png'),
(1, 2, '2.png'),
(2, 1, '1.jpg'),
(2, 2, '2.jpg'),
(3, 1, '1.jpg'),
(3, 2, '2.jpg'),
(4, 1, '1.jpg'),
(4, 2, '2.jpg'),
(5, 1, '1.jpg'),
(5, 2, '2.jpg'),
(6, 1, '1.jpg'),
(6, 2, '2.jpg'),
(7, 1, '1.jpg'),
(7, 2, '2.jpg'),
(8, 1, '1.jpg'),
(8, 2, '2.jpg'),
(9, 1, '1.jpg'),
(9, 2, '2.jpg'),
(10, 1, '1.jpg'),
(10, 2, '2.jpg'),
(11, 1, '1.png'),
(11, 2, '2.png'),
(12, 1, '1.jpg'),
(12, 2, '2.jpg'),
(13, 1, '1.jpg'),
(13, 2, '2.jpg'),
(14, 1, '1.jpg'),
(14, 2, '2.jpg'),
(15, 1, '1.jpg'),
(15, 2, '2.jpg'),
(16, 1, '1.jpg'),
(16, 2, '2.jpg'),
(17, 1, '1.jpg'),
(17, 2, '2.jpg'),
(18, 1, '1.jpg'),
(18, 2, '2.jpg'),
(19, 1, '1.jpg'),
(19, 2, '2.jpg'),
(19, 3, '3.jpg'),
(20, 1, '1.jpg'),
(20, 2, '2.jpg'),
(20, 3, '3.jpg'),
(21, 1, '1.jpg'),
(21, 2, '2.jpg'),
(22, 1, '1.jpg'),
(22, 2, '2.jpg'),
(23, 1, '1.jpg'),
(23, 2, '2.jpg'),
(24, 1, '1.jpg'),
(24, 2, '2.jpg'),
(25, 1, '1.jpg'),
(25, 2, '2.jpg'),
(26, 1, '1.jpeg'),
(26, 2, '2.jpeg'),
(27, 1, '1.jpg'),
(27, 2, '2.jpg'),
(28, 1, '1.jpg'),
(28, 2, '2.jpg'),
(29, 1, '1.jpg'),
(29, 2, '2.jpg'),
(30, 1, '1.jpg'),
(30, 2, '2.jpg'),
(31, 1, '1.jpg'),
(31, 2, '2.jpg'),
(32, 1, '1.jpg'),
(32, 2, '2.jpg'),
(33, 1, '1.jpg'),
(33, 2, '2.jpg'),
(34, 1, '1.jpg'),
(34, 2, '2.jpg'),
(35, 1, '1.jpg'),
(35, 2, '2.jpg'),
(36, 1, '1.jpg'),
(36, 2, '2.jpg'),
(37, 1, '1.jpg'),
(37, 2, '2.jpg'),
(38, 1, '1.jpg'),
(38, 2, '2.jpg'),
(39, 1, '1.jpg'),
(39, 2, '2.jpg'),
(40, 1, '1.jpg'),
(40, 2, '2.jpg'),
(41, 1, '1.jpg'),
(41, 2, '2.jpg'),
(42, 1, '1.jpg'),
(42, 2, '2.jpg'),
(43, 1, '1.jpg'),
(43, 2, '2.jpg'),
(44, 1, '1.jpg'),
(44, 2, '2.jpg'),
(45, 1, '1.jpg'),
(45, 2, '2.jpg'),
(46, 1, '1.jpg'),
(46, 2, '2.jpg'),
(47, 1, '1.jpg'),
(47, 2, '2.jpg'),
(48, 1, '1.jpg'),
(48, 2, '2.jpg'),
(49, 1, '1.jpg'),
(49, 2, '2.jpg'),
(50, 1, '1.jpg'),
(50, 2, '2.jpg'),
(51, 1, '1.jpg'),
(51, 2, '2.jpg'),
(52, 1, '1.png'),
(52, 2, '2.png'),
(53, 1, '1.jpg'),
(53, 2, '2.jpg'),
(54, 1, '1.jpg'),
(54, 2, '2.jpg'),
(55, 1, '1.jpg'),
(55, 2, '2.jpg'),
(56, 1, '1.jpg'),
(56, 2, '2.jpg'),
(57, 1, '1.jpg'),
(57, 2, '2.jpg'),
(58, 1, '1.jpg'),
(58, 2, '2.jpg'),
(59, 1, '1.jpg'),
(59, 2, '2.jpg'),
(60, 1, '1.jpg'),
(60, 2, '2.jpg'),
(61, 1, '1.jpg'),
(61, 2, '2.jpg'),
(62, 1, '1.jpg'),
(62, 2, '2.jpg'),
(63, 1, '1.jpg'),
(63, 2, '2.jpg'),
(64, 1, '1.jpg'),
(64, 2, '2.jpg'),
(65, 1, '1.jpg'),
(65, 2, '2.jpg'),
(66, 1, '1.jpg'),
(66, 2, '2.jpg'),
(67, 1, '1.jpg'),
(67, 2, '2.jpg'),
(68, 1, '1.jpg'),
(68, 2, '2.jpg'),
(69, 1, '1.jpg'),
(69, 2, '2.jpg'),
(70, 1, '1.jpg'),
(70, 2, '2.jpg'),
(71, 1, '1.jpg'),
(71, 2, '2.jpg'),
(72, 1, '1.jpg'),
(72, 2, '2.jpg'),
(73, 1, '1.jpg'),
(73, 2, '2.jpg'),
(74, 1, '1.jpg'),
(74, 2, '2.jpg'),
(75, 1, '1.jpg'),
(75, 2, '2.jpg'),
(76, 1, '1.jpg'),
(76, 2, '2.jpg'),
(77, 1, '1.jpg'),
(77, 2, '2.jpg'),
(78, 1, '1.jpg'),
(78, 2, '2.jpg'),
(79, 1, '1.jpg'),
(79, 2, '2.jpg'),
(80, 1, '1.jpg'),
(80, 2, '2.jpg'),
(81, 1, '1.jpg'),
(81, 2, '2.jpg'),
(82, 1, '1.jpg'),
(82, 2, '2.jpg'),
(83, 1, '1.png'),
(83, 2, '2.png'),
(84, 1, '1.jpg'),
(84, 2, '2.jpg'),
(85, 1, '1.jpg'),
(85, 2, '2.jpg'),
(86, 1, '1.jpg'),
(86, 2, '2.jpg'),
(87, 1, '1.jpg'),
(87, 2, '2.jpg'),
(88, 1, '1.jpg'),
(88, 2, '2.jpg'),
(89, 1, '1.jpg'),
(89, 2, '2.jpg'),
(90, 1, '1.jpg'),
(90, 2, '2.jpg'),
(91, 1, '1.jpg'),
(91, 2, '2.jpg'),
(92, 1, '1.jpg'),
(92, 2, '2.jpg'),
(93, 1, '1.png'),
(93, 2, '2.png'),
(94, 1, '1.jpg'),
(94, 2, '2.jpg'),
(95, 1, '1.jpg'),
(95, 2, '2.jpg'),
(96, 1, '1.jpg'),
(97, 1, '1.jpg'),
(98, 1, '1.jpg'),
(99, 1, '1.jpg'),
(100, 1, '1.jpg'),
(101, 1, '1.jpg'),
(102, 1, '1.jpg'),
(103, 1, '1.jpg'),
(103, 2, '2.jpg'),
(104, 1, '1.jpg'),
(105, 1, '1.jpg'),
(105, 2, '2.jpg'),
(106, 1, '1.jpg'),
(106, 2, '2.jpg'),
(107, 1, '1.jpg'),
(108, 1, '1.jpg'),
(108, 2, '2.jpg'),
(109, 1, '1.jpg'),
(109, 2, '2.jpg'),
(109, 3, '3.jpg'),
(110, 1, '1.jpg'),
(110, 2, '2.jpg'),
(110, 3, '3.jpg'),
(111, 1, '1.jpg'),
(111, 2, '2.jpg'),
(111, 3, '3.jpg'),
(111, 4, '4.jpg'),
(112, 1, '1.jpg'),
(112, 2, '2.jpg'),
(112, 3, '3.jpg'),
(113, 1, '1.jpg'),
(114, 1, '1.jpg'),
(115, 1, '1.jpg'),
(116, 1, '1.jpg'),
(116, 2, '2.jpg'),
(116, 3, '3.jpg'),
(117, 1, '1.jpg'),
(117, 2, '2.jpg'),
(117, 3, '3.jpg'),
(118, 1, '1.png'),
(119, 1, '1.jpg'),
(120, 1, '1.jpg'),
(121, 1, '1.jpg'),
(122, 1, '1.jpg'),
(122, 2, '2.jpg'),
(123, 1, '1.jpg'),
(123, 2, '2.jpg'),
(124, 1, '1.jpg'),
(124, 2, '2.jpg'),
(125, 1, '1.jpg'),
(125, 2, '2.jpg'),
(126, 1, '1.jpg'),
(127, 1, '1.jpg'),
(128, 1, '1.jpg'),
(128, 2, '2.jpg'),
(129, 1, '1.jpg'),
(129, 2, '2.jpg'),
(130, 1, '1.jpg'),
(131, 1, '1.jpg'),
(132, 1, '1.jpg'),
(132, 2, '2.jpg'),
(133, 1, '1.jpg'),
(133, 2, '2.jpg'),
(134, 1, '1.jpg'),
(134, 2, '2.jpg'),
(135, 1, '1.jpg'),
(135, 2, '2.jpg'),
(135, 3, '3.jpg'),
(136, 1, '1.png'),
(136, 2, '2.jpg'),
(137, 1, '1.jpg'),
(137, 2, '2.jpg'),
(137, 3, '3.jpg'),
(137, 4, '4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(900) DEFAULT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT '0.00',
  `descuento` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `estado` char(1) NOT NULL DEFAULT 'P' COMMENT 'P=pendiente\nN=nuevo\nA=antiguo',
  `id_subcategoria` int UNSIGNED DEFAULT NULL,
  `id_tipo` int UNSIGNED DEFAULT NULL,
  `id_proveedor` int UNSIGNED DEFAULT NULL,
  `foto` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `cantidad`, `descripcion`, `precio`, `descuento`, `estado`, `id_subcategoria`, `id_tipo`, `id_proveedor`, `foto`) VALUES
(1, 'THANOS', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 29, 24, 'S'),
(2, 'THE JOCKER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 45, 24, 'S'),
(3, 'HARLEY QUINN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 45, 24, 'S'),
(4, 'RICK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 65, 24, 'S'),
(5, 'MORTY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 65, 24, 'S'),
(6, 'SUPERMAN 85', 4, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 30, 24, 'S'),
(7, 'LUFFY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 61, 24, 'S'),
(8, 'JASON', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 74, 24, 'S'),
(9, 'GROOT', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 48, 24, 'S'),
(10, 'PENNYWISE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 53, 24, 'S'),
(11, 'IRON SPIDERMAN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 29, 24, 'S'),
(12, 'HARRY POTTER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 50, 24, 'S'),
(13, 'LEATHERFACE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 58, 24, 'S'),
(14, 'FREDDY KRUEGER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 62, 24, 'S'),
(15, 'DR. STRANGE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 41, 24, 'S'),
(16, 'JOHN WICK (CHASE)', 2, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '45.00', '0.00', 'N', 30, 54, 24, 'S'),
(17, 'BELLATRIX LESTRANGE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '50.00', '0.00', 'N', 30, 50, 24, 'S'),
(18, 'MAD-EYE MOODY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '50.00', '0.00', 'N', 30, 50, 24, 'S'),
(19, 'DAENERYS AND DROGON', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 23cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '70.00', '0.00', 'N', 30, 46, 24, 'S'),
(20, 'PROFESSOR SQUIRRELL', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 50, 24, 'S'),
(21, 'SIRIUS BLACK (CHASE)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 50, 24, 'S'),
(22, 'HARRY POTTER WITH GOLDEN EGG', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 50, 24, 'S'),
(23, 'THE MOUNTAIN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 46, 24, 'S'),
(24, 'SAUL GOODMAN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 32, 24, 'S'),
(25, 'HEISENBERG', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 32, 24, 'S'),
(26, 'WALTER WHITE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 32, 24, 'S'),
(27, 'TONY STARK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 69, 24, 'S'),
(28, 'LEONIDAS', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '55.00', '0.00', 'N', 30, 25, 24, 'S'),
(29, 'MINERVA MCGONAGAL', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 50, 24, 'S'),
(30, 'CHUCKY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 35, 24, 'S'),
(31, 'PICKLE RICK (WITH LASER)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 65, 24, 'S'),
(32, 'BILLY (SAW)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 67, 24, 'S'),
(33, 'DEADPOOL - 320', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 38, 24, 'S'),
(34, 'MICHAEL MYERS (FYE)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 49, 24, 'S'),
(35, 'NYMERIA', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(36, 'THE HOUND (SABUESO)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(37, 'CAPTAIN MARVEL (CHASE)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 57, 24, 'S'),
(38, 'ARROW', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 27, 24, 'S'),
(39, 'GOHAN SS2', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(40, 'STEVE (CAPITAN AMERICA)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 36, 24, 'S'),
(41, 'BLACK PANTHER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 36, 24, 'S'),
(42, 'IRON PATRIOT', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 72, 24, 'S'),
(43, 'THOR RAGNAROK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 72, 24, 'S'),
(44, 'HULK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 28, 24, 'S'),
(45, 'BLACK PANTHER SIN CASCO', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 31, 24, 'S'),
(46, 'MAJIN VEGETA', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(47, 'VEGITO', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(48, 'GOKU SS3', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(49, 'SUPER SAYAYIN BROLY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(50, 'MAESTRO ROSHI (SPECIAL)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(51, 'GOHAN (TRAINING OUTFIT)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(52, 'ZEN-OH', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(53, 'MAJIN BUU', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(54, 'KRILIN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(55, 'TRUNKS', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(56, 'SANSA STARK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(57, 'HODOR', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(58, 'RENLY BARATHEON', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(59, 'JAIMIE LANNISTER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(60, 'SAMUEL TARLY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(61, 'TIFFANY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 35, 24, 'S'),
(62, 'MEN IN BLACK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 59, 24, 'S'),
(63, 'SIMBA', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 44, 24, 'S'),
(64, 'MUFASA', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 44, 24, 'S'),
(65, 'PUMBAA', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 44, 24, 'S'),
(66, 'JESSICA RABBIT', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 40, 24, 'S'),
(67, 'BEERUS (BILS)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 42, 24, 'S'),
(68, 'NANCY', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 70, 24, 'S'),
(69, 'HULK BUSTER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 29, 24, 'S'),
(70, 'CIRI', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 71, 24, 'S'),
(71, 'KRATOS', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 47, 24, 'S'),
(72, 'KRATOS (SPECIAL)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 47, 24, 'S'),
(73, 'KRATOS (GLOW IN THE DARK)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 47, 24, 'S'),
(74, 'ALIEN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 73, 24, 'S'),
(75, 'WONDER WOMAN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 75, 24, 'S'),
(76, 'AQUAMAN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 55, 24, 'S'),
(77, 'SAILORMOON', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 66, 24, 'S'),
(78, 'EREDIN', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 71, 24, 'S'),
(79, 'DAENERYS', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(80, 'JON SNOW', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(81, 'GHOST', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(82, 'TYRION LANNISTER', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(83, 'ROBB STARK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(84, 'YGRITTE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(85, 'GREY WIND', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(86, 'GREY WORM', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(87, 'MELISANDRE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(88, 'DROGON', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(89, 'VISERION', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(90, 'RHAEGAL', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(91, 'JON SNOW (GMAC EXCLUSIVE)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 46, 24, 'S'),
(92, 'THOR (GLOW IN THE DARK)', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 72, 24, 'S'),
(93, 'ALBUS DUMBLEDORE', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 50, 24, 'S'),
(94, 'SIRIUS BLACK', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 50, 24, 'S'),
(95, 'SUPERMAN 207', 1, 'Nombre de la marca: Funko\n<br>\nTamaño: 10cm\n<br>\nMaterial: PVC\n<br>\nEntrega: producto en caja\n<br><br><br>\nPara adquirir el producto pongasé en contacto con nosotros\n<br>\nLa entrega se realiza a domicilio solo para lugares céntricos o previa coordinación', '0.00', '0.00', 'P', 30, 55, 24, 'S'),
(96, 'ALFOMBRILLA RICK AND MORTY 1', 1, '', '0.00', '0.00', 'P', 31, 65, 24, 'S'),
(97, 'ALFOMBRILLA RICK AND MORTY 2', 1, '', '0.00', '0.00', 'P', 31, 65, 24, 'S'),
(98, 'ALFOMBRILLA MARIO BROS', 1, '', '0.00', '0.00', 'P', 31, 56, 24, 'S'),
(99, 'ALFOMBRILLA NARUTO Y SASUKE', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(100, 'ALFOMBRILLA NARUTO, SASUKE Y SAKURA', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(101, 'ALFOMBRILLA NARUTO ENFURECIDO', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(102, 'ALFOMBRILLA ITACHI', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(103, 'ALFOMBRILLA INSIGNIA DRAGON BALL', 1, '', '0.00', '0.00', 'P', 31, 42, 24, 'S'),
(104, 'ALFOMBRILLA GOKU PEQUEñO', 1, '', '0.00', '0.00', 'P', 31, 42, 24, 'S'),
(105, 'ALFOMBRILLA GOKU SS3', 1, '', '0.00', '0.00', 'P', 31, 42, 24, 'S'),
(106, 'ALFOMBRILLA ONE PIECE', 1, '', '0.00', '0.00', 'P', 31, 61, 24, 'S'),
(107, 'ALFOMBRILLA NARUTO COMPLETO', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(108, 'ALFOMBRILLA NARUTO PODEROSO', 1, '', '0.00', '0.00', 'P', 31, 60, 24, 'S'),
(109, 'CABLE USB 3M IPHONE', 6, '', '0.00', '0.00', 'P', 32, 33, 24, 'S'),
(110, 'CABLE USB 2M IPHONE', 6, '', '0.00', '0.00', 'P', 32, 33, 24, 'S'),
(111, 'CABLE IPHONE 1M CON CAJA', 12, '', '0.00', '0.00', 'P', 32, 33, 24, 'S'),
(112, 'POWER BANK 20000MAH', 6, '', '0.00', '0.00', 'P', 33, 64, 24, 'S'),
(113, 'HUB USB CUADRADO', 4, '', '0.00', '0.00', 'P', 34, 51, 24, 'S'),
(114, 'HUB USB CIRCULAR', 2, '', '0.00', '0.00', 'P', 34, 51, 24, 'S'),
(115, 'VIDEO GAME CONSOLA 500 GAMES TRANSPARENTE', 2, '', '0.00', '0.00', 'P', 35, 37, 24, 'S'),
(116, 'DOCK CARGADOR DE MANDO DE PS4', 1, '', '0.00', '0.00', 'P', 36, 26, 24, 'S'),
(117, 'DOCK CARGADOR DE MANDO DE PS4 CON CONSOLA', 1, '', '0.00', '0.00', 'P', 36, 26, 24, 'S'),
(118, 'SKIN MARVEL', 1, '', '0.00', '0.00', 'P', 36, 68, 24, 'S'),
(119, 'SKIN 2', 1, '', '0.00', '0.00', 'P', 36, 68, 24, 'S'),
(120, 'BARCO GO SUNNY', 1, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(121, 'BARCO GOING MERRY', 1, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(122, 'ACE', 2, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(123, 'LUFFY Y SHANKS', 1, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(124, 'OJO DE ALCON (MIHAWK)', 1, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(125, 'LUFFY BALCÓN', 1, '', '0.00', '0.00', 'P', 37, 61, 24, 'S'),
(126, 'SHIKAMARU', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(127, 'KAKASHI', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(128, 'PRIMER HOKAGE', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(129, 'SEGUNDO HOKAGE', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(130, 'OBITO', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(131, 'MADARA', 1, '', '0.00', '0.00', 'P', 37, 60, 24, 'S'),
(132, 'ANGEMON', 1, '', '0.00', '0.00', 'P', 37, 39, 24, 'S'),
(133, 'ASH CON PIKACHU', 1, '', '0.00', '0.00', 'P', 37, 63, 24, 'S'),
(134, 'GOKU CON NUBE VOLADORA', 1, '', '0.00', '0.00', 'P', 37, 42, 24, 'S'),
(135, 'GOKU CON TRAJE AMARILLO', 1, '', '0.00', '0.00', 'P', 37, 42, 24, 'S'),
(136, 'ITACHI CON AGUMON', 1, '', '0.00', '0.00', 'P', 37, 39, 24, 'S'),
(137, 'ISHIDA CON GABUMON', 1, '', '0.00', '0.00', 'P', 37, 39, 24, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `url`) VALUES
(24, 'TONGLU QINYANG E-COMMERCE CO., LTD.', 'HTTPS://TLQY.EN.ALIBABA.COM/?SPM=A2756.TRADE-ORDER-LIST.0.0.169E76E98SXT4M&TRACELOG=FROM_ORDERLIST_COMPANY');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_categoria` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre`, `id_categoria`) VALUES
(30, 'FUNKO', 42),
(31, 'ALFOMBRILLA', 43),
(32, 'CABLE', 44),
(33, 'POWERBANK', 44),
(34, 'ACCESORIO DE COMPUTADORA', 43),
(35, 'OTROS', 45),
(36, 'PS4', 45),
(37, 'MUÑECO Y FIGURA DE ACCIÓN', 42);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int UNSIGNED NOT NULL,
  `nombre_es` varchar(100) NOT NULL,
  `nombre_en` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre_es`, `nombre_en`) VALUES
(25, '300', '300'),
(26, 'ACCESORIO PS4', 'ACCESORIO PS4'),
(27, 'FLECHA VERDE', 'ARROW'),
(28, 'AVENGERS ERA DE ULTRON', 'AVENGERS AGE OF ULTRON'),
(29, 'AVENGERS INFINITY WARS', 'AVENGERS INFINITY WARS'),
(30, 'BATMAN VS SUPERMAN', 'SUPERMAN'),
(31, 'PANTERA NEGRA', 'BLACK PANTHER'),
(32, 'BREAKING BAD', 'BREAKING BAD'),
(33, 'ACCESORIO IPHONE', 'ACCESORIO IPHONE'),
(35, 'CHUCKY', 'CHUCKY'),
(36, 'CIVIL WAR', 'CIVIL WAR'),
(37, 'CONSOLA RETRO', 'CONSOLA RETRO'),
(38, 'DEADPOOL', 'DEADPOOL'),
(39, 'DIGIMON', 'DIGIMON'),
(40, 'DISNEY', 'DISNEY'),
(41, 'DOCTOR STRANGE', 'DOCTOR STRANGE'),
(42, 'DRAGON BALL', 'DRAGON BALL'),
(44, 'EL REY LEON', 'EL REY LEON'),
(45, 'ESCUADRON SUICIDA', 'SUICIDE SQUAD'),
(46, 'JUEGO DE TRONOS', 'GAME OF THRONES'),
(47, 'DIOS DE LA GUERRA', 'GOD OF WAR'),
(48, 'GUARDIANES DE LA GALAXIA 2', 'GUARDIANS OF THE GALAXY 2'),
(49, 'HALLOWEEN', 'HALLOWEEN'),
(50, 'HARRY POTTER', 'HARRY POTTER'),
(51, 'HUB', 'HUB'),
(52, 'IRON MAN 3', 'IRON MAN 3'),
(53, 'IT', 'IT'),
(54, 'JOHN WICK 2', 'JOHN WICK 2'),
(55, 'LIGA DE LA JUSTICIA', 'JUSTICE LEAGUE'),
(56, 'MARIO BROSS', 'MARIO BROSS'),
(57, 'MARVEL', 'MARVEL'),
(58, 'MASACRE EN TEXAS', 'THE TEXAS CHAIN SAW MASSACRE'),
(59, 'HOMBRES DE NEGRO', 'MEN IN BLACK'),
(60, 'NARUTO', 'NARUTO'),
(61, 'ONE PIECE', 'ONE PIECE'),
(62, 'PESADILLA EN ELM STREET', 'A NIGHTMARE ON ELM STREET'),
(63, 'POKEMON', 'POKEMON'),
(64, 'POWERBANK', 'POWERBANK'),
(65, 'RICK AND MORTY', 'RICK AND MORTY'),
(66, 'SAILORMOON', 'SAILORMOON'),
(67, 'SAW', 'SAW'),
(68, 'SKINS', 'SKINS'),
(69, 'SPIDERMAN', 'SPIDERMAN'),
(70, 'STRANGER THINGS', 'STRANGER THINGS'),
(71, 'EL BRUJO', 'THE WITCHER'),
(72, 'THOR RAGNAROK', 'THOR RAGNAROK'),
(73, 'TOY STORY', 'TOY STORY'),
(74, 'VIERNES 13', 'FRIDAY THE 13th'),
(75, 'MUJER MARAVILLA', 'WONDER WOMAN');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `estado`) VALUES
('christianjcd2512@hotmail.com', '00c9e9fcba35b05178246d24f85aaf20', 'A'),
('silviopd01@gmail.com', '05ce1b8ccca10743785becff19eb43e1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id_venta` int UNSIGNED NOT NULL,
  `producto_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_venta` int UNSIGNED NOT NULL,
  `id_venta_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED DEFAULT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `fk_compra_usuario` (`email`);

--
-- Indexes for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_compra`,`id_compra_detalle`),
  ADD KEY `fk_compra_detalle_producto_0` (`id_producto`);

--
-- Indexes for table `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`id_correlativo`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_producto`,`id_imagenes`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_subcategoria` (`id_subcategoria`),
  ADD KEY `fk_producto_proveedor_0` (`id_proveedor`),
  ADD KEY `fk_producto_tipo_0` (`id_tipo`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `fk_subcategoria_categoria` (`id_categoria`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_venta_usuario` (`email`);

--
-- Indexes for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_venta`,`id_venta_detalle`),
  ADD KEY `fk_venta_detalle_producto_0` (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `id_correlativo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_detalle_compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`),
  ADD CONSTRAINT `fk_compra_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagenes_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_proveedor_0` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`),
  ADD CONSTRAINT `fk_producto_tipo_0` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`);

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_venta_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_venta_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
