-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 22, 2020 at 06:46 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto` (IN `p_nombre` VARCHAR(100))  BEGIN
    SELECT p.id_producto,
           p.nombre as nombre_producto,
           p.cantidad,
           p.descripcion,
           p.precio,
           p.descuento,
           p.estado,
           p.id_subcategoria,
           p.id_tipo,
           p.id_proveedor,
           s.nombre as nombre_subcategoria,
           c.nombre as nombre_categoria,
           t.nombre_es as nombre_es_tipo,
           t.nombre_en as nombre_en_tipo,
           p1.nombre as nombre_proveedor,
           p1.url
    FROM store.producto p
             INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
             INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
             INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
             INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
    WHERE p.nombre like concat('%%',p_nombre,'%%');
    END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto_foto` (IN `p_nombre` VARCHAR(100))  BEGIN
   SELECT p.id_producto,
       p.nombre as nombre_producto,
       p.cantidad,
       p.descripcion,
       p.precio,
       p.descuento,
       p.estado,
       p.id_subcategoria,
       p.id_tipo,
       p.id_proveedor,
       s.nombre as nombre_subcategoria,
       c.nombre as nombre_categoria,
       t.nombre_es as nombre_es_tipo,
       t.nombre_en as nombre_en_tipo,
       p1.nombre as nombre_proveedor,
       p1.url,
       IFNULL((select i.id_imagenes from imagenes i where i.id_producto=p.id_producto order by 1 desc limit 1),  0) as id_imagenes
FROM store.producto p
	INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
		INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
	INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
	INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
WHERE p.nombre like concat('%%',p_nombre,'%%');
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto_por_nombre` (IN `p_nombre` VARCHAR(100))  BEGIN
    select * from producto where nombre like p_nombre;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_correlativo` (IN `p_tabla` VARCHAR(50))  BEGIN

DECLARE v_numero varchar(50);

select numero+1 into v_numero from correlativo where tabla=p_tabla;

    IF v_numero IS NOT NULL THEN
        SELECT v_numero as numero;
    ELSE
        SELECT -1;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_inicio_sesion` (IN `p_email` VARCHAR(50), IN `p_password` CHAR(32))  BEGIN

DECLARE v_email varchar(50);

select email into v_email
from usuario
where password = p_password and email = p_email and estado = 'A';

    IF v_email IS NOT NULL THEN
        SELECT
                200 as estado,
               '' as mensaje,
                v_email as email;
    ELSE
        SELECT 500 as estado,'EROR... REVISE SU USUARIO Y/O CONTRASEÑA SEAN CORRECTA' as mensaje, '' as email;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_buscar_compra` (IN `p_nombre` VARCHAR(100))  BEGIN

DECLARE v_id_producto varchar(100);

select id_producto into v_id_producto from producto where UPPER(nombre)=UPPER(p_nombre);

    IF v_id_producto IS NOT NULL THEN
        SELECT v_id_producto as numero;
    ELSE
        SELECT -1 as numero;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_relativo` (IN `p_id_producto` INT)  BEGIN

DECLARE v_tipo varchar(100);
DECLARE v_subcategoria varchar(100);
DECLARE v_return varchar(100);

select s.nombre , t.nombre_en into v_subcategoria,v_tipo
from producto p
    inner join tipo t on p.id_tipo = t.id_tipo
    inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
where p.id_producto=p_id_producto;

select t.nombre_en into v_return
from producto p
    inner join tipo t on p.id_tipo = t.id_tipo
    inner join subcategoria s on p.id_subcategoria = s.id_subcategoria
where t.nombre_en like v_tipo and p.id_producto<>p_id_producto
order by precio desc
limit 1;

    IF v_return IS NOT NULL THEN
        select p.foto,p.id_producto,p.nombre,p.precio,s.nombre as nombre_subcategoria, t.nombre_es as nombre_es, t.nombre_en as nombre_en from producto p inner join tipo t on p.id_tipo = t.id_tipo inner join subcategoria s on p.id_subcategoria = s.id_subcategoria where t.nombre_en like v_tipo and p.id_producto<>p_id_producto order by precio desc limit 0,10;
    ELSE
        select p.foto,p.id_producto,p.nombre,p.precio,s.nombre as nombre_subcategoria, t.nombre_es as nombre_es, t.nombre_en as nombre_en from producto p inner join tipo t on p.id_tipo = t.id_tipo inner join subcategoria s on p.id_subcategoria = s.id_subcategoria where s.nombre like v_subcategoria and p.id_producto<>p_id_producto order by precio desc limit 0,10;
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(42, 'JUEGOS Y JUGUETES'),
(43, 'COMPUTACIÓN'),
(44, 'CELULARES Y TABLETS'),
(45, 'CONSOLA Y VIDEOJUEGOS');

-- --------------------------------------------------------

--
-- Table structure for table `compra`
--

CREATE TABLE `compra` (
  `id_compra` int UNSIGNED NOT NULL,
  `lote` int UNSIGNED NOT NULL,
  `productos_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio real del envio',
  `precio_tarjeta` decimal(14,2) UNSIGNED NOT NULL,
  `precio_envio_unidad` decimal(14,2) UNSIGNED NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL,
  `precio_total_envio` decimal(14,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra`
--

INSERT INTO `compra` (`id_compra`, `lote`, `productos_total`, `sub_total`, `total`, `precio_real_envio`, `precio_tarjeta`, `precio_envio_unidad`, `fecha_hora`, `email`, `precio_total_envio`) VALUES
(1, 3, 29, '197.75', '501.87', '97.97', '8.73', '3.68', '2020-01-20 23:28:07', 'silviopd01@gmail.com', '304.12');

-- --------------------------------------------------------

--
-- Table structure for table `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_compra` int UNSIGNED NOT NULL,
  `id_compra_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED NOT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio_base` decimal(14,2) UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio_base +''precio_envio_unidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_compra`, `id_compra_detalle`, `id_producto`, `cantidad`, `precio_base`, `sub_total`, `envio`, `total`, `precio_real`) VALUES
(1, 1, 1, 1, '3.34', '3.34', '10.55', '13.89', '7.02'),
(1, 2, 2, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 3, 3, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 4, 4, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 5, 5, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 6, 6, 1, '4.17', '4.17', '10.55', '14.72', '7.85'),
(1, 7, 7, 1, '5.00', '5.00', '10.55', '15.55', '8.68'),
(1, 8, 8, 1, '5.34', '5.34', '10.55', '15.89', '9.02'),
(1, 9, 9, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 10, 10, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 11, 11, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 12, 12, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 13, 13, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 14, 14, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 15, 15, 1, '6.17', '6.17', '10.55', '16.72', '9.85'),
(1, 16, 16, 2, '6.67', '13.34', '10.20', '23.54', '10.35'),
(1, 17, 17, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 18, 18, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 19, 19, 1, '8.34', '8.34', '18.30', '26.64', '12.02'),
(1, 20, 20, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 21, 21, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 22, 22, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 23, 23, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 24, 24, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 25, 25, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 26, 26, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 27, 27, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 28, 28, 1, '10.99', '10.99', '10.55', '21.54', '14.67');

-- --------------------------------------------------------

--
-- Table structure for table `correlativo`
--

CREATE TABLE `correlativo` (
  `id_correlativo` int UNSIGNED NOT NULL,
  `tabla` varchar(100) NOT NULL,
  `numero` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correlativo`
--

INSERT INTO `correlativo` (`id_correlativo`, `tabla`, `numero`) VALUES
(1, 'compra', 1),
(2, 'venta', 0),
(3, 'producto', 28);

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_producto` int UNSIGNED NOT NULL,
  `id_imagenes` int NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id_producto`, `id_imagenes`, `url`) VALUES
(11, 1, '1.png'),
(11, 2, '2.png'),
(28, 1, '1.jpg'),
(28, 2, '2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(900) DEFAULT NULL,
  `precio` decimal(14,2) UNSIGNED NOT NULL,
  `descuento` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `estado` char(1) NOT NULL DEFAULT 'A',
  `id_subcategoria` int UNSIGNED DEFAULT NULL,
  `id_tipo` int UNSIGNED DEFAULT NULL,
  `id_proveedor` int UNSIGNED DEFAULT NULL,
  `foto` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `cantidad`, `descripcion`, `precio`, `descuento`, `estado`, `id_subcategoria`, `id_tipo`, `id_proveedor`, `foto`) VALUES
(0, 'Mike like swimming. Mike watches football. ', 1570126088, 'Anne bought new car. Anne is shopping. Tony bought new car. Tony is walking. Tony has free time. ', '730609460287.11', '729.79', 'A', 33, 75, 24, 'g'),
(1, 'THANOS', 1, '', '45.00', '0.00', 'A', 30, 29, 24, 'N'),
(2, 'THE JOCKER', 1, '', '45.00', '0.00', 'A', 30, 45, 24, 'N'),
(3, 'HARLEY QUINN', 1, '', '45.00', '0.00', 'A', 30, 45, 24, 'N'),
(4, 'RICK', 1, '', '45.00', '0.00', 'A', 30, 65, 24, 'N'),
(5, 'MORTY', 1, '', '45.00', '0.00', 'A', 30, 65, 24, 'N'),
(6, 'SUPERMAN', 1, '', '45.00', '0.00', 'A', 30, 30, 24, 'N'),
(7, 'LUFFY', 1, '', '45.00', '0.00', 'A', 30, 61, 24, 'N'),
(8, 'JASON', 1, '', '45.00', '0.00', 'A', 30, 74, 24, 'N'),
(9, 'GROOT', 1, '', '45.00', '0.00', 'A', 30, 48, 24, 'N'),
(10, 'PENNYWISE', 1, '', '45.00', '0.00', 'A', 30, 53, 24, 'N'),
(11, 'IRON SPIDERMAN', 1, '', '45.00', '0.00', 'A', 30, 29, 24, 'S'),
(12, 'HARRY POTTER', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(13, 'LEATHERFACE', 1, '', '45.00', '0.00', 'A', 30, 58, 24, 'N'),
(14, 'FREDDY KRUEGER', 1, '', '45.00', '0.00', 'A', 30, 62, 24, 'N'),
(15, 'DR. STRANGE', 1, '', '45.00', '0.00', 'A', 30, 41, 24, 'N'),
(16, 'JOHN WICK (CHASE)', 2, '', '45.00', '0.00', 'A', 30, 54, 24, 'N'),
(17, 'BELLATRIX LESTRANGE', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(18, 'MAD-EYE MOODY', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(19, 'DAENERYS AND DROGON', 1, '', '70.00', '0.00', 'A', 30, 46, 24, 'N'),
(20, 'PROFESSOR SQUIRRELL', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(21, 'SIRIUS BLACK', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(22, 'HARRY POTTER WITH GOLDEN EGG', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(23, 'THE MOUNTAIN', 1, '', '55.00', '0.00', 'A', 30, 46, 24, 'N'),
(24, 'SAUL GOODMAN', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(25, 'HEISENBERG', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(26, 'WALTER WHITE', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(27, 'TONY STARK', 1, '', '55.00', '0.00', 'A', 30, 69, 24, 'N'),
(28, 'LEONIDAS', 1, '', '55.00', '0.00', 'A', 30, 25, 24, 'S'),
(29, 'Rudi watches football. I watches football. ', 1802512063, 'Anne is shopping. Anne is walking. Tony has free time. Anne bought new car. John has free time. ', '570467018161.25', '691.63', 'A', 35, 59, 24, 'N'),
(30, 'Mike loves flowers. Rudi loves flowers. ', 1479606320, NULL, '557732251884.91', '202.32', 'A', 34, 27, 24, 'N'),
(31, 'I like sports. ', 1712955679, 'John bought new car. Anne has free time. John has free time. Tony bought new car. John bought new car. ', '571757063149.32', '34.07', 'A', 36, 31, 24, 'N'),
(32, 'I watches football. ', 265695738, 'John has free time. Tony is walking. John bought new car. Anne bought new car. John is shopping. ', '897644602376.59', '193.10', 'A', 33, 46, 24, 'N'),
(33, 'I loves flowers. Rudi watches football. ', 231014531, 'John bought new car. Anne is shopping. Tony bought new car. John is shopping. Anne has free time. ', '214512122683.64', '232.10', 'A', 36, 29, 24, 'N'),
(34, 'Rudi watches football. ', 70359707, 'John has free time. Anne is walking. Anne has free time. Tony bought new car. John is shopping. ', '323345901478.78', '38.40', 'A', 33, 29, 24, 'N'),
(35, 'Mike like swimming. Mike watches football. ', 2002877782, 'Anne has free time. John is walking. Tony is shopping. Anne bought new car. Tony is walking. ', '148131482388.70', '683.36', 'A', 36, 44, 24, 'N'),
(36, 'I like sports. Rudi like sports. ', 573006457, 'Anne has free time. Anne has free time. Anne bought new car. John bought new car. ', '680841109935.60', '582.59', 'A', 30, 74, 24, 'N'),
(37, 'I watches football. ', 1190581120, 'Anne bought new car. Tony has free time. John is shopping. Tony has free time. ', '936445569652.74', '832.62', 'A', 30, 42, 24, 'N'),
(38, 'Mike watches football. ', 488052483, 'Tony is walking. Tony is shopping. Tony has free time. John is shopping. John has free time. ', '561774649667.90', '616.91', 'A', 30, 35, 24, 'N'),
(39, 'Mike loves flowers. Rudi watches football. ', 306801119, NULL, '292338860467.66', '649.93', 'A', 32, 72, 24, 'N'),
(40, 'I loves flowers. Mike like swimming. ', 108721130, 'Tony is walking. John is walking. ', '813386523215.88', '273.57', 'A', 36, 51, 24, 'N'),
(41, 'Mike like sports. ', 281161585, 'Tony bought new car. Tony has free time. Tony bought new car. ', '335385508560.56', '702.09', 'A', 31, 62, 24, 'N'),
(42, 'I watches football. ', 124394731, 'John bought new car. Anne is shopping. John bought new car. John has free time. Tony has free time. ', '721891677590.02', '998.22', 'A', 36, 44, 24, 'N'),
(43, 'Mike loves flowers. Rudi watches football. ', 988508620, 'Tony bought new car. Anne is shopping. Tony is walking. ', '616343589887.71', '475.21', 'A', 34, 46, 24, 'N'),
(44, 'Mike like swimming. Rudi watches football. ', 1004920843, 'John bought new car. Anne has free time. Tony is walking. John is shopping. Anne bought new car. ', '716805783525.46', '91.52', 'A', 30, 64, 24, 'N'),
(45, 'Mike watches football. ', 543558809, 'Anne is walking. Anne is shopping. John is walking. Tony is walking. John has free time. ', '57232941193.06', '857.06', 'A', 37, 64, 24, 'N'),
(46, 'Mike like sports. Mike loves flowers. ', 1695099879, 'Anne has free time. Tony is walking. John bought new car. Anne has free time. Anne has free time. ', '450335677313.39', '559.94', 'A', 36, 36, 24, 'N'),
(47, 'Mike loves flowers. Mike like sports. ', 1657697649, 'Anne bought new car. John has free time. John has free time. Tony has free time. ', '533027791823.32', '492.72', 'A', 32, 38, 24, 'N'),
(48, 'Rudi watches football. ', 741511677, 'Anne is walking. Anne is walking. John bought new car. Tony is walking. Anne has free time. ', '623352304164.52', '835.53', 'A', 37, 70, 24, 'N'),
(49, 'Rudi loves flowers. Rudi watches football. ', 1556039312, 'Tony has free time. Anne bought new car. John has free time. Anne bought new car. ', '688887774806.59', '182.75', 'A', 31, 70, 24, 'N'),
(50, 'Rudi loves flowers. Mike like swimming. ', 1899249900, 'Anne has free time. John has free time. Tony bought new car. John bought new car. ', '864891831681.84', '28.28', 'A', 36, 72, 24, 'N'),
(51, 'Rudi loves flowers. ', 907479117, 'Anne has free time. John has free time. Tony bought new car. John has free time. John is walking. ', '504121680975.86', '184.19', 'A', 34, 28, 24, 'N'),
(52, 'Mike watches football. Mike watches football. ', 362450710, 'Anne bought new car. John has free time. John is walking. Anne has free time. Anne has free time. ', '498441172522.78', '885.83', 'A', 36, 39, 24, 'N'),
(53, 'I loves flowers. Rudi watches football. ', 335617623, 'Anne bought new car. Tony bought new car. ', '730140780660.83', '158.96', 'A', 35, 53, 24, 'N'),
(54, 'Mike loves flowers. I loves flowers. ', 374391246, 'Anne bought new car. Tony is walking. John has free time. ', '833931638287.79', '942.92', 'A', 31, 38, 24, 'N'),
(55, 'Rudi like sports. Mike like swimming. ', 1209915257, 'Tony has free time. John has free time. John is shopping. Tony has free time. John is walking. ', '434550933147.06', '579.16', 'A', 36, 38, 24, 'N'),
(56, 'I watches football. I watches football. ', 208987943, 'Anne bought new car. Anne has free time. Tony has free time. John has free time. ', '163492147951.08', '7.85', 'A', 31, 47, 24, 'N'),
(57, 'Rudi loves flowers. Rudi like sports. ', 346277032, NULL, '657920277338.00', '573.46', 'A', 35, 38, 24, 'N'),
(58, 'I like swimming. I like swimming. ', 1361490460, 'Anne bought new car. Tony is shopping. ', '381326446334.97', '5.88', 'A', 36, 34, 24, 'N'),
(59, 'Rudi like swimming. Rudi like sports. ', 1133811621, 'John bought new car. John is walking. Anne has free time. Anne is walking. Tony bought new car. ', '454930246404.04', '775.31', 'A', 34, 38, 24, 'N'),
(60, 'Mike like swimming. Mike like swimming. ', 61500489, 'John is shopping. Tony is walking. Tony has free time. John has free time. Tony has free time. ', '828929960361.36', '295.35', 'A', 35, 52, 24, 'N'),
(61, 'I watches football. Rudi watches football. ', 1195502579, 'Tony bought new car. Anne has free time. ', '59486560097.25', '642.64', 'A', 30, 41, 24, 'N'),
(62, 'Rudi like swimming. Rudi watches football. ', 1282944731, NULL, '837227092936.12', '876.32', 'A', 30, 69, 24, 'N'),
(63, 'Mike loves flowers. Mike watches football. ', 1048738717, NULL, '436472178865.29', '593.90', 'A', 35, 62, 24, 'N'),
(64, 'Mike loves flowers. Rudi loves flowers. ', 1944105894, 'Anne bought new car. Anne bought new car. Tony has free time. John bought new car. Tony has free time. ', '513690627727.53', '281.47', 'A', 36, 60, 24, 'N'),
(65, 'Mike loves flowers. I watches football. ', 87974585, 'John bought new car. John has free time. John is walking. Tony has free time. John bought new car. ', '304331297896.57', '14.88', 'A', 33, 45, 24, 'N'),
(66, 'Rudi watches football. I loves flowers. ', 989082393, 'John has free time. John is walking. Tony is shopping. Anne is shopping. Tony has free time. ', '637327653022.44', '333.12', 'A', 34, 45, 24, 'N'),
(67, 'Mike watches football. ', 847327837, 'Tony bought new car. Anne has free time. ', '716369022437.41', '769.23', 'A', 32, 73, 24, 'N'),
(68, 'Mike watches football. ', 1718410612, 'Anne bought new car. Anne bought new car. Tony is walking. Anne has free time. John has free time. ', '375434344571.99', '970.34', 'A', 34, 45, 24, 'N'),
(69, 'Mike watches football. Rudi watches football. ', 707442196, 'Tony bought new car. John is shopping. John has free time. Tony is shopping. John bought new car. ', '248952856815.40', '68.80', 'A', 36, 54, 24, 'N'),
(70, 'Rudi watches football. Mike watches football. ', 1420141612, 'John bought new car. John is walking. Tony is walking. ', '736708258298.67', '82.53', 'A', 34, 32, 24, 'N'),
(71, 'Rudi like swimming. ', 1573088622, 'Anne is shopping. Anne bought new car. John has free time. John has free time. Tony has free time. ', '676744631036.00', '833.28', 'A', 34, 46, 24, 'N'),
(72, 'Rudi like swimming. I loves flowers. ', 1514087666, 'John is walking. Anne bought new car. John has free time. Tony bought new car. John bought new car. ', '804766890241.57', '987.06', 'A', 31, 72, 24, 'N'),
(73, 'I like sports. Rudi watches football. ', 1141365695, 'Anne has free time. Tony bought new car. ', '24342914814.34', '772.38', 'A', 35, 63, 24, 'N'),
(74, 'Mike watches football. Rudi loves flowers. ', 2074068, 'Anne has free time. John is shopping. Tony is shopping. John has free time. John has free time. ', '449078958816.59', '190.24', 'A', 30, 51, 24, 'N'),
(75, 'Mike loves flowers. Mike watches football. ', 2039344977, 'John bought new car. Tony has free time. ', '16562346671.24', '27.69', 'A', 31, 31, 24, 'N'),
(76, 'Mike loves flowers. Mike watches football. ', 773339441, 'Anne has free time. Anne is shopping. John bought new car. Tony is walking. ', '247122986708.41', '61.56', 'A', 30, 40, 24, 'N'),
(77, 'Mike like swimming. Rudi loves flowers. ', 1352262686, 'John has free time. Anne is walking. John has free time. Anne bought new car. Tony has free time. ', '42933541583.45', '111.69', 'A', 35, 45, 24, 'N'),
(78, 'Mike loves flowers. Rudi watches football. ', 382219873, 'John has free time. Anne has free time. ', '148222056516.42', '809.12', 'A', 34, 54, 24, 'N'),
(79, 'I like sports. Rudi watches football. ', 1055662347, 'Anne bought new car. Tony bought new car. ', '783413771846.35', '165.22', 'A', 36, 54, 24, 'N'),
(80, 'Rudi like swimming. Rudi like sports. ', 1954110780, 'Anne bought new car. John is shopping. Anne bought new car. Tony has free time. Tony has free time. ', '107567092629.09', '806.36', 'A', 34, 46, 24, 'N'),
(81, 'Rudi watches football. I like swimming. ', 606581965, 'Anne bought new car. Anne has free time. Tony bought new car. John bought new car. John bought new car. ', '979918022625.77', '762.06', 'A', 34, 28, 24, 'N'),
(82, 'I like swimming. Mike watches football. ', 1589777239, 'Tony bought new car. Anne has free time. John bought new car. Anne has free time. Tony is shopping. ', '675658212240.90', '830.72', 'A', 31, 45, 24, 'N'),
(83, 'Rudi like sports. Rudi like swimming. ', 579416414, 'Tony is shopping. Tony bought new car. John bought new car. Anne bought new car. Tony bought new car. ', '490902790920.15', '360.72', 'A', 34, 48, 24, 'N'),
(84, 'Mike watches football. Rudi loves flowers. ', 1390432218, 'Anne bought new car. Tony has free time. Tony bought new car. ', '766617980187.80', '119.69', 'A', 33, 60, 24, 'N'),
(85, 'Rudi loves flowers. I watches football. ', 1746914044, 'Tony is shopping. Anne has free time. Anne bought new car. Tony has free time. Tony has free time. ', '633273049167.72', '269.64', 'A', 31, 26, 24, 'N'),
(86, 'Mike loves flowers. Rudi like swimming. ', 1142351084, 'Tony bought new car. John has free time. Anne bought new car. ', '487037177591.56', '146.07', 'A', 32, 35, 24, 'N'),
(87, 'Rudi like sports. Rudi like sports. ', 79084126, 'John bought new car. Anne is walking. John has free time. John has free time. Tony has free time. ', '937377860218.32', '919.88', 'A', 35, 26, 24, 'N'),
(88, 'Mike watches football. Rudi watches football. ', 1325399101, 'Anne is walking. Anne is shopping. ', '689305413459.78', '534.12', 'A', 34, 70, 24, 'N'),
(89, 'Mike watches football. Mike watches football. ', 372966688, 'Anne is walking. John has free time. John bought new car. Tony bought new car. John bought new car. ', '247473387143.21', '426.01', 'A', 34, 74, 24, 'N'),
(90, 'Mike watches football. Mike loves flowers. ', 232960925, 'Anne bought new car. Anne is walking. John bought new car. John bought new car. John has free time. ', '539480851592.90', '444.20', 'A', 36, 28, 24, 'N'),
(91, 'Rudi like swimming. Mike like sports. ', 1254748079, 'Anne bought new car. Tony bought new car. Tony bought new car. Tony bought new car. Anne is shopping. ', '663054850706.06', '509.01', 'A', 31, 61, 24, 'N'),
(92, 'Rudi watches football. Mike watches football. ', 835121724, NULL, '845018916134.73', '587.12', 'A', 35, 72, 24, 'N'),
(93, 'Rudi watches football. ', 1859548706, 'John has free time. Anne is shopping. Anne bought new car. Anne has free time. Tony is walking. ', '97804865602.84', '968.82', 'A', 33, 55, 24, 'N'),
(94, 'Mike watches football. Mike loves flowers. ', 1130472073, NULL, '673083481310.82', '863.33', 'A', 35, 72, 24, 'N'),
(95, 'Rudi loves flowers. Mike like sports. ', 1930974551, 'Tony bought new car. Tony is walking. John has free time. John has free time. John is walking. ', '335111684073.85', '906.86', 'A', 35, 54, 24, 'N'),
(96, 'Mike watches football. Rudi watches football. ', 1898227307, 'Tony has free time. Anne has free time. Tony bought new car. ', '172309758885.51', '553.15', 'A', 32, 74, 24, 'N'),
(97, 'Mike loves flowers. I watches football. ', 660422257, 'Anne bought new car. Anne is walking. John bought new car. John has free time. John has free time. ', '875345132949.71', '969.01', 'A', 33, 33, 24, 'N'),
(98, 'Mike watches football. Mike like sports. ', 803272158, 'Anne bought new car. John is walking. John bought new car. Tony is shopping. Tony bought new car. ', '771999307897.21', '337.99', 'A', 36, 27, 24, 'N'),
(99, 'Rudi like swimming. Mike loves flowers. ', 888995136, 'Anne is walking. Anne bought new car. Anne bought new car. Tony bought new car. ', '734125234512.64', '287.20', 'A', 35, 66, 24, 'N'),
(100, 'Mike loves flowers. I loves flowers. ', 1587444410, NULL, '620659432320.33', '933.30', 'A', 37, 54, 24, 'N'),
(101, 'I watches football. Rudi watches football. ', 1874554653, 'Anne is shopping. Anne is walking. John has free time. ', '687095698056.89', '488.97', 'A', 37, 47, 24, 'N'),
(102, 'Mike watches football. Mike like sports. ', 1941200105, 'Anne bought new car. Anne is walking. ', '304025649607.22', '203.84', 'A', 35, 56, 24, 'N'),
(103, 'Mike watches football. ', 577719842, 'Anne bought new car. John bought new car. John bought new car. Tony is shopping. ', '430259833327.01', '956.18', 'A', 34, 60, 24, 'N'),
(104, 'I like swimming. Rudi loves flowers. ', 702909943, 'Anne is shopping. John is walking. Tony bought new car. Anne is shopping. John has free time. ', '380328631153.04', '721.77', 'A', 37, 42, 24, 'N'),
(105, 'I watches football. ', 1945301976, 'Tony is shopping. Anne bought new car. Tony bought new car. Anne has free time. Tony has free time. ', '291413168353.45', '188.82', 'A', 36, 63, 24, 'N'),
(106, 'Mike watches football. ', 1399883845, 'Tony bought new car. Anne has free time. Anne has free time. John is shopping. John has free time. ', '932315108952.92', '478.58', 'A', 30, 42, 24, 'N'),
(107, 'Mike like sports. Rudi watches football. ', 148132249, 'Anne bought new car. Tony is walking. John bought new car. John has free time. Anne has free time. ', '683905867609.40', '952.11', 'A', 32, 73, 24, 'N'),
(108, 'Rudi loves flowers. Mike watches football. ', 465458355, 'John bought new car. John is walking. Anne bought new car. Anne has free time. ', '271753288050.27', '780.14', 'A', 32, 69, 24, 'N'),
(109, 'Mike watches football. Rudi like sports. ', 688620863, 'Anne bought new car. John is shopping. John is shopping. John has free time. ', '525801379704.91', '559.43', 'A', 34, 70, 24, 'N'),
(110, 'I loves flowers. ', 2110522593, 'Tony bought new car. Anne bought new car. Tony has free time. John is walking. Anne has free time. ', '768827719640.19', '232.93', 'A', 34, 71, 24, 'N'),
(111, 'Mike watches football. Rudi watches football. ', 2060683647, 'Anne bought new car. Tony is walking. Tony bought new car. Tony has free time. ', '583006034945.06', '686.22', 'A', 34, 52, 24, 'N'),
(112, 'Mike loves flowers. Mike watches football. ', 1185610085, NULL, '907555108986.67', '632.83', 'A', 35, 42, 24, 'N'),
(113, 'Mike loves flowers. I like swimming. ', 45523759, 'Tony is shopping. Anne has free time. John has free time. ', '752290192225.93', '373.76', 'A', 34, 66, 24, 'N'),
(114, 'Mike watches football. Rudi watches football. ', 1269260491, 'Anne is shopping. Anne has free time. Tony bought new car. John is walking. Tony has free time. ', '496359921718.25', '646.60', 'A', 31, 55, 24, 'N'),
(115, 'I watches football. Mike like sports. ', 2098745342, 'Tony bought new car. John is shopping. John bought new car. John bought new car. John bought new car. ', '938095922659.60', '98.13', 'A', 34, 74, 24, 'N'),
(116, 'Rudi watches football. Rudi watches football. ', 1206486192, NULL, '163085595957.99', '596.03', 'A', 37, 36, 24, 'N'),
(117, 'Rudi watches football. Rudi loves flowers. ', 555783838, 'Tony has free time. John is shopping. Tony bought new car. Tony has free time. Tony has free time. ', '545040893515.91', '758.65', 'A', 37, 41, 24, 'N'),
(118, 'Rudi like swimming. I watches football. ', 215848669, 'Anne has free time. John is shopping. Tony has free time. Anne has free time. Anne has free time. ', '24464550449.17', '677.78', 'A', 31, 48, 24, 'N'),
(119, 'Mike watches football. I loves flowers. ', 1141423039, 'Tony has free time. John is walking. Anne has free time. John bought new car. Tony bought new car. ', '996124469269.16', '406.49', 'A', 35, 46, 24, 'N'),
(120, 'Mike watches football. I like swimming. ', 343330106, 'John is shopping. John bought new car. John is shopping. John has free time. Tony bought new car. ', '695524623173.73', '451.01', 'A', 33, 67, 24, 'N'),
(121, 'I watches football. Mike like swimming. ', 478641203, 'Anne bought new car. Anne has free time. John has free time. ', '775482182653.45', '34.21', 'A', 31, 33, 24, 'N'),
(122, 'Mike loves flowers. Mike watches football. ', 1201521543, NULL, '564301505792.59', '731.04', 'A', 31, 42, 24, 'N'),
(123, 'Mike loves flowers. Rudi like swimming. ', 1218121724, 'John bought new car. Anne is shopping. John has free time. Tony has free time. John has free time. ', '605409133461.65', '611.16', 'A', 32, 42, 24, 'N'),
(124, 'Mike loves flowers. Rudi watches football. ', 854011968, NULL, '736905799065.82', '792.65', 'A', 36, 54, 24, 'N'),
(125, 'Rudi loves flowers. Mike watches football. ', 1313953456, 'Anne is shopping. Anne is shopping. John is shopping. ', '712194679122.91', '62.19', 'A', 32, 70, 24, 'N'),
(126, 'Mike watches football. Mike loves flowers. ', 1731142010, 'Anne is shopping. John has free time. Tony has free time. Tony has free time. John is shopping. ', '8436547958.50', '375.11', 'A', 37, 45, 24, 'N'),
(127, 'Mike watches football. Rudi watches football. ', 915583316, 'Anne has free time. Anne is shopping. Tony bought new car. John has free time. ', '66619541881.97', '339.66', 'A', 37, 42, 24, 'N'),
(128, 'Rudi like swimming. I loves flowers. ', 738595407, 'Tony is walking. Anne is walking. ', '153042230965.00', '787.11', 'A', 33, 60, 24, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `url`) VALUES
(24, 'TONGLU QINYANG E-COMMERCE CO., LTD.', 'HTTPS://TLQY.EN.ALIBABA.COM/?SPM=A2756.TRADE-ORDER-LIST.0.0.169E76E98SXT4M&TRACELOG=FROM_ORDERLIST_COMPANY');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_categoria` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre`, `id_categoria`) VALUES
(30, 'FUNKO', 42),
(31, 'ALFOMBRILLA', 43),
(32, 'CABLE', 44),
(33, 'POWERBANK', 44),
(34, 'ACCESORIO DE COMPUTADORA', 43),
(35, 'OTROS', 45),
(36, 'PS4', 45),
(37, 'MUÑECO Y FIGURA DE ACCIÓN', 42);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int UNSIGNED NOT NULL,
  `nombre_es` varchar(100) NOT NULL,
  `nombre_en` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre_es`, `nombre_en`) VALUES
(25, '300', '300'),
(26, 'ACCESORIO PS4', 'PS4'),
(27, 'FLECHA VERDE', 'ARROW'),
(28, 'AVENGERS ERA DE ULTRON', 'AVENGERS AGE OF ULTRON'),
(29, 'AVENGERS GUERRA DEL INFINITO', 'AVENGERS INFINITY WARS'),
(30, 'BATMAN VS SUPERMAN', 'SUPERMAN'),
(31, 'PANTERA NEGRA', 'BLACK PANTHER'),
(32, 'BREAKING BAD', 'BREAKING BAD'),
(33, 'CABLE IPHONE', 'CABLE IPHONE'),
(34, 'CHUCKY 2', 'CHILDS PLAY 2'),
(35, 'CHUCKY', 'CHUCKY'),
(36, 'CIVIL WAR', 'CIVIL WAR'),
(37, 'CONSOLA RETRO', 'CONSOLA RETRO'),
(38, 'DEADPOOL', 'DEADPOOL'),
(39, 'DIGIMON', 'DIGIMON'),
(40, 'DISNEY', 'DISNEY'),
(41, 'DOCTOR STRANGE', 'DOCTOR STRANGE'),
(42, 'DRAGON BALL', 'DRAGON BALL'),
(43, 'DRAGON BALL Z', 'DRAGON BALL Z'),
(44, 'EL REY LEON', 'EL REY LEON'),
(45, 'ESCUADRON SUICIDA', 'SUICIDE SQUAD'),
(46, 'JUEGO DE TRONOS', 'GAME OF THRONES'),
(47, 'DIOS DE LA GUERRA', 'GOD OF WAR'),
(48, 'GUARDIANES DE LA GALAXIA 2', 'GUARDIANS OF THE GALAXY 2'),
(49, 'HALLOWEEN', 'HALLOWEEN'),
(50, 'HARRY POTTER', 'HARRY POTTER'),
(51, 'HUB', 'HUB'),
(52, 'IRON MAN 3', 'IRON MAN 3'),
(53, 'IT', 'IT'),
(54, 'JOHN WICK 2', 'JOHN WICK 2'),
(55, 'LIGA DE LA JUSTICIA', 'JUSTICE LEAGUE'),
(56, 'MARIO BROSS', 'MARIO BROSS'),
(57, 'MARVEL', 'MARVEL'),
(58, 'MASACRE EN TEXAS', 'THE TEXAS CHAIN SAW MASSACRE'),
(59, 'HOMBRES DE NEGRO', 'MEN IN BLACK'),
(60, 'NARUTO', 'NARUTO'),
(61, 'ONE PIECE', 'ONE PIECE'),
(62, 'PESADILLA EN ELM STREET', 'A NIGHTMARE ON ELM STREET'),
(63, 'POKEMON', 'POKEMON'),
(64, 'POWERBANK', 'POWERBANK'),
(65, 'RICK AND MORTY', 'RICK AND MORTY'),
(66, 'SAILORMOON', 'SAILORMOON'),
(67, 'SAW', 'SAW'),
(68, 'SKINS', 'SKINS'),
(69, 'SPIDERMAN DE REGRESO A CASA', 'SPIDERMAN HOMECOMMING'),
(70, 'STRANGER THINGS', 'STRANGER THINGS'),
(71, 'EL BRUJO', 'THE WITCHER'),
(72, 'THOR RAGNAROK', 'THOR RAGNAROK'),
(73, 'TOY STORY', 'TOY STORY'),
(74, 'VIERNES 13', 'FRIDAY THE 13th'),
(75, 'MUJER MARAVILLA', 'WONDER WOMAN');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `estado`) VALUES
('christianjcd2512@hotmail.com', '00c9e9fcba35b05178246d24f85aaf20', 'A'),
('silviopd01@gmail.com', '05ce1b8ccca10743785becff19eb43e1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id_venta` int UNSIGNED NOT NULL,
  `producto_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_venta` int UNSIGNED NOT NULL,
  `id_venta_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED DEFAULT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `fk_compra_usuario` (`email`);

--
-- Indexes for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_compra`,`id_compra_detalle`),
  ADD KEY `fk_compra_detalle_producto_0` (`id_producto`);

--
-- Indexes for table `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`id_correlativo`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_producto`,`id_imagenes`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_subcategoria` (`id_subcategoria`),
  ADD KEY `fk_producto_tipo` (`id_tipo`),
  ADD KEY `fk_producto_proveedor_0` (`id_proveedor`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `fk_subcategoria_categoria` (`id_categoria`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_venta_usuario` (`email`);

--
-- Indexes for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_venta`,`id_venta_detalle`),
  ADD KEY `fk_venta_detalle_producto_0` (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `id_correlativo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_detalle_compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`),
  ADD CONSTRAINT `fk_compra_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagenes_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_proveedor_0` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`),
  ADD CONSTRAINT `fk_producto_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`);

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_venta_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_venta_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
