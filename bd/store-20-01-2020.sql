-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 20, 2020 at 11:49 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

DELIMITER $$
--
-- Procedures
--
CREATE PROCEDURE `f_buscar_producto` (IN `p_nombre` VARCHAR(100))  BEGIN
    SELECT p.id_producto,
       p.nombre as nombre_producto,
       p.cantidad,
       p.descripcion,
       p.precio,
       p.descuento,
       p.estado,
       p.id_subcategoria,
       p.id_tipo,
       p.id_proveedor,
       s.nombre as nombre_subcategoria,
       c.nombre as nombre_categoria,
       t.nombre_es as nombre_es_tipo,
       t.nombre_en as nombre_en_tipo,
       p1.nombre as nombre_proveedor,
       p1.url
FROM store.producto p
	INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
		INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
	INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
	INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
WHERE p.nombre like concat('%%',p_nombre,'%%');
END$$

CREATE PROCEDURE `f_buscar_producto_foto` (IN `p_nombre` VARCHAR(100))  BEGIN
   SELECT p.id_producto,
       p.nombre as nombre_producto,
       p.cantidad,
       p.descripcion,
       p.precio,
       p.descuento,
       p.estado,
       p.id_subcategoria,
       p.id_tipo,
       p.id_proveedor,
       s.nombre as nombre_subcategoria,
       c.nombre as nombre_categoria,
       t.nombre_es as nombre_es_tipo,
       t.nombre_en as nombre_en_tipo,
       p1.nombre as nombre_proveedor,
       p1.url,
       IFNULL( i.id_imagenes,  0) as id_imagenes
FROM store.producto p
	INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
		INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
	INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
	INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
    LEFT JOIN store.imagenes i on (p.id_producto = i.id_producto)
WHERE p.nombre like concat('%%',p_nombre,'%%');
END$$

CREATE PROCEDURE `f_buscar_producto_por_nombre` (IN `p_nombre` VARCHAR(100))  BEGIN
    select * from producto where nombre like p_nombre;
END$$

CREATE PROCEDURE `f_correlativo` (IN `p_tabla` VARCHAR(50))  BEGIN

DECLARE v_numero varchar(50);

select numero+1 into v_numero from correlativo where tabla=p_tabla;

    IF v_numero IS NOT NULL THEN
        SELECT v_numero as numero;
    ELSE
        SELECT -1;
    END IF;
END$$

CREATE PROCEDURE `f_inicio_sesion` (IN `p_email` VARCHAR(50), IN `p_password` CHAR(32))  BEGIN

DECLARE v_email varchar(50);

select email into v_email
from usuario
where password = p_password and email = p_email and estado = 'A';

    IF v_email IS NOT NULL THEN
        SELECT
                200 as estado,
               '' as mensaje,
                v_email as email;
    ELSE
        SELECT 500 as estado,'EROR... REVISE SU USUARIO Y/O CONTRASEÑA SEAN CORRECTA' as mensaje, '' as email;
    END IF;
END$$

CREATE PROCEDURE `f_producto_buscar_compra` (IN `p_nombre` VARCHAR(100))  BEGIN

DECLARE v_id_producto varchar(100);

select id_producto into v_id_producto from producto where UPPER(nombre)=UPPER(p_nombre);

    IF v_id_producto IS NOT NULL THEN
        SELECT v_id_producto as numero;
    ELSE
        SELECT -1 as numero;
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(42, 'JUEGOS Y JUGUETES'),
(43, 'COMPUTACIÓN'),
(44, 'CELULARES Y TABLETS'),
(45, 'CONSOLA Y VIDEOJUEGOS');

-- --------------------------------------------------------

--
-- Table structure for table `compra`
--

CREATE TABLE `compra` (
  `id_compra` int UNSIGNED NOT NULL,
  `lote` int UNSIGNED NOT NULL,
  `productos_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio real del envio',
  `precio_tarjeta` decimal(14,2) UNSIGNED NOT NULL,
  `precio_envio_unidad` decimal(14,2) UNSIGNED NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL,
  `precio_total_envio` decimal(14,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra`
--

INSERT INTO `compra` (`id_compra`, `lote`, `productos_total`, `sub_total`, `total`, `precio_real_envio`, `precio_tarjeta`, `precio_envio_unidad`, `fecha_hora`, `email`, `precio_total_envio`) VALUES
(1, 3, 29, '197.75', '501.87', '97.97', '8.73', '3.68', '2020-01-20 23:28:07', 'silviopd01@gmail.com', '304.12');

-- --------------------------------------------------------

--
-- Table structure for table `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_compra` int UNSIGNED NOT NULL,
  `id_compra_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED NOT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio_base` decimal(14,2) UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio_base +''precio_envio_unidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_compra`, `id_compra_detalle`, `id_producto`, `cantidad`, `precio_base`, `sub_total`, `envio`, `total`, `precio_real`) VALUES
(1, 1, 1, 1, '3.34', '3.34', '10.55', '13.89', '7.02'),
(1, 2, 2, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 3, 3, 1, '4.17', '4.17', '16.84', '21.01', '7.85'),
(1, 4, 4, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 5, 5, 1, '4.17', '4.17', '16.13', '20.30', '7.85'),
(1, 6, 6, 1, '4.17', '4.17', '10.55', '14.72', '7.85'),
(1, 7, 7, 1, '5.00', '5.00', '10.55', '15.55', '8.68'),
(1, 8, 8, 1, '5.34', '5.34', '10.55', '15.89', '9.02'),
(1, 9, 9, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 10, 10, 1, '5.84', '5.84', '22.52', '28.36', '9.52'),
(1, 11, 11, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 12, 12, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 13, 13, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 14, 14, 1, '5.84', '5.84', '10.55', '16.39', '9.52'),
(1, 15, 15, 1, '6.17', '6.17', '10.55', '16.72', '9.85'),
(1, 16, 16, 2, '6.67', '13.34', '10.20', '23.54', '10.35'),
(1, 17, 17, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 18, 18, 1, '7.99', '7.99', '16.84', '24.83', '11.67'),
(1, 19, 19, 1, '8.34', '8.34', '18.30', '26.64', '12.02'),
(1, 20, 20, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 21, 21, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 22, 22, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 23, 23, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 24, 24, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 25, 25, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 26, 26, 1, '9.17', '9.17', '22.52', '31.69', '12.85'),
(1, 27, 27, 1, '9.17', '9.17', '10.55', '19.72', '12.85'),
(1, 28, 28, 1, '10.99', '10.99', '10.55', '21.54', '14.67');

-- --------------------------------------------------------

--
-- Table structure for table `correlativo`
--

CREATE TABLE `correlativo` (
  `id_correlativo` int UNSIGNED NOT NULL,
  `tabla` varchar(100) NOT NULL,
  `numero` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correlativo`
--

INSERT INTO `correlativo` (`id_correlativo`, `tabla`, `numero`) VALUES
(1, 'compra', 1),
(2, 'venta', 0),
(3, 'producto', 28);

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_producto` int UNSIGNED NOT NULL,
  `id_imagenes` int NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id_producto`, `id_imagenes`, `url`) VALUES
(1, 3, '3.png'),
(1, 4, '4.png'),
(28, 1, '1.jpg'),
(28, 2, '2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(900) DEFAULT NULL,
  `precio` decimal(14,2) UNSIGNED NOT NULL,
  `descuento` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `estado` char(1) NOT NULL DEFAULT 'A',
  `id_subcategoria` int UNSIGNED DEFAULT NULL,
  `id_tipo` int UNSIGNED DEFAULT NULL,
  `id_proveedor` int UNSIGNED DEFAULT NULL,
  `foto` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `cantidad`, `descripcion`, `precio`, `descuento`, `estado`, `id_subcategoria`, `id_tipo`, `id_proveedor`, `foto`) VALUES
(1, 'THANOS', 1, '', '45.00', '0.00', 'A', 30, 29, 24, 'N'),
(2, 'THE JOCKER', 1, '', '45.00', '0.00', 'A', 30, 45, 24, 'N'),
(3, 'HARLEY QUINN', 1, '', '45.00', '0.00', 'A', 30, 45, 24, 'N'),
(4, 'RICK', 1, '', '45.00', '0.00', 'A', 30, 65, 24, 'N'),
(5, 'MORTY', 1, '', '45.00', '0.00', 'A', 30, 65, 24, 'N'),
(6, 'SUPERMAN', 1, '', '45.00', '0.00', 'A', 30, 30, 24, 'N'),
(7, 'LUFFY', 1, '', '45.00', '0.00', 'A', 30, 61, 24, 'N'),
(8, 'JASON', 1, '', '45.00', '0.00', 'A', 30, 74, 24, 'N'),
(9, 'GROOT', 1, '', '45.00', '0.00', 'A', 30, 48, 24, 'N'),
(10, 'PENNYWISE', 1, '', '45.00', '0.00', 'A', 30, 53, 24, 'N'),
(11, 'IRON SPIDERMAN', 1, '', '45.00', '0.00', 'A', 30, 29, 24, 'N'),
(12, 'HARRY POTTER', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(13, 'LEATHERFACE', 1, '', '45.00', '0.00', 'A', 30, 58, 24, 'N'),
(14, 'FREDDY KRUEGER', 1, '', '45.00', '0.00', 'A', 30, 62, 24, 'N'),
(15, 'DR. STRANGE', 1, '', '45.00', '0.00', 'A', 30, 41, 24, 'N'),
(16, 'JOHN WICK (CHASE)', 2, '', '45.00', '0.00', 'A', 30, 54, 24, 'N'),
(17, 'BELLATRIX LESTRANGE', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(18, 'MAD-EYE MOODY', 1, '', '45.00', '0.00', 'A', 30, 50, 24, 'N'),
(19, 'DAENERYS AND DROGON', 1, '', '70.00', '0.00', 'A', 30, 46, 24, 'N'),
(20, 'PROFESSOR SQUIRRELL', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(21, 'SIRIUS BLACK', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(22, 'HARRY POTTER WITH GOLDEN EGG', 1, '', '55.00', '0.00', 'A', 30, 50, 24, 'N'),
(23, 'THE MOUNTAIN', 1, '', '55.00', '0.00', 'A', 30, 46, 24, 'N'),
(24, 'SAUL GOODMAN', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(25, 'HEISENBERG', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(26, 'WALTER WHITE', 1, '', '55.00', '0.00', 'A', 30, 32, 24, 'N'),
(27, 'TONY STARK', 1, '', '55.00', '0.00', 'A', 30, 69, 24, 'N'),
(28, 'LEONIDAS', 1, '', '55.00', '0.00', 'A', 30, 25, 24, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `url`) VALUES
(24, 'TONGLU QINYANG E-COMMERCE CO., LTD.', 'HTTPS://TLQY.EN.ALIBABA.COM/?SPM=A2756.TRADE-ORDER-LIST.0.0.169E76E98SXT4M&TRACELOG=FROM_ORDERLIST_COMPANY');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_categoria` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre`, `id_categoria`) VALUES
(30, 'FUNKO', 42),
(31, 'ALFOMBRILLA', 43),
(32, 'CABLE', 44),
(33, 'POWERBANK', 44),
(34, 'ACCESORIO DE COMPUTADORA', 43),
(35, 'OTROS', 45),
(36, 'PS4', 45),
(37, 'MUÑECO Y FIGURA DE ACCIÓN', 42);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int UNSIGNED NOT NULL,
  `nombre_es` varchar(100) NOT NULL,
  `nombre_en` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre_es`, `nombre_en`) VALUES
(25, '300', '300'),
(26, 'ACCESORIO PS4', 'PS4'),
(27, 'FLECHA VERDE', 'ARROW'),
(28, 'AVENGERS ERA DE ULTRON', 'AVENGERS AGE OF ULTRON'),
(29, 'AVENGERS GUERRA DEL INFINITO', 'AVENGERS INFINITY WARS'),
(30, 'BATMAN VS SUPERMAN', 'SUPERMAN'),
(31, 'PANTERA NEGRA', 'BLACK PANTHER'),
(32, 'BREAKING BAD', 'BREAKING BAD'),
(33, 'CABLE IPHONE', 'CABLE IPHONE'),
(34, 'CHUCKY 2', 'CHILDS PLAY 2'),
(35, 'CHUCKY', 'CHUCKY'),
(36, 'CIVIL WAR', 'CIVIL WAR'),
(37, 'CONSOLA RETRO', 'CONSOLA RETRO'),
(38, 'DEADPOOL', 'DEADPOOL'),
(39, 'DIGIMON', 'DIGIMON'),
(40, 'DISNEY', 'DISNEY'),
(41, 'DOCTOR STRANGE', 'DOCTOR STRANGE'),
(42, 'DRAGON BALL', 'DRAGON BALL'),
(43, 'DRAGON BALL Z', 'DRAGON BALL Z'),
(44, 'EL REY LEON', 'EL REY LEON'),
(45, 'ESCUADRON SUICIDA', 'SUICIDE SQUAD'),
(46, 'JUEGO DE TRONOS', 'GAME OF THRONES'),
(47, 'DIOS DE LA GUERRA', 'GOD OF WAR'),
(48, 'GUARDIANES DE LA GALAXIA 2', 'GUARDIANS OF THE GALAXY 2'),
(49, 'HALLOWEEN', 'HALLOWEEN'),
(50, 'HARRY POTTER', 'HARRY POTTER'),
(51, 'HUB', 'HUB'),
(52, 'IRON MAN 3', 'IRON MAN 3'),
(53, 'IT', 'IT'),
(54, 'JOHN WICK 2', 'JOHN WICK 2'),
(55, 'LIGA DE LA JUSTICIA', 'JUSTICE LEAGUE'),
(56, 'MARIO BROSS', 'MARIO BROSS'),
(57, 'MARVEL', 'MARVEL'),
(58, 'MASACRE EN TEXAS', 'THE TEXAS CHAIN SAW MASSACRE'),
(59, 'HOMBRES DE NEGRO', 'MEN IN BLACK'),
(60, 'NARUTO', 'NARUTO'),
(61, 'ONE PIECE', 'ONE PIECE'),
(62, 'PESADILLA EN ELM STREET', 'A NIGHTMARE ON ELM STREET'),
(63, 'POKEMON', 'POKEMON'),
(64, 'POWERBANK', 'POWERBANK'),
(65, 'RICK AND MORTY', 'RICK AND MORTY'),
(66, 'SAILORMOON', 'SAILORMOON'),
(67, 'SAW', 'SAW'),
(68, 'SKINS', 'SKINS'),
(69, 'SPIDERMAN DE REGRESO A CASA', 'SPIDERMAN HOMECOMMING'),
(70, 'STRANGER THINGS', 'STRANGER THINGS'),
(71, 'EL BRUJO', 'THE WITCHER'),
(72, 'THOR RAGNAROK', 'THOR RAGNAROK'),
(73, 'TOY STORY', 'TOY STORY'),
(74, 'VIERNES 13', 'FRIDAY THE 13th'),
(75, 'MUJER MARAVILLA', 'WONDER WOMAN');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `estado`) VALUES
('christianjcd2512@hotmail.com', '00c9e9fcba35b05178246d24f85aaf20', 'A'),
('silviopd01@gmail.com', '05ce1b8ccca10743785becff19eb43e1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id_venta` int UNSIGNED NOT NULL,
  `producto_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_venta` int UNSIGNED NOT NULL,
  `id_venta_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED DEFAULT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `fk_compra_usuario` (`email`);

--
-- Indexes for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_compra`,`id_compra_detalle`),
  ADD KEY `fk_compra_detalle_producto_0` (`id_producto`);

--
-- Indexes for table `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`id_correlativo`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_producto`,`id_imagenes`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_subcategoria` (`id_subcategoria`),
  ADD KEY `fk_producto_tipo` (`id_tipo`),
  ADD KEY `fk_producto_proveedor_0` (`id_proveedor`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `fk_subcategoria_categoria` (`id_categoria`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_venta_usuario` (`email`);

--
-- Indexes for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_venta`,`id_venta_detalle`),
  ADD KEY `fk_venta_detalle_producto_0` (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `id_correlativo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_detalle_compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`),
  ADD CONSTRAINT `fk_compra_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagenes_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_proveedor_0` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`),
  ADD CONSTRAINT `fk_producto_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`);

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_venta_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_venta_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
