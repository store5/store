-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 20, 2020 at 05:59 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto` (IN `p_nombre` VARCHAR(100))  BEGIN
    SELECT p.id_producto,
       p.nombre as nombre_producto,
       p.cantidad,
       p.descripcion,
       p.precio,
       p.descuento,
       p.estado,
       p.id_subcategoria,
       p.id_tipo,
       p.id_proveedor,
       s.nombre as nombre_subcategoria,
       c.nombre as nombre_categoria,
       t.nombre_es as nombre_es_tipo,
       t.nombre_en as nombre_en_tipo,
       p1.nombre as nombre_proveedor,
       p1.url
FROM store.producto p
	INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
		INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )
	INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
	INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )
WHERE p.nombre like concat('%%',p_nombre,'%%');
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_buscar_producto_por_nombre` (IN `p_nombre` VARCHAR(100))  BEGIN
    select * from producto where nombre like p_nombre;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_correlativo` (IN `p_tabla` VARCHAR(50))  BEGIN

DECLARE v_numero varchar(50);

select numero+1 into v_numero from correlativo where tabla=p_tabla;

    IF v_numero IS NOT NULL THEN
        SELECT v_numero as numero;
    ELSE
        SELECT -1;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_inicio_sesion` (IN `p_email` VARCHAR(50), IN `p_password` CHAR(32))  BEGIN

DECLARE v_email varchar(50);

select email into v_email
from usuario
where password = p_password and email = p_email and estado = 'A';

    IF v_email IS NOT NULL THEN
        SELECT
                200 as estado,
               '' as mensaje,
                v_email as email;
    ELSE
        SELECT 500 as estado,'EROR... REVISE SU USUARIO Y/O CONTRASEÑA SEAN CORRECTA' as mensaje, '' as email;
    END IF;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `f_producto_buscar_compra` (IN `p_nombre` VARCHAR(100))  BEGIN

DECLARE v_id_producto varchar(100);

select id_producto into v_id_producto from producto where UPPER(nombre)=UPPER(p_nombre);

    IF v_id_producto IS NOT NULL THEN
        SELECT v_id_producto as numero;
    ELSE
        SELECT -1 as numero;
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`) VALUES
(1, 'JUEGOS Y JUGUETES'),
(2, 'COMPUTACIÓN'),
(3, 'CELULARES Y TABLETS'),
(21, 'CONSOLA Y VIDEOJUEGOS'),
(22, 'Mike like swimming. Mike watches football. '),
(23, 'I watches football. I watches football. '),
(24, 'Rudi like swimming. I loves flowers. '),
(25, 'Mike watches football. Mike like sports. '),
(26, 'Mike watches football. Mike like swimming. '),
(27, 'Rudi watches football. Mike watches football. '),
(28, 'I like sports. Rudi loves flowers. '),
(29, 'Mike loves flowers. Mike watches football. '),
(30, 'I watches football. I loves flowers. '),
(31, 'Mike watches football. Mike watches football. '),
(32, 'Mike watches football. Rudi loves flowers. '),
(33, 'I loves flowers. Rudi watches football. '),
(34, 'Mike watches football. I like swimming. '),
(35, 'I watches football. Rudi loves flowers. '),
(36, 'Mike like sports. '),
(37, 'I watches football. Rudi loves flowers. '),
(38, 'Mike like sports. Mike watches football. '),
(39, 'Mike watches football. I loves flowers. '),
(40, 'I watches football. Rudi like sports. '),
(41, 'Mike like sports. Mike loves flowers. ');

-- --------------------------------------------------------

--
-- Table structure for table `compra`
--

CREATE TABLE `compra` (
  `id_compra` int UNSIGNED NOT NULL,
  `lote` int UNSIGNED NOT NULL,
  `productos_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real_envio` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio real del envio',
  `precio_tarjeta` decimal(14,2) UNSIGNED NOT NULL,
  `precio_envio_unidad` decimal(14,2) UNSIGNED NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL,
  `precio_total_envio` decimal(14,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra`
--

INSERT INTO `compra` (`id_compra`, `lote`, `productos_total`, `sub_total`, `total`, `precio_real_envio`, `precio_tarjeta`, `precio_envio_unidad`, `fecha_hora`, `email`, `precio_total_envio`) VALUES
(1, 1, 2, '3.00', '5.00', '7.00', '8.00', '9.00', '2020-01-19 06:35:51', 'silviopd01@gmail.com', '6.00'),
(2, 1, 1, '1.00', '1.00', '1.00', '1.00', '1.00', '2020-01-19 09:09:26', 'silviopd01@gmail.com', '1.00'),
(3, 99, 9, '9.00', '9.00', '9.00', '9.00', '9.00', '2020-01-20 04:14:29', 'silviopd01@gmail.com', '9.00'),
(4, 3, 3, '3.00', '3.00', '3.00', '3.00', '3.00', '2020-01-20 05:43:15', 'silviopd01@gmail.com', '3.00');

-- --------------------------------------------------------

--
-- Table structure for table `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_compra` int UNSIGNED NOT NULL,
  `id_compra_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED NOT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio_base` decimal(14,2) UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED NOT NULL,
  `envio` decimal(14,2) UNSIGNED NOT NULL,
  `total` decimal(14,2) UNSIGNED NOT NULL,
  `precio_real` decimal(14,2) UNSIGNED NOT NULL COMMENT 'precio_base +''precio_envio_unidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_compra`, `id_compra_detalle`, `id_producto`, `cantidad`, `precio_base`, `sub_total`, `envio`, `total`, `precio_real`) VALUES
(1, 1, 201, 11, '13.00', '14.00', '15.00', '16.00', '17.00'),
(1, 2, 202, 19, '22.00', '23.00', '24.00', '25.00', '26.00'),
(2, 1, 203, 1, '1.00', '1.00', '1.00', '2.00', '2.00'),
(2, 2, 204, 1, '1.00', '1.00', '1.00', '2.00', '2.00'),
(3, 1, 203, 9, '9.00', '81.00', '9.00', '90.00', '18.00'),
(3, 2, 201, 9, '9.00', '81.00', '9.00', '90.00', '18.00'),
(4, 1, 205, 3, '3.00', '9.00', '3.00', '12.00', '6.00'),
(4, 2, 206, 3, '333.00', '999.00', '3.00', '12.00', '336.00');

-- --------------------------------------------------------

--
-- Table structure for table `correlativo`
--

CREATE TABLE `correlativo` (
  `id_correlativo` int UNSIGNED NOT NULL,
  `tabla` varchar(100) NOT NULL,
  `numero` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correlativo`
--

INSERT INTO `correlativo` (`id_correlativo`, `tabla`, `numero`) VALUES
(1, 'compra', 4),
(2, 'venta', 0),
(3, 'producto', 206);

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_producto` int UNSIGNED NOT NULL,
  `id_imagenes` int NOT NULL,
  `url` varchar(100) NOT NULL,
  `foto` char(1) DEFAULT 'S'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(900) DEFAULT NULL,
  `precio` decimal(14,2) UNSIGNED NOT NULL,
  `descuento` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `estado` char(1) NOT NULL DEFAULT 'A',
  `id_subcategoria` int UNSIGNED DEFAULT NULL,
  `id_tipo` int UNSIGNED DEFAULT NULL,
  `id_proveedor` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `cantidad`, `descripcion`, `precio`, `descuento`, `estado`, `id_subcategoria`, `id_tipo`, `id_proveedor`) VALUES
(0, 'Mike like swimming. Mike watches football. ', 1570126088, 'Anne bought new car. Anne is shopping. Tony bought new car. Tony is walking. Tony has free time. ', '730609460287.11', '729.79', 'A', 23, 24, 8),
(1, 'I watches football. I watches football. ', 1935844031, 'John is shopping. John is shopping. John bought new car. John is walking. ', '918714013854.59', '88.17', 'A', 13, 4, 20),
(2, 'Rudi like swimming. I loves flowers. ', 1066918394, 'Anne bought new car. Anne is walking. John bought new car. John has free time. Tony is walking. ', '918607118989.95', '488.42', 'A', 11, 6, 20),
(3, 'Mike watches football. Mike like sports. ', 2117154592, 'John bought new car. Anne is walking. Anne is walking. John is shopping. John is shopping. ', '679557163780.98', '461.38', 'A', 14, 13, 23),
(4, 'Mike watches football. Mike like swimming. ', 1840659868, 'Tony bought new car. Tony has free time. Tony bought new car. ', '78387774502.48', '448.15', 'A', 24, 8, 6),
(5, 'Rudi watches football. Mike watches football. ', 2120470093, 'Anne bought new car. Tony is shopping. Tony is shopping. Tony has free time. Anne bought new car. ', '24813560435.39', '697.01', 'A', 22, 13, 10),
(6, 'I like sports. Rudi loves flowers. ', 489965424, 'Anne bought new car. Anne has free time. John bought new car. ', '699251693474.62', '277.49', 'A', 19, NULL, 14),
(7, 'Mike loves flowers. Mike watches football. ', 160618522, 'John is shopping. John bought new car. ', '804936964329.73', '759.20', 'A', 17, 17, 5),
(8, 'I watches football. I loves flowers. ', 1595919047, 'Anne bought new car. Anne bought new car. Tony bought new car. John bought new car. ', '151232102134.02', '219.03', 'A', NULL, 12, 3),
(9, 'Mike watches football. Mike watches football. ', 2039214544, 'Anne bought new car. Anne is shopping. John bought new car. Tony has free time. John is walking. ', '924581042235.45', '919.49', 'A', 15, 23, 23),
(10, 'Mike watches football. Rudi loves flowers. ', 65526659, 'Tony has free time. Anne is walking. John bought new car. Tony bought new car. Tony is shopping. ', '763835425928.12', '885.17', 'A', 23, 9, 18),
(11, 'I loves flowers. Rudi watches football. ', 1485318616, 'John is walking. John bought new car. John has free time. John has free time. John has free time. ', '3380708894.59', '221.78', 'A', 8, 22, NULL),
(12, 'Mike watches football. I like swimming. ', 1828992267, 'Tony bought new car. Tony is walking. Tony bought new car. Tony has free time. ', '736423184878.18', '882.99', 'A', 12, 10, 12),
(13, 'I watches football. Rudi loves flowers. ', 121863560, 'Anne has free time. Anne bought new car. Tony bought new car. ', '956220097148.83', '439.03', 'A', 20, NULL, 10),
(14, 'Mike like sports. ', 876785565, NULL, '823649095435.57', '392.48', 'A', 19, 23, 17),
(15, 'I watches football. Rudi loves flowers. ', 756749168, 'Anne is walking. Tony has free time. Tony has free time. ', '188682968088.29', '993.74', 'A', 21, 9, 7),
(16, 'Mike like sports. Mike watches football. ', 2133925812, 'Tony is shopping. Anne is walking. Tony is shopping. Tony bought new car. John has free time. ', '326098776020.44', '880.62', 'A', 8, 19, 5),
(17, 'Mike watches football. I loves flowers. ', 1206768426, NULL, '751548412277.31', '282.87', 'A', 16, 15, 11),
(18, 'I watches football. Rudi like sports. ', 1022169017, 'Anne bought new car. Anne bought new car. Tony bought new car. ', '280419535206.11', '247.58', 'A', 25, 5, 5),
(19, 'Mike like sports. Mike loves flowers. ', 106110341, 'John is walking. Anne is walking. John is shopping. Tony has free time. John has free time. ', '230113872691.14', '592.97', 'A', 10, 22, 3),
(20, 'Mike watches football. I like swimming. ', 1250420388, 'Tony bought new car. Tony is walking. ', '807623576165.97', '511.34', 'A', 7, 8, NULL),
(21, 'I loves flowers. Mike like sports. ', 1828030354, 'Tony bought new car. Tony is walking. John bought new car. Anne has free time. Tony is walking. ', '759212574605.99', '77.13', 'A', 20, 20, 14),
(22, 'Mike watches football. Mike watches football. ', 491073319, NULL, '781376502903.20', '40.12', 'A', 11, NULL, 5),
(23, 'Rudi like sports. Rudi like swimming. ', 328964149, 'Tony bought new car. John bought new car. John bought new car. Tony has free time. ', '893084472172.10', '682.38', 'A', 7, 21, NULL),
(24, 'Mike loves flowers. I watches football. ', 2070007347, 'Tony has free time. John is walking. John bought new car. Tony bought new car. ', '524046686017.29', '450.28', 'A', 12, 5, 20),
(25, 'Mike watches football. Mike watches football. ', 499559765, 'Tony bought new car. John is walking. ', '51419792524.92', '354.20', 'A', NULL, 8, 6),
(26, 'I watches football. I watches football. ', 1324227872, 'John is shopping. Anne has free time. John bought new car. Tony is shopping. Anne bought new car. ', '346814947190.93', '301.54', 'A', 13, 9, 14),
(27, 'Mike loves flowers. Mike watches football. ', 1307592918, 'Tony bought new car. John bought new car. ', '480214215640.90', '125.31', 'A', 8, 7, 5),
(28, 'Mike loves flowers. Mike watches football. ', 531757751, NULL, '906581493933.20', '349.39', 'A', 10, 5, 17),
(29, 'Rudi watches football. I watches football. ', 1802512063, 'Anne is shopping. Anne is walking. Tony has free time. Anne bought new car. John has free time. ', '570467018161.25', '691.63', 'A', 8, 18, 5),
(30, 'Mike loves flowers. Rudi loves flowers. ', 1479606320, NULL, '557732251884.91', '202.32', 'A', 20, 5, 6),
(31, 'I like sports. ', 1712955679, 'John bought new car. Anne has free time. John has free time. Tony bought new car. John bought new car. ', '571757063149.32', '34.07', 'A', 22, 6, 19),
(32, 'I watches football. ', 265695738, 'John has free time. Tony is walking. John bought new car. Anne bought new car. John is shopping. ', '897644602376.59', '193.10', 'A', 23, 12, 21),
(33, 'I loves flowers. Rudi watches football. ', 231014531, 'John bought new car. Anne is shopping. Tony bought new car. John is shopping. Anne has free time. ', '214512122683.64', '232.10', 'A', 15, 5, 10),
(34, 'Rudi watches football. ', 70359707, 'John has free time. Anne is walking. Anne has free time. Tony bought new car. John is shopping. ', '323345901478.78', '38.40', 'A', 14, NULL, 9),
(35, 'Mike like swimming. Mike watches football. ', 2002877782, 'Anne has free time. John is walking. Tony is shopping. Anne bought new car. Tony is walking. ', '148131482388.70', '683.36', 'A', 15, 11, 21),
(36, 'I like sports. Rudi like sports. ', 573006457, 'Anne has free time. Anne has free time. Anne bought new car. John bought new car. ', '680841109935.60', '582.59', 'A', 13, 24, 20),
(37, 'I watches football. ', 1190581120, 'Anne bought new car. Tony has free time. John is shopping. Tony has free time. ', '936445569652.74', '832.62', 'A', 13, 11, 7),
(38, 'Mike watches football. ', 488052483, 'Tony is walking. Tony is shopping. Tony has free time. John is shopping. John has free time. ', '561774649667.90', '616.91', 'A', NULL, 8, 11),
(39, 'Mike loves flowers. Rudi watches football. ', 306801119, NULL, '292338860467.66', '649.93', 'A', 11, 23, 20),
(40, 'I loves flowers. Mike like swimming. ', 108721130, 'Tony is walking. John is walking. ', '813386523215.88', '273.57', 'A', 15, 14, 4),
(41, 'Mike like sports. ', 281161585, 'Tony bought new car. Tony has free time. Tony bought new car. ', '335385508560.56', '702.09', 'A', 6, 19, 5),
(42, 'I watches football. ', 124394731, 'John bought new car. Anne is shopping. John bought new car. John has free time. Tony has free time. ', '721891677590.02', '998.22', 'A', 22, 12, 6),
(43, 'Mike loves flowers. Rudi watches football. ', 988508620, 'Tony bought new car. Anne is shopping. Tony is walking. ', '616343589887.71', '475.21', 'A', 20, 13, 5),
(44, 'Mike like swimming. Rudi watches football. ', 1004920843, 'John bought new car. Anne has free time. Tony is walking. John is shopping. Anne bought new car. ', '716805783525.46', '91.52', 'A', NULL, 20, 22),
(45, 'Mike watches football. ', 543558809, 'Anne is walking. Anne is shopping. John is walking. Tony is walking. John has free time. ', '57232941193.06', '857.06', 'A', 16, NULL, 15),
(46, 'Mike like sports. Mike loves flowers. ', 1695099879, 'Anne has free time. Tony is walking. John bought new car. Anne has free time. Anne has free time. ', '450335677313.39', '559.94', 'A', 15, 8, 4),
(47, 'Mike loves flowers. Mike like sports. ', 1657697649, 'Anne bought new car. John has free time. John has free time. Tony has free time. ', '533027791823.32', '492.72', 'A', 11, 9, 3),
(48, 'Rudi watches football. ', 741511677, 'Anne is walking. Anne is walking. John bought new car. Tony is walking. Anne has free time. ', '623352304164.52', '835.53', 'A', 9, 22, 10),
(49, 'Rudi loves flowers. Rudi watches football. ', 1556039312, 'Tony has free time. Anne bought new car. John has free time. Anne bought new car. ', '688887774806.59', '182.75', 'A', 19, NULL, 17),
(50, 'Rudi loves flowers. Mike like swimming. ', 1899249900, 'Anne has free time. John has free time. Tony bought new car. John bought new car. ', '864891831681.84', '28.28', 'A', 22, 23, NULL),
(51, 'Rudi loves flowers. ', 907479117, 'Anne has free time. John has free time. Tony bought new car. John has free time. John is walking. ', '504121680975.86', '184.19', 'A', 21, 5, 21),
(52, 'Mike watches football. Mike watches football. ', 362450710, 'Anne bought new car. John has free time. John is walking. Anne has free time. Anne has free time. ', '498441172522.78', '885.83', 'A', 22, 10, 21),
(53, 'I loves flowers. Rudi watches football. ', 335617623, 'Anne bought new car. Tony bought new car. ', '730140780660.83', '158.96', 'A', 18, 15, 18),
(54, 'Mike loves flowers. I loves flowers. ', 374391246, 'Anne bought new car. Tony is walking. John has free time. ', '833931638287.79', '942.92', 'A', 6, 9, 12),
(55, 'Rudi like sports. Mike like swimming. ', 1209915257, 'Tony has free time. John has free time. John is shopping. Tony has free time. John is walking. ', '434550933147.06', '579.16', 'A', 15, NULL, 13),
(56, 'I watches football. I watches football. ', 208987943, 'Anne bought new car. Anne has free time. Tony has free time. John has free time. ', '163492147951.08', '7.85', 'A', 25, 13, 18),
(57, 'Rudi loves flowers. Rudi like sports. ', 346277032, NULL, '657920277338.00', '573.46', 'A', 10, NULL, 14),
(58, 'I like swimming. I like swimming. ', 1361490460, 'Anne bought new car. Tony is shopping. ', '381326446334.97', '5.88', 'A', 22, 7, 4),
(59, 'Rudi like swimming. Rudi like sports. ', 1133811621, 'John bought new car. John is walking. Anne has free time. Anne is walking. Tony bought new car. ', '454930246404.04', '775.31', 'A', 21, NULL, 14),
(60, 'Mike like swimming. Mike like swimming. ', 61500489, 'John is shopping. Tony is walking. Tony has free time. John has free time. Tony has free time. ', '828929960361.36', '295.35', 'A', 18, 15, 14),
(61, 'I watches football. Rudi watches football. ', 1195502579, 'Tony bought new car. Anne has free time. ', '59486560097.25', '642.64', 'A', 5, 10, NULL),
(62, 'Rudi like swimming. Rudi watches football. ', 1282944731, NULL, '837227092936.12', '876.32', 'A', 13, 22, 6),
(63, 'Mike loves flowers. Mike watches football. ', 1048738717, NULL, '436472178865.29', '593.90', 'A', 18, 19, 23),
(64, 'Mike loves flowers. Rudi loves flowers. ', 1944105894, 'Anne bought new car. Anne bought new car. Tony has free time. John bought new car. Tony has free time. ', '513690627727.53', '281.47', 'A', 15, 18, 3),
(65, 'Mike loves flowers. I watches football. ', 87974585, 'John bought new car. John has free time. John is walking. Tony has free time. John bought new car. ', '304331297896.57', '14.88', 'A', 14, 12, 7),
(66, 'Rudi watches football. I loves flowers. ', 989082393, 'John has free time. John is walking. Tony is shopping. Anne is shopping. Tony has free time. ', '637327653022.44', '333.12', 'A', 20, NULL, 10),
(67, 'Mike watches football. ', 847327837, 'Tony bought new car. Anne has free time. ', '716369022437.41', '769.23', 'A', 24, 23, 20),
(68, 'Mike watches football. ', 1718410612, 'Anne bought new car. Anne bought new car. Tony is walking. Anne has free time. John has free time. ', '375434344571.99', '970.34', 'A', 20, NULL, 5),
(69, 'Mike watches football. Rudi watches football. ', 707442196, 'Tony bought new car. John is shopping. John has free time. Tony is shopping. John bought new car. ', '248952856815.40', '68.80', 'A', 10, 16, 23),
(70, 'Rudi watches football. Mike watches football. ', 1420141612, 'John bought new car. John is walking. Tony is walking. ', '736708258298.67', '82.53', 'A', 21, 7, 10),
(71, 'Rudi like swimming. ', 1573088622, 'Anne is shopping. Anne bought new car. John has free time. John has free time. Tony has free time. ', '676744631036.00', '833.28', 'A', 20, 12, 10),
(72, 'Rudi like swimming. I loves flowers. ', 1514087666, 'John is walking. Anne bought new car. John has free time. Tony bought new car. John bought new car. ', '804766890241.57', '987.06', 'A', 25, 23, 12),
(73, 'I like sports. Rudi watches football. ', 1141365695, 'Anne has free time. Tony bought new car. ', '24342914814.34', '772.38', 'A', 14, 19, 12),
(74, 'Mike watches football. Rudi loves flowers. ', 2074068, 'Anne has free time. John is shopping. Tony is shopping. John has free time. John has free time. ', '449078958816.59', '190.24', 'A', 5, 15, 18),
(75, 'Mike loves flowers. Mike watches football. ', 2039344977, 'John bought new car. Tony has free time. ', '16562346671.24', '27.69', 'A', 25, 6, 19),
(76, 'Mike loves flowers. Mike watches football. ', 773339441, 'Anne has free time. Anne is shopping. John bought new car. Tony is walking. ', '247122986708.41', '61.56', 'A', 5, 10, 6),
(77, 'Mike like swimming. Rudi loves flowers. ', 1352262686, 'John has free time. Anne is walking. John has free time. Anne bought new car. Tony has free time. ', '42933541583.45', '111.69', 'A', NULL, NULL, 4),
(78, 'Mike loves flowers. Rudi watches football. ', 382219873, 'John has free time. Anne has free time. ', '148222056516.42', '809.12', 'A', 20, 16, 9),
(79, 'I like sports. Rudi watches football. ', 1055662347, 'Anne bought new car. Tony bought new car. ', '783413771846.35', '165.22', 'A', 22, NULL, 11),
(80, 'Rudi like swimming. Rudi like sports. ', 1954110780, 'Anne bought new car. John is shopping. Anne bought new car. Tony has free time. Tony has free time. ', '107567092629.09', '806.36', 'A', 11, 12, NULL),
(81, 'Rudi watches football. I like swimming. ', 606581965, 'Anne bought new car. Anne has free time. Tony bought new car. John bought new car. John bought new car. ', '979918022625.77', '762.06', 'A', NULL, 5, 20),
(82, 'I like swimming. Mike watches football. ', 1589777239, 'Tony bought new car. Anne has free time. John bought new car. Anne has free time. Tony is shopping. ', '675658212240.90', '830.72', 'A', 25, NULL, 6),
(83, 'Rudi like sports. Rudi like swimming. ', 579416414, 'Tony is shopping. Tony bought new car. John bought new car. Anne bought new car. Tony bought new car. ', '490902790920.15', '360.72', 'A', 11, 13, 22),
(84, 'Mike watches football. Rudi loves flowers. ', 1390432218, 'Anne bought new car. Tony has free time. Tony bought new car. ', '766617980187.80', '119.69', 'A', 23, 18, 14),
(85, 'Rudi loves flowers. I watches football. ', 1746914044, 'Tony is shopping. Anne has free time. Anne bought new car. Tony has free time. Tony has free time. ', '633273049167.72', '269.64', 'A', 6, 4, 17),
(86, 'Mike loves flowers. Rudi like swimming. ', 1142351084, 'Tony bought new car. John has free time. Anne bought new car. ', '487037177591.56', '146.07', 'A', 11, 8, 6),
(87, 'Rudi like sports. Rudi like sports. ', 79084126, 'John bought new car. Anne is walking. John has free time. John has free time. Tony has free time. ', '937377860218.32', '919.88', 'A', 8, 4, 17),
(88, 'Mike watches football. Rudi watches football. ', 1325399101, 'Anne is walking. Anne is shopping. ', '689305413459.78', '534.12', 'A', 20, 22, 12),
(89, 'Mike watches football. Mike watches football. ', 372966688, 'Anne is walking. John has free time. John bought new car. Tony bought new car. John bought new car. ', '247473387143.21', '426.01', 'A', 11, 24, 20),
(90, 'Mike watches football. Mike loves flowers. ', 232960925, 'Anne bought new car. Anne is walking. John bought new car. John bought new car. John has free time. ', '539480851592.90', '444.20', 'A', 15, 5, 10),
(91, 'Rudi like swimming. Mike like sports. ', 1254748079, 'Anne bought new car. Tony bought new car. Tony bought new car. Tony bought new car. Anne is shopping. ', '663054850706.06', '509.01', 'A', 6, 19, 16),
(92, 'Rudi watches football. Mike watches football. ', 835121724, NULL, '845018916134.73', '587.12', 'A', NULL, 23, 5),
(93, 'Rudi watches football. ', 1859548706, 'John has free time. Anne is shopping. Anne bought new car. Anne has free time. Tony is walking. ', '97804865602.84', '968.82', 'A', 23, 16, NULL),
(94, 'Mike watches football. Mike loves flowers. ', 1130472073, NULL, '673083481310.82', '863.33', 'A', 18, 23, NULL),
(95, 'Rudi loves flowers. Mike like sports. ', 1930974551, 'Tony bought new car. Tony is walking. John has free time. John has free time. John is walking. ', '335111684073.85', '906.86', 'A', NULL, NULL, 10),
(96, 'Mike watches football. Rudi watches football. ', 1898227307, 'Tony has free time. Anne has free time. Tony bought new car. ', '172309758885.51', '553.15', 'A', 12, 24, 7),
(97, 'Mike loves flowers. I watches football. ', 660422257, 'Anne bought new car. Anne is walking. John bought new car. John has free time. John has free time. ', '875345132949.71', '969.01', 'A', 17, 7, 3),
(98, 'Mike watches football. Mike like sports. ', 803272158, 'Anne bought new car. John is walking. John bought new car. Tony is shopping. Tony bought new car. ', '771999307897.21', '337.99', 'A', 10, 5, 11),
(99, 'Rudi like swimming. Mike loves flowers. ', 888995136, 'Anne is walking. Anne bought new car. Anne bought new car. Tony bought new car. ', '734125234512.64', '287.20', 'A', 18, 21, 13),
(100, 'Mike loves flowers. I loves flowers. ', 1587444410, NULL, '620659432320.33', '933.30', 'A', 16, 16, NULL),
(101, 'I watches football. Rudi watches football. ', 1874554653, 'Anne is shopping. Anne is walking. John has free time. ', '687095698056.89', '488.97', 'A', 9, 13, 10),
(102, 'Mike watches football. Mike like sports. ', 1941200105, 'Anne bought new car. Anne is walking. ', '304025649607.22', '203.84', 'A', 8, 17, NULL),
(103, 'Mike watches football. ', 577719842, 'Anne bought new car. John bought new car. John bought new car. Tony is shopping. ', '430259833327.01', '956.18', 'A', 20, 18, 11),
(104, 'I like swimming. Rudi loves flowers. ', 702909943, 'Anne is shopping. John is walking. Tony bought new car. Anne is shopping. John has free time. ', '380328631153.04', '721.77', 'A', 9, NULL, 22),
(105, 'I watches football. ', 1945301976, 'Tony is shopping. Anne bought new car. Tony bought new car. Anne has free time. Tony has free time. ', '291413168353.45', '188.82', 'A', 22, 19, 18),
(106, 'Mike watches football. ', 1399883845, 'Tony bought new car. Anne has free time. Anne has free time. John is shopping. John has free time. ', '932315108952.92', '478.58', 'A', 13, NULL, 18),
(107, 'Mike like sports. Rudi watches football. ', 148132249, 'Anne bought new car. Tony is walking. John bought new car. John has free time. Anne has free time. ', '683905867609.40', '952.11', 'A', 24, 23, 17),
(108, 'Rudi loves flowers. Mike watches football. ', 465458355, 'John bought new car. John is walking. Anne bought new car. Anne has free time. ', '271753288050.27', '780.14', 'A', NULL, 22, NULL),
(109, 'Mike watches football. Rudi like sports. ', 688620863, 'Anne bought new car. John is shopping. John is shopping. John has free time. ', '525801379704.91', '559.43', 'A', 21, 22, NULL),
(110, 'I loves flowers. ', 2110522593, 'Tony bought new car. Anne bought new car. Tony has free time. John is walking. Anne has free time. ', '768827719640.19', '232.93', 'A', 21, 22, NULL),
(111, 'Mike watches football. Rudi watches football. ', 2060683647, 'Anne bought new car. Tony is walking. Tony bought new car. Tony has free time. ', '583006034945.06', '686.22', 'A', 21, 15, 19),
(112, 'Mike loves flowers. Mike watches football. ', 1185610085, NULL, '907555108986.67', '632.83', 'A', 8, NULL, NULL),
(113, 'Mike loves flowers. I like swimming. ', 45523759, 'Tony is shopping. Anne has free time. John has free time. ', '752290192225.93', '373.76', 'A', 11, 21, 8),
(114, 'Mike watches football. Rudi watches football. ', 1269260491, 'Anne is shopping. Anne has free time. Tony bought new car. John is walking. Tony has free time. ', '496359921718.25', '646.60', 'A', 25, 16, NULL),
(115, 'I watches football. Mike like sports. ', 2098745342, 'Tony bought new car. John is shopping. John bought new car. John bought new car. John bought new car. ', '938095922659.60', '98.13', 'A', 11, 24, 20),
(116, 'Rudi watches football. Rudi watches football. ', 1206486192, NULL, '163085595957.99', '596.03', 'A', 9, 8, 17),
(117, 'Rudi watches football. Rudi loves flowers. ', 555783838, 'Tony has free time. John is shopping. Tony bought new car. Tony has free time. Tony has free time. ', '545040893515.91', '758.65', 'A', 7, 10, NULL),
(118, 'Rudi like swimming. I watches football. ', 215848669, 'Anne has free time. John is shopping. Tony has free time. Anne has free time. Anne has free time. ', '24464550449.17', '677.78', 'A', 6, 13, NULL),
(119, 'Mike watches football. I loves flowers. ', 1141423039, 'Tony has free time. John is walking. Anne has free time. John bought new car. Tony bought new car. ', '996124469269.16', '406.49', 'A', 8, 12, NULL),
(120, 'Mike watches football. I like swimming. ', 343330106, 'John is shopping. John bought new car. John is shopping. John has free time. Tony bought new car. ', '695524623173.73', '451.01', 'A', 14, 21, 5),
(121, 'I watches football. Mike like swimming. ', 478641203, 'Anne bought new car. Anne has free time. John has free time. ', '775482182653.45', '34.21', 'A', 25, 7, 10),
(122, 'Mike loves flowers. Mike watches football. ', 1201521543, NULL, '564301505792.59', '731.04', 'A', 6, 11, 5),
(123, 'Mike loves flowers. Rudi like swimming. ', 1218121724, 'John bought new car. Anne is shopping. John has free time. Tony has free time. John has free time. ', '605409133461.65', '611.16', 'A', 12, NULL, 21),
(124, 'Mike loves flowers. Rudi watches football. ', 854011968, NULL, '736905799065.82', '792.65', 'A', 15, 16, 8),
(125, 'Rudi loves flowers. Mike watches football. ', 1313953456, 'Anne is shopping. Anne is shopping. John is shopping. ', '712194679122.91', '62.19', 'A', NULL, 22, 17),
(126, 'Mike watches football. Mike loves flowers. ', 1731142010, 'Anne is shopping. John has free time. Tony has free time. Tony has free time. John is shopping. ', '8436547958.50', '375.11', 'A', 16, 12, 18),
(127, 'Mike watches football. Rudi watches football. ', 915583316, 'Anne has free time. Anne is shopping. Tony bought new car. John has free time. ', '66619541881.97', '339.66', 'A', 16, NULL, 15),
(128, 'Rudi like swimming. I loves flowers. ', 738595407, 'Tony is walking. Anne is walking. ', '153042230965.00', '787.11', 'A', 23, 18, NULL),
(129, 'I like sports. Mike loves flowers. ', 1434069212, 'John has free time. Anne has free time. Tony bought new car. John bought new car. John bought new car. ', '745926153395.31', '425.19', 'A', 24, 22, 12),
(201, '10', 20, '12', '13.00', '0.00', 'A', 5, 4, 3),
(202, '18', 19, '20', '21.00', '0.00', 'A', 5, 4, 3),
(203, '11', 10, '1', '1.00', '0.00', 'A', 5, 4, 3),
(204, '1111', 1, '1', '1.00', '0.00', 'A', 5, 4, 3),
(205, '3', 3, '333', '3.00', '0.00', 'A', 5, 4, 3),
(206, '33', 3, '333', '3.00', '0.00', 'A', 5, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `url`) VALUES
(3, '1212', 'asdfasdf'),
(4, 'Mike like swimming. Mike watches football. ', 'http://vgodu.local72/pgrqr/ssa.php'),
(5, 'I watches football. I watches football. ', 'http://ojkk.web5/lstmu/hxfh.htm'),
(6, 'Rudi like swimming. I loves flowers. ', 'http://nay.net8/thqun/ped.htm'),
(7, 'Mike watches football. Mike like sports. ', 'http://ict.net0/afmkz/yxzvp.html'),
(8, 'Mike watches football. Mike like swimming. ', 'http://sfue.net75/sykqj/csv.aspx'),
(9, 'Rudi watches football. Mike watches football. ', 'http://pxfa.net10/eahao/grv.html'),
(10, 'I like sports. Rudi loves flowers. ', 'http://cex.localb/lgeho/kwpfp.php'),
(11, 'Mike loves flowers. Mike watches football. ', 'http://ouv.net/vaxya/tskou.html'),
(12, 'I watches football. I loves flowers. ', 'http://lyk.web4/kvtrd/djq.aspx'),
(13, 'Mike watches football. Mike watches football. ', 'http://zhq.web08/inohi/siq.htm'),
(14, 'Mike watches football. Rudi loves flowers. ', 'http://spuf.netr/nvqnh/svk.aspx'),
(15, 'I loves flowers. Rudi watches football. ', 'http://upl.netp6/qgjpw/jkku.php'),
(16, 'Mike watches football. I like swimming. ', 'http://hqkm.local/woxtl/bgupo/ebp.html'),
(17, 'I watches football. Rudi loves flowers. ', 'http://yympt.web54/tzqpd/hgmb.html'),
(18, 'Mike like sports. ', 'http://sfl.netv6/kcogg/cbcvi/nmg.htm'),
(19, 'I watches football. Rudi loves flowers. ', 'http://bnx.web/arcyy/ejhex/msu.htm'),
(20, 'Mike like sports. Mike watches football. ', 'http://qje.local81/mwors/mumf.htm'),
(21, 'Mike watches football. I loves flowers. ', 'http://ujtvt.local/oxhym/eywp.htm'),
(22, 'I watches football. Rudi like sports. ', 'http://vfsis.local/fkvnq/jydbr.php'),
(23, 'Mike like sports. Mike loves flowers. ', 'http://qfmmd.local4/olflh/rfi.htm');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_categoria` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre`, `id_categoria`) VALUES
(5, 'aa', 1),
(6, 'Mike like swimming. Mike watches football. ', 25),
(7, 'I watches football. I watches football. ', 1),
(8, 'Rudi like swimming. I loves flowers. ', 38),
(9, 'Mike watches football. Mike like sports. ', 1),
(10, 'Mike watches football. Mike like swimming. ', 40),
(11, 'Rudi watches football. Mike watches football. ', 28),
(12, 'I like sports. Rudi loves flowers. ', 30),
(13, 'Mike loves flowers. Mike watches football. ', NULL),
(14, 'I watches football. I loves flowers. ', 35),
(15, 'Mike watches football. Mike watches football. ', 40),
(16, 'Mike watches football. Rudi loves flowers. ', 1),
(17, 'I loves flowers. Rudi watches football. ', 34),
(18, 'Mike watches football. I like swimming. ', 38),
(19, 'I watches football. Rudi loves flowers. ', 2),
(20, 'Mike like sports. ', 27),
(21, 'I watches football. Rudi loves flowers. ', 26),
(22, 'Mike like sports. Mike watches football. ', 41),
(23, 'Mike watches football. I loves flowers. ', 31),
(24, 'I watches football. Rudi like sports. ', 29),
(25, 'Mike like sports. Mike loves flowers. ', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int UNSIGNED NOT NULL,
  `nombre_es` varchar(100) NOT NULL,
  `nombre_en` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre_es`, `nombre_en`) VALUES
(4, 'ss', 'fdf'),
(5, 'Mike like swimming. Mike watches football. ', 'Mike like sports. Mike like sports. '),
(6, 'I watches football. I watches football. ', 'Mike loves flowers. '),
(7, 'Rudi like swimming. I loves flowers. ', 'Mike like swimming. Rudi watches football. '),
(8, 'Mike watches football. Mike like sports. ', 'Rudi like swimming. Mike like sports. '),
(9, 'Mike watches football. Mike like swimming. ', 'Mike watches football. Rudi watches football. '),
(10, 'Rudi watches football. Mike watches football. ', 'Mike loves flowers. Mike watches football. '),
(11, 'I like sports. Rudi loves flowers. ', 'Mike loves flowers. Mike watches football. '),
(12, 'Mike loves flowers. Mike watches football. ', 'Mike like swimming. Rudi loves flowers. '),
(13, 'I watches football. I loves flowers. ', 'Mike like sports. Rudi watches football. '),
(14, 'Mike watches football. Mike watches football. ', 'Mike loves flowers. Mike watches football. '),
(15, 'Mike watches football. Rudi loves flowers. ', 'Rudi watches football. Rudi loves flowers. '),
(16, 'I loves flowers. Rudi watches football. ', 'I watches football. '),
(17, 'Mike watches football. I like swimming. ', 'I watches football. Mike watches football. '),
(18, 'I watches football. Rudi loves flowers. ', NULL),
(19, 'Mike like sports. ', 'I loves flowers. Rudi watches football. '),
(20, 'I watches football. Rudi loves flowers. ', 'Mike loves flowers. Mike loves flowers. '),
(21, 'Mike like sports. Mike watches football. ', NULL),
(22, 'Mike watches football. I loves flowers. ', 'Mike like swimming. Mike loves flowers. '),
(23, 'I watches football. Rudi like sports. ', 'I like sports. Mike watches football. '),
(24, 'Mike like sports. Mike loves flowers. ', 'I loves flowers. ');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `estado`) VALUES
('christianjcd2512@hotmail.com', '00c9e9fcba35b05178246d24f85aaf20', 'A'),
('silviopd01@gmail.com', '05ce1b8ccca10743785becff19eb43e1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

CREATE TABLE `venta` (
  `id_venta` int UNSIGNED NOT NULL,
  `producto_total` int UNSIGNED NOT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_venta` int UNSIGNED NOT NULL,
  `id_venta_detalle` int UNSIGNED NOT NULL,
  `id_producto` int UNSIGNED DEFAULT NULL,
  `cantidad` int UNSIGNED NOT NULL,
  `precio` decimal(14,2) UNSIGNED DEFAULT NULL,
  `sub_total` decimal(14,2) UNSIGNED DEFAULT NULL,
  `descuento` decimal(5,2) UNSIGNED DEFAULT '0.00',
  `total` decimal(14,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `fk_compra_usuario` (`email`);

--
-- Indexes for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_compra`,`id_compra_detalle`),
  ADD KEY `fk_compra_detalle_producto_0` (`id_producto`);

--
-- Indexes for table `correlativo`
--
ALTER TABLE `correlativo`
  ADD PRIMARY KEY (`id_correlativo`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_producto`,`id_imagenes`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_producto_subcategoria` (`id_subcategoria`),
  ADD KEY `fk_producto_tipo` (`id_tipo`),
  ADD KEY `fk_producto_proveedor_0` (`id_proveedor`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `fk_subcategoria_categoria` (`id_categoria`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_venta_usuario` (`email`);

--
-- Indexes for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_venta`,`id_venta_detalle`),
  ADD KEY `fk_venta_detalle_producto_0` (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `correlativo`
--
ALTER TABLE `correlativo`
  MODIFY `id_correlativo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_detalle_compra` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id_compra`),
  ADD CONSTRAINT `fk_compra_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_imagenes_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_proveedor_0` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`),
  ADD CONSTRAINT `fk_producto_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id_tipo`);

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Constraints for table `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`);

--
-- Constraints for table `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_venta_detalle_producto_0` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `fk_venta_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
