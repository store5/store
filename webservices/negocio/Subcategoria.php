<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Subcategoria extends Conexion
{

    private $id_categoria, $nombre, $id_subcategoria;

    function getId_categoria()
    {
        return $this->id_categoria;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getIdSubcategoria()
    {
        return $this->id_subcategoria;
    }

    public function setIdSubcategoria($id_subcategoria)
    {
        $this->id_subcategoria = $id_subcategoria;
    }

    public function listar()
    {
        try {
            $sql = "select s.id_subcategoria as id_subcategoria,s.nombre as nombre_subcategoria,s.id_categoria as id_categoria,c.nombre as nombre_categoria from store.subcategoria s inner join categoria c where s.id_categoria=c.id_categoria order by 2 asc";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function eliminar()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "delete from store.subcategoria where id_subcategoria = :p_id_subcategoria ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_subcategoria", $this->getIdSubcategoria());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }

        return false;
    }

    public function agregar()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "INSERT INTO store.subcategoria(nombre,id_categoria) VALUES( :p_nombre, :p_id_categoria  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", strtoupper($this->getNombre()));
            $sentencia->bindValue(":p_id_categoria", $this->getId_categoria());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }

        return false;
    }

    public function leerDatos($p_id_subcategoria)
    {
        try {
            $sql = "SELECT * FROM store.subcategoria where id_subcategoria = :p_id_subcategoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_subcategoria", $p_id_subcategoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function editar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE store.subcategoria SET nombre=:p_nombre, id_categoria=:p_id_categoria where id_subcategoria=:p_id_subcategoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $this->getNombre());
            $sentencia->bindValue(":p_id_categoria", $this->getId_categoria());
            $sentencia->bindValue(":p_id_subcategoria", $this->getIdSubcategoria());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $ex);
        }
        return false;
    }

    public function listarSubcategoriaCategoria()
    {
        try {
            $sql = "SELECT s.id_subcategoria, s.nombre as nombre_subcategoria, s.id_categoria, c.nombre as nombre_categoria
                    FROM store.subcategoria s 
                        INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )  ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
}
