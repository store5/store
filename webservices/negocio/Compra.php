<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Compra extends Conexion
{

    private $id_compra,
        $lote,
        $productos_total,
        $sub_total,
        $total,
        $precio_total_envio,
        $precio_real_envio,
        $precio_tarjeta,
        $precio_envio_unidad,
        $fecha_hora,
        $email,
        $detalleCompra;

    private $id_producto;

    public function getIdCompra()
    {
        return $this->id_compra;
    }

    public function setIdCompra($id_compra)
    {
        $this->id_compra = $id_compra;
    }

    public function getLote()
    {
        return $this->lote;
    }

    public function setLote($lote)
    {
        $this->lote = $lote;
    }

    public function getProductosTotal()
    {
        return $this->productos_total;
    }

    public function setProductosTotal($productos_total)
    {
        $this->productos_total = $productos_total;
    }

    public function getSubTotal()
    {
        return $this->sub_total;
    }

    public function setSubTotal($sub_total)
    {
        $this->sub_total = $sub_total;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getPrecioTotalEnvio()
    {
        return $this->precio_total_envio;
    }

    public function setPrecioTotalEnvio($precio_total_envio)
    {
        $this->precio_total_envio = $precio_total_envio;
    }

    public function getPrecioRealEnvio()
    {
        return $this->precio_real_envio;
    }

    public function setPrecioRealEnvio($precio_real_envio)
    {
        $this->precio_real_envio = $precio_real_envio;
    }

    public function getPrecioTarjeta()
    {
        return $this->precio_tarjeta;
    }

    public function setPrecioTarjeta($precio_tarjeta)
    {
        $this->precio_tarjeta = $precio_tarjeta;
    }

    public function getPrecioEnvioUnidad()
    {
        return $this->precio_envio_unidad;
    }

    public function setPrecioEnvioUnidad($precio_envio_unidad)
    {
        $this->precio_envio_unidad = $precio_envio_unidad;
    }

    public function getFechaHora()
    {
        return $this->fecha_hora;
    }

    public function setFechaHora($fecha_hora)
    {
        $this->fecha_hora = $fecha_hora;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getDetalleCompra()
    {
        return $this->detalleCompra;
    }

    public function setDetalleCompra($detalleCompra)
    {
        $this->detalleCompra = $detalleCompra;
    }

    public function getIdProducto()
    {
        return $this->id_producto;
    }

    public function setIdProducto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    public function agregar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "CALL f_correlativo('compra')";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();

            if ($sentencia->rowCount()) {
                $nuevoNumeroCompra = $resultado["numero"];
                $this->setIdCompra($nuevoNumeroCompra);

                $sql = "INSERT INTO store.compra( 
                                                id_compra, 
                                                lote, 
                                                productos_total, 
                                                sub_total, 
                                                total, 
                                                precio_total_envio, 
                                                precio_real_envio, 
                                                precio_tarjeta, 
                                                precio_envio_unidad, 
                                                email) 
                                        VALUES ( 
                                                :p_id_compra, 
                                                :p_lote, 
                                                :p_productos_total, 
                                                :p_sub_total, 
                                                :p_total, 
                                                :p_precio_total_envio, 
                                                :p_precio_real_envio, 
                                                :p_precio_tarjeta, 
                                                :p_precio_envio_unidad, 
                                                :p_email);";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindValue(":p_id_compra", $this->getIdCompra());
                $sentencia->bindValue(":p_lote", $this->getLote());
                $sentencia->bindValue(":p_productos_total", $this->getProductosTotal());
                $sentencia->bindValue(":p_sub_total", $this->getSubTotal());
                $sentencia->bindValue(":p_total", $this->getTotal());
                $sentencia->bindValue(":p_precio_total_envio", $this->getPrecioTotalEnvio());
                $sentencia->bindValue(":p_precio_real_envio", $this->getPrecioRealEnvio());
                $sentencia->bindValue(":p_precio_tarjeta", $this->getPrecioTarjeta());
                $sentencia->bindValue(":p_precio_envio_unidad", $this->getPrecioEnvioUnidad());
                $sentencia->bindValue(":p_email", $this->getEmail());
                $sentencia->execute();

                /* INSERTAR EN LA TABLA VENTA_DETALLE */
                $detalleVentaArray = json_decode($this->getDetalleCompra()); //Convertir de formato JSON a formato array

                $item = 0;

                foreach ($detalleVentaArray as $key => $value) {
                    $item++;

                    $sql = "CALL f_correlativo('producto')";
                    $sentencia = $this->dblink->prepare($sql);
                    $sentencia->execute();
                    $resultado = $sentencia->fetch();

                    if ($sentencia->rowCount()) {
                        $nuevoidproducto = $resultado["numero"];
                        $this->setIdProducto($nuevoidproducto);
                    }

                    $sql = "CALL f_producto_buscar_compra(:p_nombre)";
                    $sentencia = $this->dblink->prepare($sql);
                    $sentencia->bindValue(":p_nombre", $value->nombre);
                    $sentencia->execute();
                    $resultado = $sentencia->fetch();

                    if ($sentencia->rowCount()) {
                        if ($resultado["numero"] == -1) {
                            $sql = "INSERT INTO store.producto( 
                                                                id_producto,
                                                                nombre, 
                                                                cantidad, 
                                                                descripcion, 
                                                                id_subcategoria, 
                                                                id_tipo, 
                                                                id_proveedor) 
                                                            VALUES ( 
                                                                :p_id_producto, 
                                                                :p_nombre, 
                                                                :p_cantidad, 
                                                                :p_descripcion, 
                                                                :p_id_subcategoria, 
                                                                :p_id_tipo, 
                                                                :p_id_proveedor)";

                            $sentencia = $this->dblink->prepare($sql);
                            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
                            $sentencia->bindValue(":p_nombre", strtoupper($value->nombre));
                            $sentencia->bindValue(":p_cantidad", $value->cantidad);
                            $sentencia->bindValue(":p_descripcion", $value->descripcion);
                            $sentencia->bindValue(":p_id_subcategoria", $value->id_subcategoria);
                            $sentencia->bindValue(":p_id_tipo", $value->id_tipo);
                            $sentencia->bindValue(":p_id_proveedor", $value->id_proveedor);
                            $sentencia->execute();

                            $sql = "update correlativo set numero = numero + 1 where tabla = 'producto'";
                            $sentencia = $this->dblink->prepare($sql);
                            $sentencia->execute();

                            $sql = "INSERT INTO store.compra_detalle( 
                                                                    id_compra, 
                                                                    id_compra_detalle, 
                                                                    id_producto, 
                                                                    cantidad, 
                                                                    precio_base, 
                                                                    sub_total, 
                                                                    envio, 
                                                                    total, 
                                                                    precio_real) 
                                                                VALUES( 
                                                                    :p_id_compra, 
                                                                    :p_id_compra_detalle, 
                                                                    :p_id_producto, 
                                                                    :p_cantidad, 
                                                                    :p_precio_base, 
                                                                    :p_sub_total, 
                                                                    :p_envio, 
                                                                    :p_total, 
                                                                    :p_precio_real) ";

                            $sentencia = $this->dblink->prepare($sql);
                            $sentencia->bindValue(":p_id_compra", $this->getIdCompra());
                            $sentencia->bindValue(":p_id_compra_detalle", $item);
                            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
                            $sentencia->bindValue(":p_cantidad", $value->cantidad);
                            $sentencia->bindValue(":p_precio_base", $value->precio_base);
                            $sentencia->bindValue(":p_sub_total", $value->sub_total);
                            $sentencia->bindValue(":p_envio", $value->envio);
                            $sentencia->bindValue(":p_total", $value->total);
                            $sentencia->bindValue(":p_precio_real", $value->precio_real);
                            $sentencia->execute();
                        } else {
                            $this->setIdProducto($resultado["numero"]);

                            $sql = "update producto set cantidad=cantidad + :p_cantidad where id_producto=:p_id_producto";
                            $sentencia = $this->dblink->prepare($sql);
                            $sentencia->bindValue(":p_cantidad", $value->cantidad);
                            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
                            $sentencia->execute();

                            $sql = "INSERT INTO store.compra_detalle( 
                                                                    id_compra, 
                                                                    id_compra_detalle, 
                                                                    id_producto, 
                                                                    cantidad, 
                                                                    precio_base, 
                                                                    sub_total, 
                                                                    envio, 
                                                                    total, 
                                                                    precio_real) 
                                                                VALUES( 
                                                                    :p_id_compra, 
                                                                    :p_id_compra_detalle, 
                                                                    :p_id_producto, 
                                                                    :p_cantidad, 
                                                                    :p_precio_base, 
                                                                    :p_sub_total, 
                                                                    :p_envio, 
                                                                    :p_total, 
                                                                    :p_precio_real) ";

                            $sentencia = $this->dblink->prepare($sql);
                            $sentencia->bindValue(":p_id_compra", $this->getIdCompra());
                            $sentencia->bindValue(":p_id_compra_detalle", $item);
                            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
                            $sentencia->bindValue(":p_cantidad", $value->cantidad);
                            $sentencia->bindValue(":p_precio_base", $value->precio_base);
                            $sentencia->bindValue(":p_sub_total", $value->sub_total);
                            $sentencia->bindValue(":p_envio", $value->envio);
                            $sentencia->bindValue(":p_total", $value->total);
                            $sentencia->bindValue(":p_precio_real", $value->precio_real);
                            $sentencia->execute();
                        }
                    }
                }

                $sql = "update correlativo set numero = numero + 1 where tabla = 'compra'";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();

                //Terminar la transacción
                $this->dblink->commit();

                return true;
            } else {
                throw new Exception("Error ....");
            }
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception($exc);
        }
        return false;
    }

}