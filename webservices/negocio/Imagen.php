<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Imagen extends Conexion {

    private $id_producto, $id_imagenes, $url;

    public function getIdProducto()
    {
        return $this->id_producto;
    }

    public function setIdProducto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    public function getIdImagenes()
    {
        return $this->id_imagenes;
    }

    public function setIdImagenes($id_imagenes)
    {
        $this->id_imagenes = $id_imagenes;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        
        try {
            
            $sql = "INSERT INTO store.imagenes(id_producto,id_imagenes,url) VALUES( :p_id_producto,:p_imagenes,:p_url  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
            $sentencia->bindValue(":p_imagenes", $this->getIdImagenes());
            $sentencia->bindValue(":p_url", $this->getUrl());
            $sentencia->execute();

            $sql = "UPDATE store.producto SET foto = 'S' WHERE id_producto = :p_id_producto;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
            $sentencia->execute();

            $this->dblink->commit();
            
            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }
        
        return false;
    }

    public function actualizar() {
        $this->dblink->beginTransaction();

        try {
            $sql = "UPDATE store.producto SET foto = 'S' WHERE id_producto = :p_id_producto;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }

        return false;
    }
    
}
