<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Proveedor extends Conexion {
    
    private $id_proveedor, $nombre, $url;
    
    public function getIdproveedor() {
        return $this->id_proveedor;
    }
    
    public function setIdproveedor($id_proveedor) {
        $this->id_proveedor = $id_proveedor;
    }
    
    public function getNombre() {
        return $this->nombre;
    }
    
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function getUrl() {
        return $this->url;
    }
    
    public function setUrl($url) {
        $this->url = $url;
    }
    
    public function listar() {
        try {
            $sql = "select * from store.proveedor";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
    
    public function eliminar() {
        $this->dblink->beginTransaction();
        
        try {
            
            $sql = "delete from store.proveedor where id_proveedor = :p_id_proveedor ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_proveedor", $this->getIdproveedor());
            $sentencia->execute();
            
            $this->dblink->commit();
            
            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }
        
        return false;
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        
        try {
            
            $sql = "INSERT INTO store.proveedor(nombre, url) VALUES( :p_nombre, :p_url  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", strtoupper($this->getNombre()));
            $sentencia->bindValue(":p_url", strtoupper($this->getUrl()));
            $sentencia->execute();
    
            $this->dblink->commit();
            
            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }
        
        return false;
    }
    
    public function leerDatos($p_id_proveedor) {
        try {
            $sql = "SELECT * FROM store.proveedor where id_proveedor = :p_id_proveedor";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_proveedor", $p_id_proveedor);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE store.proveedor SET nombre=:p_nombre, url=:p_url where id_proveedor=:p_id_proveedor";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $this->getNombre());
            $sentencia->bindValue(":p_url", $this->getUrl());
            $sentencia->bindValue(":p_id_proveedor", $this->getIdproveedor());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $ex);
        }
        return false;
    }
    
}
