<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Tipo extends Conexion
{

    private $id_tipo, $nombre_es, $nombre_en;

    public function getIdTipo()
    {
        return $this->id_tipo;
    }

    public function setIdTipo($id_tipo)
    {
        $this->id_tipo = $id_tipo;
    }

    public function getNombreEs()
    {
        return $this->nombre_es;
    }

    public function setNombreEs($nombre_es)
    {
        $this->nombre_es = $nombre_es;
    }

    public function getNombreEn()
    {
        return $this->nombre_en;
    }

    public function setNombreEn($nombre_en)
    {
        $this->nombre_en = $nombre_en;
    }

    public function listar($sub)
    {
        try {
            if ($sub == '') {
                $sql = "select * from store.tipo ORDER BY nombre_en ASC";
            } else {
                $sql = "SELECT t.id_tipo,s.id_subcategoria, s.nombre, t.nombre_es, t.nombre_en
                        FROM store.producto p
                            INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )
                            INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        WHERE s.id_subcategoria=" . $sub."
                        ORDER BY t.nombre_en ASC";
            }
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function eliminar()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "delete from store.tipo where id_tipo = :p_id_tipo ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_tipo", $this->getIdTipo());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }

        return false;
    }

    public function agregar()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "INSERT INTO store.tipo(nombre_es, nombre_en) VALUES( :p_nombre_es, :p_nombre_en  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre_es", strtoupper($this->getNombreEs()));
            $sentencia->bindValue(":p_nombre_en", strtoupper($this->getNombreEn()));
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }

        return false;
    }

    public function leerDatos($p_id_tipo)
    {
        try {
            $sql = "SELECT * FROM store.tipo where id_tipo = :p_id_tipo";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_tipo", $p_id_tipo);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function editar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE store.tipo SET nombre_es=:p_nombre_es, nombre_en=:p_nombre_en where id_tipo=:p_id_tipo";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre_es", $this->getNombreEs());
            $sentencia->bindValue(":p_nombre_en", $this->getNombreEn());
            $sentencia->bindValue(":p_id_tipo", $this->getIdTipo());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $ex);
        }
        return false;
    }

    public function searchTipo($search)
    {
        try {
            $sql = "SELECT 
                           t.id_tipo,
                           t.nombre_es,
                           t.nombre_en
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                        INNER JOIN categoria c on s.id_categoria = c.id_categoria
                    WHERE
                        p.nombre like concat('%%',:p_search,'%%') OR
                        t.nombre_es like concat('%%',:p_search,'%%') OR
                        t.nombre_en like concat('%%',:p_search,'%%') OR
                        s.nombre like concat('%%',:p_search,'%%') OR
                        c.nombre like concat('%%',:p_search,'%%')
                    ORDER BY 3 ASC";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_search", $search);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
}
