<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Categoria extends Conexion {
    
    private $id_categoria, $nombre;
    
    function getId_categoria() {
        return $this->id_categoria;
    }
    
    function getNombre() {
        return $this->nombre;
    }
    
    function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }
    
    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function listar() {
        try {
            $sql = "select * from store.categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
    
    public function eliminar() {
        $this->dblink->beginTransaction();
        
        try {
            
            $sql = "delete from store.categoria where id_categoria = :p_id_categoria ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_categoria", $this->getId_categoria());
            $sentencia->execute();
            
            $this->dblink->commit();
            
            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }
        
        return false;
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        
        try {
            
            $sql = "INSERT INTO store.categoria(nombre) VALUES( :p_nombre  );";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", strtoupper($this->getNombre()));
            $sentencia->execute();
            
            $this->dblink->commit();
            
            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $exc);
        }
        
        return false;
    }
    
    public function leerDatos($p_id_categoria) {
        try {
            $sql = "SELECT * FROM store.categoria where id_categoria = :p_id_categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_categoria", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE store.categoria SET nombre=:p_nombre where id_categoria=:p_id_categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $this->getNombre());
            $sentencia->bindValue(":p_id_categoria", $this->getId_categoria());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $ex);
        }
        return false;
    }
    
}
