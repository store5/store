<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Sesion extends Conexion
{

    private $email, $password, $estado;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function validarSesion()
    {
        try {
            $sql = " CALL f_inicio_sesion( :p_email , md5(:p_clave))";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getPassword());
            $sentencia->execute();
            return $sentencia->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            throw $ex;
        }
    }


}