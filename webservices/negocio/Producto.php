<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/datos/Conexion.clase.php';

class Producto extends Conexion
{
    private $id_producto,$nombre,$precio,$descripcion,$estado;
    private $page;

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getIdProducto()
    {
        return $this->id_producto;
    }

    public function setIdProducto($id_producto)
    {
        $this->id_producto = $id_producto;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function listar()
    {
        try {
            $sql = "SELECT
                           p.id_producto,
                           p.nombre as nombre_producto,
                           p.cantidad,
                           p.descripcion,
                           p.precio,
                           p.estado,
                           p.id_subcategoria,
                           p.id_tipo,
                           p.id_proveedor,
                           s.nombre as nombre_subcategoria,
                           c.id_categoria,
                           c.nombre as nombre_categoria,
                           p1.nombre as nombre_proveedor,
                           t.nombre_es as tipo_nombre_es,
                           t.nombre_en as tipo_nombre_en
                    FROM store.producto p
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria  )  
                            INNER JOIN store.categoria c ON ( s.id_categoria = c.id_categoria  )  
                        INNER JOIN store.proveedor p1 ON ( p.id_proveedor = p1.id_proveedor  )  
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )  ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function listarWeb($page,$tipo,$sub)
    {
        try {
            if($tipo != '' && $sub != ''){
                $sql = "SELECT p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo,
                            p.estado
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                    WHERE
                        t.id_tipo in (".$tipo .") AND
                        s.id_subcategoria = ".$sub." AND
                        p.id_producto ORDER BY 10,8 ASC LIMIT ".(($page-1)*60).",60
                    ";
            }
            if($tipo == '' && $sub == ''){
                $sql = "SELECT p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo,
                            p.estado
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                    WHERE
                        p.id_producto ORDER BY 10,8 ASC LIMIT ".(($page-1)*60).",60
                    ";}
            if($tipo != '' && $sub == ''){
                $sql = "SELECT p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo,
                            p.estado
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                    WHERE
                        t.id_tipo in (".$tipo.") AND
                        p.id_producto ORDER BY 10,8 ASC LIMIT ".(($page-1)*60).",60
                    ";
            }
            if($tipo == '' && $sub != ''){
                $sql = "SELECT p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo,
                            p.estado
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                    WHERE
                        s.id_subcategoria = ".$sub." AND
                        p.id_producto ORDER BY 10,8 ASC LIMIT ".(($page-1)*60).",60
                    ";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function productosrelacionados($id_producto)
    {
        try {
            $sql = "CALL f_producto_relativo(:p_page);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_page", $id_producto);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function productoTotal($tipo,$sub)
    {
        try {

            if($tipo != '' && $sub != ''){
                $sql = "select count(*) as cantidad from producto p inner join tipo t on p.id_tipo=t.id_tipo inner join subcategoria s on p.id_subcategoria=s.id_subcategoria where s.id_subcategoria=".$sub." AND t.id_tipo in (".$tipo.")";
            }
            if($tipo == '' && $sub == ''){
                $sql = "select count(*) as cantidad from producto";
            }
            if($tipo != '' && $sub == ''){
                $sql = "select count(*) as cantidad from producto p inner join tipo t on p.id_tipo=t.id_tipo where t.id_tipo in (".$tipo.")";
            }
            if($tipo == '' && $sub != ''){
                $sql = "select count(*) as cantidad from producto p inner join tipo t on p.id_tipo=t.id_tipo inner join subcategoria s on p.id_subcategoria=s.id_subcategoria where s.id_subcategoria=".$sub."";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }


    public function buscar($p_id_categoria)
    {
        try {
            $sql = "CALL f_buscar_producto(:p_nombre)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function buscarfoto($p_id_categoria)
    {
        try {
            $sql = "CALL f_buscar_producto_foto(:p_nombre)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function buscarpornombre($p_id_categoria)
    {
        try {
            $sql = "CALL f_buscar_producto_por_nombre(:p_nombre)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function leerDatos($p_id_categoria)
    {
        try {
            $sql = "SELECT p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           p.estado,                           
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            p.id_tipo,
                            p.id_subcategoria
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                    WHERE
                        p.id_producto = :p_id_producto";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_producto", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function leerfoto($p_id_categoria)
    {
        try {
            $sql = "SELECT * FROM store.imagenes where id_producto = :p_id_categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_categoria", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function leerfotoportada($p_id_categoria)
    {
        try {
            $sql = "SELECT * FROM store.imagenes where id_producto = :p_id_categoria";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_categoria", $p_id_categoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function searchweb($page,$search,$tipo)
    {
        try {
            if($tipo != ''){
                $sql = "
                SELECT tb1.* from
                (SELECT 
                            p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                        INNER JOIN categoria c on s.id_categoria = c.id_categoria
                    WHERE
                        p.nombre like concat('%%',:p_search,'%%') OR
                        t.nombre_es like concat('%%',:p_search,'%%') OR
                        t.nombre_en like concat('%%',:p_search,'%%') OR
                        s.nombre like concat('%%',:p_search,'%%') OR
                        c.nombre like concat('%%',:p_search,'%%') 
                    ORDER BY 10,8 ASC) as tb1
                        where
                        tb1.id_tipo in (".$tipo.") AND
                        tb1.id_producto LIMIT ".(($page-1)*60).",60";
            }
            else{
                $sql = "SELECT 
                            p.id_producto,
                           p.nombre,
                           p.descripcion,
                           p.precio,
                           p.cantidad,
                           p.foto,
                           t.nombre_es,
                           t.nombre_en,
                            s.nombre as nombre_subcategoria,
                            s.id_subcategoria,
                            t.id_tipo
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                        INNER JOIN categoria c on s.id_categoria = c.id_categoria
                    WHERE
                        p.nombre like concat('%%',:p_search,'%%') OR
                        t.nombre_es like concat('%%',:p_search,'%%') OR
                        t.nombre_en like concat('%%',:p_search,'%%') OR
                        s.nombre like concat('%%',:p_search,'%%') OR
                        c.nombre like concat('%%',:p_search,'%%') 
                        ORDER BY 10,8 ASC 
                        LIMIT ".(($page-1)*60).",60
                    ";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_search", $search);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function searchTotal($search,$tipo)
    {
        try {
            if($tipo != ''){
                $sql = "SELECT count(*) as cantidad
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                        INNER JOIN categoria c on s.id_categoria = c.id_categoria
                    WHERE
                        p.nombre like concat('%%',:p_search,'%%') OR
                        t.nombre_es like concat('%%',:p_search,'%%') OR
                        t.nombre_en like concat('%%',:p_search,'%%') OR
                        s.nombre like concat('%%',:p_search,'%%') OR
                        c.nombre like concat('%%',:p_search,'%%') AND  
                        t.id_tipo in (".$tipo.")";
            }
            else{
                $sql = "SELECT count(*) as cantidad
                    FROM store.producto p
                        INNER JOIN store.tipo t ON ( p.id_tipo = t.id_tipo  )
                        INNER JOIN store.subcategoria s ON ( p.id_subcategoria = s.id_subcategoria )
                        INNER JOIN categoria c on s.id_categoria = c.id_categoria
                    WHERE
                        p.nombre like concat('%%',:p_search,'%%') OR
                        t.nombre_es like concat('%%',:p_search,'%%') OR
                        t.nombre_en like concat('%%',:p_search,'%%') OR
                        s.nombre like concat('%%',:p_search,'%%') OR
                        c.nombre like concat('%%',:p_search,'%%')";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_search", $search);
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw new Exception("Error... " . $exc);
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE store.producto SET nombre=:p_nombre, precio=:p_precio,descripcion=:p_descripcion, estado=:p_estado where id_producto=:p_id_producto";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nombre", $this->getNombre());
            $sentencia->bindValue(":p_precio", $this->getPrecio());
            $sentencia->bindValue(":p_descripcion", $this->getDescripcion());
            $sentencia->bindValue(":p_estado", $this->getEstado());
            $sentencia->bindValue(":p_id_producto", $this->getIdProducto());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw new Exception("Error... " . $ex);
        }
        return false;
    }
}
