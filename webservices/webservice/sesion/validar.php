<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/webservices/negocio/Sesion.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/webservices/util/funciones/Funciones.clase.php';

$email = $_POST["email"];
$password = $_POST["password"];

try {
    $objSesion = new Sesion();
    $objSesion->setEmail($email);
    $objSesion->setpassword($password);
    $resultado = $objSesion->validarSesion();   
    
    if ($resultado["estado"]==200) {
        unset($resultado["estado"]);
        
        /*Generar un token de seguridad*/
        require_once $_SERVER['DOCUMENT_ROOT'].'/webservices/webservice/jwt/generar.php';
        $token = generarToken(null,3600);
        $resultado["token"] = $token;
        /*Generar un token de seguridad*/
        
        Funciones::imprimeJSON(200, "Bienvenido a la aplicacion", $resultado);
    } else {
        Funciones::imprimeJSON(500, $resultado["mensaje"], "");
    }
} catch (Exception $exc) {
    
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}