<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Subcategoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

try {
    $obj = new Subcategoria();
    $resultado = $obj->listarSubcategoriaCategoria();
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_subcategoria" => $resultado[$i]["id_subcategoria"],
            "nombre_subcategoria" => $resultado[$i]["nombre_subcategoria"],
            "id_categoria" => $resultado[$i]["id_categoria"],
            "nombre_categoria" => $resultado[$i]["nombre_categoria"]

        );
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
