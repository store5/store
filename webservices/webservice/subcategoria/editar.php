<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Subcategoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_subcategoria = $_POST["id_subcategoria"];
$id_categoria = $_POST["id_categoria"];
$nombre = $_POST["nombre"];

try {
    $obj = new Subcategoria();
    $obj->setIdSubcategoria($id_subcategoria);
    $obj->setId_categoria($id_categoria);
    $obj->setNombre($nombre);
    $resultado = $obj->editar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Modificación Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}