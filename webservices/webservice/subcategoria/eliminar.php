<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Subcategoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_subcategoria = $_POST["id_subcategoria"];

try {
    $obj = new Subcategoria();
    $obj->setIdSubcategoria($id_subcategoria);
    $resultado = $obj->eliminar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Se Elimino Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
