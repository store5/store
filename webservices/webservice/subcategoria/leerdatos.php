<?php
header('Access-Control-Allow-Origin: *');
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Subcategoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_subcategoria = $_POST["id_subcategoria"];

try {
    $obj = new Subcategoria();
    $resultado = $obj->leerDatos($id_subcategoria);
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_subcategoria" => $resultado[$i]["id_subcategoria"],
            "nombre" => $resultado[$i]["nombre"],
            "id_categoria" => $resultado[$i]["id_categoria"]
        );
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
