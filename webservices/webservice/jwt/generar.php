<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/webservices/util/jwt/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/webservices/util/jwt/auth.php';

function generarToken($data=null, $timeToken=3600){
    return Auth::SignIn($data, $timeToken);
}
