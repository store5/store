<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Tipo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$nombre_es = $_POST["nombre_es"];
$nombre_en = $_POST["nombre_en"];

try {
    $obj = new Tipo();
    $obj->setNombreEs($nombre_es);
    $obj->setNombreEn($nombre_en);
    $resultado = $obj->agregar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
