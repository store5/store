<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Tipo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_tipo = $_POST["id_tipo"];

try {
    $obj = new Tipo();
    $obj->setIdTipo($id_tipo);
    $resultado = $obj->eliminar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Se Elimino Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
