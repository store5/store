<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Tipo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_tipo = $_POST["id_tipo"];

try {
    $obj = new Tipo();
    $resultado = $obj->leerDatos($id_tipo);
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array("id_tipo" => $resultado[$i]["id_tipo"], "nombre_es" => $resultado[$i]["nombre_es"], "nombre_en" => $resultado[$i]["nombre_en"]);
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
