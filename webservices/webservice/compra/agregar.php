<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Compra.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$lote = $_POST["lote"];
$productos_total = $_POST["productos_total"];
$sub_total = $_POST["sub_total"];
$total = $_POST["total"];
$precio_total_envio = $_POST["precio_total_envio"];
$precio_real_envio = $_POST["precio_real_envio"];
$precio_tarjeta = $_POST["precio_tarjeta"];
$precio_envio_unidad = $_POST["precio_envio_unidad"];
$email = $_POST["email"];
$detalleCompra = $_POST["detalleCompra"];

try {
    $obj = new Compra();
    $obj->setLote($lote);
    $obj->setProductosTotal($productos_total);
    $obj->setSubTotal($sub_total);
    $obj->setTotal($total);
    $obj->setPrecioTotalEnvio($precio_total_envio);
    $obj->setPrecioRealEnvio($precio_real_envio);
    $obj->setPrecioTarjeta($precio_tarjeta);
    $obj->setPrecioEnvioUnidad($precio_envio_unidad);
    $obj->setEmail($email);
    $obj->setDetalleCompra($detalleCompra);

    $resultado = $obj->agregar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}