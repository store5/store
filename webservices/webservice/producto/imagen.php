<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Imagen.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_producto = $_POST["id_producto"];

try {
    $upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/webservices/fotos/';

    for ($x = 0; $x < count($_FILES["files"]["name"]); $x++) {
        //print_r($_FILES['files']['name'][$x]);
        $userpic = $_FILES['files']['name'][$x];
        $tmp_dir = $_FILES['files']['tmp_name'][$x];
        $ext = pathinfo($userpic, PATHINFO_EXTENSION);

        move_uploaded_file($tmp_dir, $upload_dir .$id_producto.'-'. $userpic);

        $obj = new Imagen();
        $obj->setIdProducto($id_producto);
        $obj->setIdImagenes(substr($userpic, 0,strrpos($userpic, '.')));
        $obj->setUrl($userpic);
        $obj->agregar();
    }

    $obj->actualizar();

    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
