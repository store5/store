<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_producto = $_POST["id_producto"];

try {
    $obj = new Producto();
    $resultado = $obj->leerDatos($id_producto);
    $listacategoria = array();

    for ($i = 0; $i < count($resultado); $i++) {

        $listafotos = array();
        if ($resultado[$i]["foto"] === 'N'){
            $listafotos[0] = array("url" => 'no-disponible.svg');
        }else{
            $resultado2 = $obj->leerfotoportada($resultado[$i]["id_producto"]);
            for ($j = 0; $j < count($resultado2); $j++) {
                $listafotos[$j] = array("url" => $resultado2[$j]["url"]);
            }
        }

        $datos = array(
            "id_producto" => $resultado[$i]["id_producto"],
            "nombre" => $resultado[$i]["nombre"],
            "descripcion" => $resultado[$i]["descripcion"],
            "precio" => $resultado[$i]["precio"],
            "foto" => $resultado[$i]["foto"],
            "estado" => $resultado[$i]["estado"],
            "nombre_es" => $resultado[$i]["nombre_es"],
            "nombre_en" => $resultado[$i]["nombre_en"],
            "nombre_subcategoria" => $resultado[$i]["nombre_subcategoria"],
            "fotos_url" => $listafotos,
            "cantidad" => $resultado[$i]["cantidad"],
            "id_tipo" => $resultado[$i]["id_tipo"],
            "id_subcategoria" => $resultado[$i]["id_subcategoria"]
        );
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
