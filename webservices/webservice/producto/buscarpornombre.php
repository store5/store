<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';


$nombre = $_POST["nombre"];

try {
    $obj = new Producto();
    $resultado = $obj->buscarpornombre($nombre);
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_producto" => $resultado[$i]["id_producto"],
            "nombre" => $resultado[$i]["nombre"],
            "descripcion" => $resultado[$i]["descripcion"],
            "precio" => $resultado[$i]["precio"],
            "id_subcategoria" => $resultado[$i]["id_subcategoria"],
            "id_tipo" => $resultado[$i]["id_tipo"],
            "id_proveedor" => $resultado[$i]["id_proveedor"]
        );
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
