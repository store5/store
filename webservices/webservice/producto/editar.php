<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';


$id_producto = $_POST["id_producto"];
$nombre = $_POST["nombre"];
$precio = $_POST["precio"];
$descripcion = $_POST["descripcion"];
$estado = $_POST["estado"];

try {
    $obj = new Producto();
    $obj->setIdProducto($id_producto);
    $obj->setNombre($nombre);
    $obj->setPrecio($precio);
    $obj->setDescripcion($descripcion);
    $obj->setEstado($estado);
    $resultado = $obj->editar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Modificación Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}