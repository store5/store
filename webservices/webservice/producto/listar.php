<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

try {
    $obj = new Producto();
    $resultado = $obj->listar();
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_producto" => $resultado[$i]["id_producto"],
            "nombre_producto" => $resultado[$i]["nombre_producto"],
            "cantidad" => $resultado[$i]["cantidad"],
            "descripcion" => $resultado[$i]["descripcion"],
            "precio" => $resultado[$i]["precio"],
            "estado" => $resultado[$i]["estado"],
            "id_subcategoria" => $resultado[$i]["id_subcategoria"],
            "id_tipo" => $resultado[$i]["id_tipo"],
            "id_proveedor" => $resultado[$i]["id_proveedor"],
            "nombre_subcategoria" => $resultado[$i]["nombre_subcategoria"],
            "id_categoria" => $resultado[$i]["id_categoria"],
            "nombre_categoria" => $resultado[$i]["nombre_categoria"],
            "nombre_proveedor" => $resultado[$i]["nombre_proveedor"],
            "tipo_nombre_es" => $resultado[$i]["tipo_nombre_es"],
            "tipo_nombre_en" => $resultado[$i]["tipo_nombre_en"]
        );
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}