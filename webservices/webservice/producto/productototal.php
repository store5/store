<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$tipo = $_POST["tipo"];
$sub = $_POST["sub"];

try {
    $obj = new Producto();
    $resultado = $obj->productoTotal($tipo,$sub);
    $listacategoria = array();


        $datos = array(
            "cantidad" => $resultado["cantidad"]
        );
        $listacategoria = $datos;

    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
