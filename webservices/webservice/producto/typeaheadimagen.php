<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Producto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$nombre = $_POST["query"];

try {
    $obj = new Producto();
    $resultado = $obj->buscarfoto($nombre);
    for ($i = 0; $i < count($resultado); $i++) {
        $listacategoria[] = $resultado[$i];
    }

    echo json_encode($listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
