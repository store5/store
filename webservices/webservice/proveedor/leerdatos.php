<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Proveedor.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_proveedor = $_POST["id_proveedor"];

try {
    $obj = new Proveedor();
    $resultado = $obj->leerDatos($id_proveedor);
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array("id_proveedor" => $resultado[$i]["id_proveedor"], "nombre" => $resultado[$i]["nombre"], "url" => $resultado[$i]["url"]);
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
