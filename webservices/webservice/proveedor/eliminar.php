<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Proveedor.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';


$id_proveedor = $_POST["id_proveedor"];

try {
    $obj = new Proveedor();
    $obj->setIdproveedor($id_proveedor);
    $resultado = $obj->eliminar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Se Elimino Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
