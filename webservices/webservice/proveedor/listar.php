<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Proveedor.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/webservice/jwt/validar.php';

/*
if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];
*/

try {
    //if (validarToken($token)) {
        $obj = new Proveedor();
        $resultado = $obj->listar();
        $listaproveedor = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array("id_proveedor" => $resultado[$i]["id_proveedor"], "nombre" => $resultado[$i]["nombre"], "url" => $resultado[$i]["url"]);
            $listaproveedor[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaproveedor);
    /*
     } else {
        Funciones::imprimeJSON(500, "Token invalido", "");
    }
    */
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
