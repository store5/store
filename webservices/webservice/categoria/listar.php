<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Categoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$token = $_POST["token"];

try {
    $obj = new Categoria();
    $resultado = $obj->listar();
    $listacategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array("id_categoria" => $resultado[$i]["id_categoria"], "nombre" => $resultado[$i]["nombre"]);
        $listacategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacategoria);

} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
