<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Categoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$nombre = $_POST["nombre"];

try {
    $obj = new Categoria();
    $obj->setNombre($nombre);
    $resultado = $obj->agregar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {

    $mensajeError = $exc->getMessage();
    $posicion = strpos($mensajeError, "Raise exception:");
    if ($posicion > 0) {
        $mensajeError = substr($mensajeError, $posicion + 27, strlen($mensajeError));
    }

    Funciones::imprimeJSON(500, $mensajeError, "");
}

