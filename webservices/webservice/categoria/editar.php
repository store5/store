<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Categoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';


$id_categoria = $_POST["id_categoria"];
$nombre = $_POST["nombre"];

try {
    $obj = new Categoria();
    $obj->setId_categoria($id_categoria);
    $obj->setNombre($nombre);
    $resultado = $obj->editar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Modificación Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}