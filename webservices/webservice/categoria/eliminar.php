<?php

header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/negocio/Categoria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/webservices/util/funciones/Funciones.clase.php';

$id_categoria = $_POST["id_categoria"];

try {
    $obj = new Categoria();
    $obj->setId_categoria($id_categoria);
    $resultado = $obj->eliminar();
    if ($resultado) {
        Funciones::imprimeJSON(200, "Se Elimino Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }

} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
