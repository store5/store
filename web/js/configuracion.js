const DIRECCION_WS = "http://localhost:8080/webservices/webservice/";

function params() {
  var params = {};
  var param_array = window.location.href.split("?")[1].split("&");
  for (var i in param_array) {
    x = param_array[i].split("=");
    params[x[0]] = x[1];
  }
  return params;
}

function paramsPosition() {
  var arrayProductos = new Array();
  var param_array = window.location.href.split("?")[1].split("&");
  for (var i in param_array) {
    x = param_array[i].split("=");

    let objProductos = new Object();

    objProductos.params = x[0];
    objProductos.value = x[1];
    objProductos.position = i;

    arrayProductos.push(objProductos);
  }
  return arrayProductos;
}

$("#txtsearch").on("keydown", function(e) {
  if (e.which == 13) {
    e.preventDefault();

    location.href = `shop.html?page=1&search=${$("#txtsearch").val()}`;
  }
});

var amadoSearch = $(".search-nav");
var searchClose = $(".search-close");

amadoSearch.on("click", function() {
  $("#scrollUp").click();
  $("body").toggleClass("search-wrapper-on search-wrapper-on2");
});

searchClose.on("click", function() {
  $("body").removeClass("search-wrapper-on search-wrapper-on2");
});

function removeDuplicates(originalArray, prop) {
  var newArray = [];
  var lookupObject = {};

  for (var i in originalArray) {
    lookupObject[originalArray[i][prop]] = originalArray[i];
  }

  for (i in lookupObject) {
    newArray.push(lookupObject[i]);
  }
  return newArray;
}
