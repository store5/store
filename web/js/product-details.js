let producto;

jQuery(document).ready(function($) {
  if (params().id_producto != null && params().id_producto != "") {
    leerProducto();
  } else {
    location.href = "shop.html";
  }
});

var arrayProductos = new Array();
async function leerProducto() {
  await $.ajax({
    url: `${DIRECCION_WS}producto/leerdatos.php`,
    type: "POST",
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    data: { id_producto: params().id_producto },
    success: function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        datosJSON.datos.map(item => {
          let objProductos = new Object();

          objProductos.id_producto = item.id_producto;
          objProductos.nombre = item.nombre;
          objProductos.descripcion = item.descripcion;
          objProductos.precio = item.precio;
          objProductos.foto = item.foto;
          objProductos.nombre_es = item.nombre_es;
          objProductos.nombre_en = item.nombre_en;
          objProductos.nombre_subcategoria = item.nombre_subcategoria;
          objProductos.cantidad = item.cantidad;
          objProductos.id_tipo = item.id_tipo;
          objProductos.id_subcategoria = item.id_subcategoria;

          if (item.foto == "N") {
            objProductos.fotos_url = item.fotos_url[0].url;
          } else {
            var arrayFotos = new Array();

            item.fotos_url.map(data => {
              let objFotos = new Object();
              objFotos.url = data.url;
              arrayFotos.push(objFotos);
            });
            objProductos.fotos_url = arrayFotos;
          }

          arrayProductos.push(objProductos);
        });
      }
    }
  });
  producto = arrayProductos;
  descripcion();
  contacto();
  imagenes();
  relativeProducts();
}

function contacto() {
  let html = `
                    <li>
                      <a href="https://www.facebook.com/90sstore90/" target="_blank"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                      <a href="https://wa.me/51958529597?text=Hola!,%20Quiero%20adquirir%20listas%20producto.%20${location.href}"  target="_blank"><i class="fa fa-whatsapp"></i></a>
                    </li>
                    <li>
                      <a href="https://wa.me/51978438896?text=Hola!,%20Quiero%20adquirir%20listas%20producto.%20${location.href}" target="_blank"><i class="fa fa-whatsapp"></i></a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/_90sstore_/" target="_blank"><i class="fa fa-instagram"></i></a>
                    </li>
                    `;
  $("#contactos").html(html);
}

function descripcion() {
  let pre = producto[0].precio == 0 ? "" : `S/ ${producto[0].precio}`;
  $("#txtproductname").text(producto[0].nombre);
  $("#txtproductprice").text(pre);
  $("#txtcantidad").html(
    `<span>Cantidad :</span><ul><li><a>${producto[0].cantidad}</a></li></ul>`
  );
  $("#txtcategories").html(
    `<span>Categoria :</span><ul><li><a href="shop.html?page=1&tipo=${producto[0].id_tipo}">${producto[0].nombre_en}</a></li></ul>`
  );
  $("#txttipo").html(
    `<span>Tipo :</span><ul><li><a href="shop.html?page=1&sub=${producto[0].id_subcategoria}">${producto[0].nombre_subcategoria}</a></li></ul>`
  );
  $("#txtdescription").html(
    `<p class="text-center" style="width: 100%">
        ${producto[0].descripcion}
      </p>`
  );
}

function imagenes() {
  let html = "";
  let cantidad = 0;

  if (producto[0].foto === "N") {
    html += `<div class="dec-img-wrap" >
                <a
                  class="img-popup"
                  href=""
                  ><img
                    src="${DIRECCION_WS}../fotos/${producto[0].fotos_url}"
                    alt=""
                /></a>
              </div>  `;
  } else {
    cantidad = producto[0].fotos_url.length;
    producto[0].fotos_url.map(item => {
      html += `<div class="dec-img-wrap"  onclick=modal(${producto[0].id_producto},'${item.url}')>
                <a
                  data-target="#modalIMG" data-toggle="modal"
                  class="img-popup"
                  href=""
                  ><img
                    src="${DIRECCION_WS}../fotos/${producto[0].id_producto}-${item.url}"
                    alt=""
                /></a>
              </div>  `;
    });
  }

  $("#imagenes").html(html);

  $("#imagenes").owlCarousel({
    loop: cantidad > 2 ? true : false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 3000,
    nav: true,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true" style="position: absolute;top: 50%;"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true" style="position: absolute;top: 50%;"></i>'
    ],
    item: 10,
    margin: 30,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
        loop: true
      },
      768: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });
}

async function relativeProducts() {
  let html = "";
  let cantidad = 0;

  await $.ajax({
    url: `${DIRECCION_WS}producto/productosrelacionados.php`,
    type: "POST",
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    data: { id_producto: producto[0].id_producto },
    success: function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        cantidad = resultado.datos.length;
        datosJSON.datos.map(item => {
          let t = "";

          if (item.estado == "P") {
            t = '<span class="purple">En camino</span>';
          }
          if (item.estado == "N") {
            t = '<span class="purple">Nuevo</span>';
          }

          html += `<div class="product-wrap">
            <div class="product-img">
              <a href="product-details.html?id_producto=${item.id_producto}">`;
          if (item.foto === "N") {
            html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.fotos_url[0].url}"`;
          } else {
            if (item.fotos_url.length == 1) {
              html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[0]["url"]}" />`;
            } else {
              html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[0]["url"]}" />
                  <img class="hover-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[1]["url"]}" alt="" />`;
            }
          }

          html += `
              </a>
              ${t}
            </div>
            <div class="product-content text-center">
              <h3><a href="product-details.html?id_producto=${
                item.id_producto
              }">${item.nombre}</a></h3>
              <div class="product-rating">
                      <h5>${item.nombre_en}</h5>
                      <h5>${item.nombre_subcategoria}</h5>
              </div>
              <div class="product-price">
                <span> ${item.precio != 0 ? `S/ ${item.precio}` : ""}</span>
              </div>
            </div>
          </div>`;
        });
      }
    }
  });

  $("#relativeproducts").html(html);

  $("#relativeproducts").owlCarousel({
    loop: cantidad > 2 ? true : false,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 3000,
    nav: true,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true" style="position: absolute;top: 50%;"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true" style="position: absolute;top: 50%;"></i>'
    ],
    item: 10,
    margin: 30,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 2
      },
      768: {
        items: 2
      },
      992: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });
}

function modal(id_producto, url) {
  html = "";
  html += `<div
    aria-hidden="true"
    aria-labelledby="myModalLabel"
    class="modal fade"
    id="modalIMG"
    role="dialog"
    tabindex="-1"
  >
    <div class="modal-dialog" role="document" style="max-width: max-content !important;">
      <div class="modal-content">
        <div class="modal-body mb-0 p-0">
          <img
            src="${DIRECCION_WS}../fotos/${id_producto}-${url}"
            alt=""
            style="width:100%"
          />
        </div>
      </div>
    </div>
  </div>;`;

  $("#modal-imagen").html(html);
}
