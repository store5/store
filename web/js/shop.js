let page = 1,
  search = "",
  p_search = "",
  sub = "",
  p_sub = "",
  tipo = "",
  p_tipo = "",
  cant = 0;

jQuery(document).ready(function($) {
  let param = window.location.href.split("?")[1];

  if (param != null) {
    cant = paramsPosition().length;
    paramsPosition().map(data => {
      switch (data.params) {
        case "page":
          page = data.value;
          break;
        case "search":
          search = data.value;
          p_search = data.position;
          break;
        case "sub":
          sub = data.value;
          p_sub = data.position;
          break;
        case "tipo":
          tipo = data.value;
          p_tipo = data.position;
          break;
      }
    });
  }

  productos();
  pagination();
  listarSubCategoria();
});

var arrayProductos = new Array();

async function productos() {
  let url;

  search == ""
    ? (url = `${DIRECCION_WS}producto/listarweb.php`)
    : (url = `${DIRECCION_WS}producto/search.php`);

  await $.ajax({
    url,
    type: "POST",
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    data: {
      page,
      tipo,
      sub,
      search
    },
    success: function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        datosJSON.datos.map(item => {
          let objProductos = new Object();

          objProductos.id_producto = item.id_producto;
          objProductos.nombre = item.nombre;
          objProductos.descripcion = item.descripcion;
          objProductos.precio = item.precio;
          objProductos.foto = item.foto;
          objProductos.nombre_es = item.nombre_es;
          objProductos.nombre_en = item.nombre_en;
          objProductos.nombre_subcategoria = item.nombre_subcategoria;
          objProductos.cantidad = item.cantidad;
          objProductos.id_subcategoria = item.id_subcategoria;
          objProductos.id_tipo = item.id_tipo;
          objProductos.estado = item.estado;

          if (item.foto == "N") {
            objProductos.fotos_url = item.fotos_url[0].url;
          } else {
            var arrayFotos = new Array();

            item.fotos_url.map(data => {
              let objFotos = new Object();
              objFotos.url = data.url;
              arrayFotos.push(objFotos);
            });
            objProductos.fotos_url = arrayFotos;
          }

          arrayProductos.push(objProductos);
        });
        prodTotal = resultado.mensaje;
      }
    }
  });
  listar();
  listarTipos();
}

function listar() {
  let html = "";

  arrayProductos.map(item => {
    let t = "";

    if (item.estado == "P") {
      t = '<span class="purple">En camino</span>';
    }
    if (item.estado == "N") {
      t = '<span class="purple">Nuevo</span>';
    }

    html += `
            <div class="col-xl-4 col-md-6 col-lg-4 col-sm-6">
                  <div class="product-wrap mb-25 scroll-zoom">
                    <div class="product-img">
                      <a href="product-details.html?id_producto=${item.id_producto}">`;

    if (item.foto === "N") {
      html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.fotos_url}"`;
    } else {
      if (item.fotos_url.length == 1) {
        html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[0].url}" /> `;
      } else {
        html += `<img class="default-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[0].url}" /> `;
        html += `<img class="hover-img" src="${DIRECCION_WS}../fotos/${item.id_producto}-${item.fotos_url[1].url}" /> `;
      }
    }
    html += `     </a>
                    ${t}
                    </div>
                    <div class="product-content text-center">
                      <h3>
                        <a href="product-details.html?id_producto=${item.id_producto}">${item.nombre}</a>
                      </h3>
                      <div class="product-rating">
                      <h5>${item.nombre_en}</h5>
                        <h5>${item.nombre_subcategoria}</h5>
                      </div>
                      <div class="product-price">`;

    let pre = item.precio == 0 ? "" : `<span>S/ ${item.precio}</span>`;

    html += `     ${pre}
                      </div>
                    </div>
                  </div>
                </div>
          `;
  });

  $("#listado").html(html);
}

async function pagination() {
  let ruta, dato, showTotal;
  if (search != "") {
    ruta = `${DIRECCION_WS}producto/searchtotal.php`;
    datos = {
      search,
      tipo
    };
  } else {
    ruta = `${DIRECCION_WS}producto/productototal.php`;
    datos = {
      tipo,
      sub
    };
  }
  await $.ajax({
    url: ruta,
    type: "POST",
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    data: datos,
    success: function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        showTotal = datosJSON.datos.cantidad;

        $("#page-selection")
          .bootpag({
            total: Math.ceil(datosJSON.datos.cantidad / 60),
            maxVisible: 5,
            leaps: false,
            next: ">>",
            prev: "<<",
            page: page,

            wrapClass: "",
            activeClass: "active"
          })
          .on("page", function(event, num) {
            switch (cant) {
              case 0:
                location.href = `shop.html?page=${num}`;
                break;
              case 1:
                location.href = `shop.html?page=${num}`;
                break;
              case 2:
                if (p_search == 1) {
                  location.href = `shop.html?page=${num}&search=${search}`;
                } else if (p_search == 1) {
                  location.href = `shop.html?page=${num}&sub=${sub}`;
                } else {
                  location.href = `shop.html?page=${num}&tipo=${tipo}`;
                }
                break;
              case 3:
                if (p_search == 1) {
                  location.href = `shop.html?page=${num}&search=${search}&tipo=${
                    paramsPosition()[1].value
                  },${checkbox}`;
                } else {
                  location.href = `shop.html?page=${num}&sub=${sub}&tipo=${
                    paramsPosition()[1].value
                  },${checkbox}`;
                }
                break;
            }
          });
      }
    }
  });
  $("#txtresultados").text(
    `Showing ${(parseInt(page) - 1) * 60} of ${
      parseInt(page) * 60 > showTotal ? showTotal : parseInt(page) * 60
    } resultados ${showTotal}`
  );
}

function listarSubCategoria() {
  let ruta = DIRECCION_WS + "subcategoria/listar.php";

  $.post(ruta, function() {}).done(function(resultado) {
    let datosJSON = resultado;
    if (datosJSON.estado === 200) {
      let html = "";

      $.each(datosJSON.datos, function(i, item) {
        html += `<li><a href="shop.html?page=1&sub=${item.id_subcategoria}">${item.nombre_subcategoria}</a></li>`;
      });

      $("#lista-subcategoria").html(html);
    }
  });
}

async function listarTipos() {
  let ruta,
    data,
    datos,
    html = "";
  if (search != "") {
    ruta = DIRECCION_WS + "tipo/search.php";
    data = { page, search };
  } else {
    ruta = DIRECCION_WS + "tipo/listar.php";
    data = { page, sub };
  }

  await $.post(ruta, data, function() {}).done(function(resultado) {
    let datosJSON = resultado;
    if (datosJSON.estado === 200) {
      datos = resultado.datos;
    }
  });
  datos = removeDuplicates(datos, "nombre_en");
  datos.map((data, i) => {
    html += `<li class="flex-li" onclick="seleccionar(${data.id_tipo})"><input name="selector[]" type="checkbox" value="${data.id_tipo}" id="checkbox${data.id_tipo}" disabled/><a >${data.nombre_en}</a></li>`;
  });
  $("#txtSubs").html(html);

  tipo.split(",").map(data => {
    checkedUnchecked(data);
  });
}

function seleccionar(checkbox) {
  switch (cant) {
    case 0:
      location.href = `shop.html?page=1&tipo=${checkbox}`;
      break;
    case 1:
      location.href = `shop.html?page=1&tipo=${checkbox}`;
      break;
    case 2:
      if (p_search == 1) {
        location.href = `shop.html?page=1&search=${search}&tipo=${checkbox}`;
      } else if (p_sub == 1) {
        location.href = `shop.html?page=1&sub=${sub}&tipo=${checkbox}`;
      } else {
        if (tipo.split(",").length > 0) {
          if ($(`#checkbox${checkbox}`).is(":checked")) {
            let b = removeTipo(checkbox);

            b == ""
              ? (location.href = `shop.html?page=1`)
              : (location.href = `shop.html?page=1&tipo=${b}`);
          } else {
            location.href = `shop.html?page=1&tipo=${
              paramsPosition()[1].value
            },${checkbox}`;
          }
        } else {
          location.href = `shop.html?page=1&tipo=${checkbox}`;
        }
      }
      break;
    case 3:
      if (p_search == 1) {
        if (
          $(`#checkbox${checkbox}`).is(":checked") &&
          tipo.split(",").length > 0
        ) {
          let b = removeTipo(checkbox);
          b == ""
            ? (location.href = `shop.html?page=1&search=${search}`)
            : (location.href = `shop.html?page=1&search=${search}&tipo=${b}`);
        } else {
          location.href = `shop.html?page=1&search=${search}&tipo=${
            paramsPosition()[2].value
          },${checkbox}`;
        }
      } else {
        if ($(`#checkbox${checkbox}`).is(":checked")) {
          let b = removeTipo(checkbox);
          b == ""
            ? (location.href = `shop.html?page=1&sub=${sub}`)
            : (location.href = `shop.html?page=1&sub=${sub}&tipo=${b}`);
        } else {
          location.href = `shop.html?page=1&sub=${sub}&tipo=${
            paramsPosition()[2].value
          },${checkbox}`;
        }
      }
      break;
  }
}

function checkedUnchecked(checkbox) {
  if ($(`#checkbox${checkbox}`).is(":checked")) {
    $(`#checkbox${checkbox}`).prop("checked", false);
  } else {
    $(`#checkbox${checkbox}`).prop("checked", true);
  }
}

function removeTipo(checkbox) {
  return tipo
    .split(",")
    .filter(data => data != checkbox)
    .join(",");
}
