jQuery(document).ready(function($) {
  listar();
});

$("#btnAgregar").click(function(event) {
  $("#txttipooperacion").val("agregar");

  $("#modal_titulo").text("Agregar Proveedor");

  $("#modal_nombre").val("");
});

function listar() {
  let ruta = DIRECCION_WS + "proveedor/listar.php";

  $.post(ruta, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        html += `<table id="responsive-datatable" class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1"><thead>`;
        html += `<tr>`;
        html += `<th>ID</th>`;
        html += `<th>NOMBRE</th>`;
        html += `<th>URL</th>`;
        html += `<th>Actions</th>`;
        html += `</tr>`;
        html += `</thead>`;
        html += `<tbody>`;

        $.each(datosJSON.datos, function(i, item) {
          html += "<tr>";
          html += `<td>${item.id_proveedor}</td>`;
          html += `<td>${item.nombre}</td>`;
          html += `<td><a href="${item.url}" target="_blank">${item.url}</a></td>`;
          html += `<td>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" data-toggle="modal" data-target="#kt_modal_1" onclick="leerDatos(${item.id_proveedor})"><i class="la la-edit"></i></button>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="eliminar(${item.id_proveedor})"><i class="la la-close"></i></button>`;
          html += `</td>`;
          html += "</tr>";
        });

        html += "</tbody>";
        html += "</table>";

        $("#listado").html(html);

        $("#responsive-datatable").DataTable({ responsive: true });
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

$("#frmgrabar").submit(function(evento) {
  evento.preventDefault();

  if ($("#txttipooperacion").val() === "agregar") {
    let ruta = DIRECCION_WS + "proveedor/agregar.php";

    let nombre = $("#modal_nombre").val();
    let url = $("#modal_url").val();

    swal
      .fire({
        title: "¿Desea Registrar?",
        text: "se agregará un nuevo proveedor!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Registrar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(ruta, { nombre, url }, function() {}).done(function(
            resultado
          ) {
            let datosJSON = resultado;
            if (datosJSON.estado === 200) {
              swal.fire({
                title: "EXITO!",
                text: datosJSON.mensaje,
                type: "success"
              });

              $("#kt_modal_1").modal("hide");
              listar(); //refrescar los datos
            } else {
              swal.fire("Mensaje del sistema", resultado, "warning");
            }
          });
        }
      });
  } else {
    let ruta = DIRECCION_WS + "proveedor/editar.php";

    let id_proveedor = $("#modal_id_proveedor").val();
    let nombre = $("#modal_nombre").val();
    let url = $("#modal_url").val();

    swal
      .fire({
        title: "¿Desea Modificiar?",
        text: "se modificara el proveedor!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Modificar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(ruta, { id_proveedor, nombre, url }, function() {}).done(
            function(resultado) {
              let datosJSON = resultado;
              if (datosJSON.estado === 200) {
                swal.fire({
                  title: "EXITO!",
                  text: datosJSON.mensaje,
                  type: "success"
                });

                $("#kt_modal_1").modal("hide");
                listar(); //refrescar los datos
              } else {
                swal.fire("Mensaje del sistema", resultado, "warning");
              }
            }
          );
        }
      });
  }
});

function leerDatos(id_proveedor) {
  var ruta = DIRECCION_WS + "proveedor/leerdatos.php";

  $.post(ruta, { id_proveedor: id_proveedor }, function() {})
    .done(function(resultado) {
      var datosJSON = resultado;
      if (datosJSON.estado === 200) {
        $.each(datosJSON.datos, function(i, item) {
          $("#txtproveedoroperacion").val("editar");

          $("#modal_id_proveedor").val(item.id_proveedor);
          $("#modal_nombre").val(item.nombre);
          $("#modal_url").val(item.url);

          $("#modal_titulo").text("Editar proveedor");
        });
      } else {
        swal.fire("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      var datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

function eliminar(id_proveedor) {
  var ruta = DIRECCION_WS + "proveedor/eliminar.php";

  swal
    .fire({
      title: "¿Desea Eliminar?",
      text: "se eliminará el proveedor!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar"
    })
    .then(function(result) {
      if (result.value) {
        $.post(ruta, { id_proveedor }, function() {}).done(function(resultado) {
          let datosJSON = resultado;
          if (datosJSON.estado === 200) {
            swal.fire({
              title: "EXITO!",
              text: datosJSON.mensaje,
              type: "success"
            });

            listar(); //refrescar los datos
          } else {
            swal.fire("Mensaje del sistema", resultado, "warning");
          }
        });
      }
    });
}
