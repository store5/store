"use strict";

// Class definition
jQuery(document).ready(function($) {
  initUppy2();
  typeahead();
});

const XHRUpload = Uppy.XHRUpload;

// to get uppy companions working, please refer to the official documentation here: https://uppy.io/docs/companion/
const Dashboard = Uppy.Dashboard;

var initUppy2 = function() {
  var id = "#kt_uppy_2";

  var options = {
    proudlyDisplayPoweredByUppy: false,
    target: id,
    inline: true,
    replaceTargetContent: true,
    showProgressDetails: true,
    note: "Images and video only, 2–3 files, up to 1 MB",
    height: 470,
    metaFields: [
      { id: "name", name: "Name", placeholder: "file name" },
      { id: "id_producto", name: "ID-PRODUCTO", placeholder: "id_producto" }
    ],
    browserBackButtonClose: true
  };

  var uppyDashboard = Uppy.Core({
    autoProceed: false,
    restrictions: {
      maxFileSize: 2000000, // 2mb
      maxNumberOfFiles: 30,
      minNumberOfFiles: 1,
      allowedFileTypes: ["image/*", "video/*"]
    }
  });

  uppyDashboard.use(Dashboard, options);

  uppyDashboard.use(XHRUpload, {
    endpoint: `${DIRECCION_WS}producto/imagen.php`,
    method: "post"
  });
};

function typeahead() {
  $("#nombreproducto").typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 12,
      async: true,
      source: function(query, processSync, processAsync) {
        processSync();
        return $.ajax({
          url: `${DIRECCION_WS}producto/typeaheadimagen.php`,
          type: "POST",
          data: { query: query },
          dataType: "json",
          success: function(json) {
            let newData = [];
            $.each(json, function() {
              newData.push(
                `${this.id_producto} - ${this.nombre_producto} - ${this.id_imagenes} - ${this.nombre_proveedor}`
              );
            });
            return processAsync(newData);
          }
        });
      }
    }
  );
}
