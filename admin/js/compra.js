jQuery(document).ready(function($) {
  listarProveedor();
  listarSubcategoria();
  listarTipo();
  typeahead();
});

function typeahead() {
  $("#nombreproducto").typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      limit: 12,
      async: true,
      source: function(query, processSync, processAsync) {
        processSync();
        return $.ajax({
          url: `${DIRECCION_WS}producto/typeahead.php`,
          type: "POST",
          data: { query: query },
          dataType: "json",
          success: function(json) {
            let newData = [];
            $.each(json, function() {
              newData.push(this.nombre_producto);
            });
            return processAsync(newData);
          }
        });
      }
    }
  );
}

$("#nombreproducto").on("typeahead:selected", function(e, datum) {
  let ruta = DIRECCION_WS + "producto/buscarpornombre.php";

  $.post(ruta, { nombre: datum }, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        $.each(datosJSON.datos, function(i, item) {
          $("#txtidproducto").val(item.id_producto);
          $("#txtdescripcion").val(item.descripcion);
          $("#txtprecio").val(item.precio);
          $("#combosubcategoria")
            .select2()
            .val(item.id_subcategoria)
            .trigger("change");
          $("#comboproveedor")
            .select2()
            .val(item.id_proveedor)
            .trigger("change");
          $("#combotipo")
            .select2()
            .val(item.id_tipo)
            .trigger("change");
        });
      } else {
        swal.fire("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
});

function listarProveedor() {
  let ruta = DIRECCION_WS + "proveedor/listar.php";
  $.post(ruta, { token: TOKEN }, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        $.each(datosJSON.datos, function(i, item) {
          html += `<option value="${item.id_proveedor}">${item.nombre}</option>`;
        });

        $("#comboproveedor").html(html);

        $("#comboproveedor").select2();
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

function listarSubcategoria() {
  let ruta = DIRECCION_WS + "subcategoria/listarSubcategoriaCategoria.php";
  $.post(ruta, { token: TOKEN }, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        $.each(datosJSON.datos, function(i, item) {
          html += `<option value="${item.id_subcategoria}">${item.nombre_categoria} - ${item.nombre_subcategoria}</option>`;
        });

        $("#combosubcategoria").html(html);

        $("#combosubcategoria").select2();
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

function listarTipo() {
  let ruta = DIRECCION_WS + "tipo/listar.php";
  $.post(ruta, { token: TOKEN }, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        $.each(datosJSON.datos, function(i, item) {
          html += `<option value="${item.id_tipo}">${item.nombre_es} - ${item.nombre_en}</option>`;
        });

        $("#combotipo").html(html);

        $("#combotipo").select2();
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

document
  .getElementById("nombreproducto")
  .addEventListener("keydown", function(event) {
    if (event.keyCode == 8) {
      $("#txtidproducto").val("");
      // event.preventDefault();
    }
    if (event.keyCode == 46) {
      $("#txtidproducto").val("");
      // event.preventDefault();
    }
  });

var arrayTable = new Array();
var id_delete = 0;

$("#btnagregar").click(function() {
  id_delete++;
  let nombre = $("#nombreproducto").val();
  let cantidad = $("#txtcantidad").val();
  let precio_base = $("#txtpreciobase").val();
  let subtotal_producto = $("#txtsubtotal-producto").val();
  let envio_producto = $("#txtenvio-producto").val();
  let total_producto = $("#txttotal-producto").val();
  let precio_real = $("#txtprecioreal").val();
  let precio = $("#txtprecio").val();
  let id_subcategoria = $("#combosubcategoria").val();
  let id_tipo = $("#combotipo").val();
  let id_proveedor = $("#comboproveedor").val();
  let descripcion = $("#txtdescripcion").val();

  let objTabla = new Object(); //Crear un objeto para almacenar los datos

  /*declaramos y asignamos los valores a los atributos*/
  objTabla.id_delete = id_delete;
  objTabla.nombre = nombre;
  objTabla.cantidad = cantidad;
  objTabla.precio_base = precio_base;
  objTabla.subtotal_producto = subtotal_producto;
  objTabla.envio_producto = envio_producto;
  objTabla.total_producto = total_producto;
  objTabla.precio_real = precio_real;
  objTabla.precio = precio;
  objTabla.id_subcategoria = id_subcategoria;
  objTabla.id_tipo = id_tipo;
  objTabla.id_proveedor = id_proveedor;
  objTabla.descripcion = descripcion;
  /*declaramos y asignamos los valores a los atributos*/

  arrayTable.push(objTabla);

  mostrarTabla(arrayTable);
});

function mostrarTabla(arrayTable) {
  $("#tabla-detalle").empty();

  let html = "";

  html += `<table
    id="responsive-datatable"
    class="table table-striped- table-bordered table-hover table-checkable"
    cellspacing="0"
    width="100%"
  >
    <thead>
      <tr>
        <th>Nombe</th>
        <th>Cantidad</th>
        <th>Precio Base</th>
        <th>Sub Total</th>
        <th>Envio</th>
        <th>Total</th>
        <th>Precio Real</th>
        <th>Precio Venta</th>
        <th>Sub Categoria</th>
        <th>Tipo</th>
        <th>Proveedor</th>
        <th>Descripcion</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody id="detalleventa">`;

  arrayTable.map(data => {
    html += `
     <tr>
      <td>${data.nombre}</td>
      <td>${data.cantidad}</td>
      <td>${data.precio_base}</td>
      <td>${data.subtotal_producto}</td>
      <td>${data.envio_producto}</td>
      <td>${data.total_producto}</td>
      <td>${data.precio_real}</td>
      <td>${data.precio}</td>
      <td>${data.id_subcategoria}</td>
      <td>${data.id_tipo}</td>
      <td>${data.id_proveedor}</td>
      <td>${data.descripcion}</td>
      <td><button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="eliminar(${data.id_delete})"><i class="la la-close"></i></button></td>
    </tr>;
    `;
  });

  html += `</tbody>
  </table>`;

  $("#tabla-detalle").html(html);

  $("#responsive-datatable").DataTable({ responsive: true });

  return html;
}

function eliminar(id_delete) {
  deleteRow(arrayTable).remove("id_delete", id_delete);

  mostrarTabla(arrayTable);
}

function deleteRow(param) {
  var obj = {};

  obj.data = param;

  obj.remove = function(key, val) {
    var i = 0;
    while (this.data[i]) {
      if (this.data[i][key] === val) {
        this.data.splice(i, 1);
      } else {
        i++;
      }
    }
    return this;
  };

  return obj;
}

$("#txtlote").keyup(function() {
  this.value = this.value.replace(/[^0-9]/g, "");
});
$("#txtproductostotal").keyup(function() {
  this.value = this.value.replace(/[^0-9]/g, "");
});
$("#txtsubtotal-compra").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txttotal-compra").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txtpreciototalenvio").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txtpreciorealenvio").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txtpreciotarjeta").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txtprecioenvioporunidad").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");

  precioReal();
  subTotal();
  total();
});
$("#txtcantidad").keyup(function() {
  this.value = this.value.replace(/[^0-9]/g, "");

  precioReal();
  subTotal();
  total();
});
$("#txtpreciobase").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");

  precioReal();
  subTotal();
  total();
});
$("#txtprecio").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");
});
$("#txtenvio-producto").keyup(function() {
  this.value = this.value.replace(/[^0-9\.]/g, "");

  precioReal();
  subTotal();
  total();
});

var arrayDetalle = new Array();

document.getElementById("btnSubmit").onclick = () => {
  let ruta = DIRECCION_WS + "compra/agregar.php";

  let lote = $("#txtlote").val();
  let productos_total = $("#txtproductostotal").val();
  let sub_total = $("#txtsubtotal-compra").val();
  let total = $("#txttotal-compra").val();
  let precio_total_envio = $("#txtpreciototalenvio").val();
  let precio_real_envio = $("#txtpreciorealenvio").val();
  let precio_tarjeta = $("#txtpreciotarjeta").val();
  let precio_envio_unidad = $("#txtprecioenvioporunidad").val();
  let email = "silviopd01@gmail.com";

  arrayDetalle.splice(0, arrayDetalle.length);

  arrayTable.map(data => {
    let nombre = data.nombre;
    let cantidad = data.cantidad;
    let precio_base = data.precio_base;
    let sub_total = data.subtotal_producto;
    let envio = data.envio_producto;
    let total = data.total_producto;
    let precio_real = data.precio_real;
    let precio = data.precio;
    let id_subcategoria = data.id_subcategoria;
    let id_tipo = data.id_tipo;
    let id_proveedor = data.id_proveedor;
    let descripcion = data.descripcion;

    let objDetalle = new Object(); //Crear un objeto para almacenar los datos

    /*declaramos y asignamos los valores a los atributos*/
    objDetalle.nombre = nombre;
    objDetalle.cantidad = cantidad;
    objDetalle.precio_base = precio_base;
    objDetalle.sub_total = sub_total;
    objDetalle.envio = envio;
    objDetalle.total = total;
    objDetalle.precio_real = precio_real;
    objDetalle.precio = precio;
    objDetalle.id_subcategoria = id_subcategoria;
    objDetalle.id_tipo = id_tipo;
    objDetalle.id_proveedor = id_proveedor;
    objDetalle.descripcion = descripcion;
    /*declaramos y asignamos los valores a los atributos*/

    arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle
  });

  var jsonDetalle = JSON.stringify(arrayDetalle);

  swal
    .fire({
      title: "¿Desea Registrar?",
      text: "se agregará un nuevo tipo!",
      type: "info",
      showCancelButton: true,
      confirmButtonText: "Registrar",
      cancelButtonText: "Cancelar"
    })
    .then(function(result) {
      if (result.value) {
        $.post(
          ruta,
          {
            lote,
            productos_total,
            sub_total,
            total,
            precio_total_envio,
            precio_real_envio,
            precio_tarjeta,
            precio_envio_unidad,
            email,
            detalleCompra: jsonDetalle
          },
          function() {}
        ).done(function(resultado) {
          let datosJSON = resultado;
          if (datosJSON.estado === 200) {
            swal.fire({
              title: "EXITO!",
              text: datosJSON.mensaje,
              type: "success"
            });

            location.reload();
          } else {
            swal.fire("Mensaje del sistema", resultado, "warning");
          }
        });
      }
    });
};

function precioReal() {
  $("#txtprecioreal").val(
    (
      parseFloat($("#txtpreciobase").val()) +
      parseFloat($("#txtprecioenvioporunidad").val())
    ).toFixed(2)
  );
}

function subTotal() {
  $("#txtsubtotal-producto").val(
    (
      parseFloat($("#txtpreciobase").val()) *
      parseFloat($("#txtcantidad").val())
    ).toFixed(2)
  );
}

function total() {
  $("#txttotal-producto").val(
    (
      parseFloat($("#txtsubtotal-producto").val()) +
      parseFloat($("#txtenvio-producto").val())
    ).toFixed(2)
  );
}
