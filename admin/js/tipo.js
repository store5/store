jQuery(document).ready(function($) {
  listar();
});

$("#btnAgregar").click(function(event) {
  $("#txttipooperacion").val("agregar");

  $("#modal_titulo").text("Agregar Tipo");

  $("#modal_nombre").val("");
});

function listar() {
  let ruta = DIRECCION_WS + "tipo/listar.php";

  $.post(ruta, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        html += `<table id="responsive-datatable" class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1"><thead>`;
        html += `<tr>`;
        html += `<th>ID</th>`;
        html += `<th>NOMBRE ES</th>`;
        html += `<th>NOMBRE EN</th>`;
        html += `<th>Actions</th>`;
        html += `</tr>`;
        html += `</thead>`;
        html += `<tbody>`;

        $.each(datosJSON.datos, function(i, item) {
          html += "<tr>";
          html += `<td>${item.id_tipo}</td>`;
          html += `<td>${item.nombre_es}</td>`;
          html += `<td>${item.nombre_en}</td>`;
          html += `<td>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" data-toggle="modal" data-target="#kt_modal_1" onclick="leerDatos(${item.id_tipo})"><i class="la la-edit"></i></button>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="eliminar(${item.id_tipo})"><i class="la la-close"></i></button>`;
          html += `</td>`;
          html += "</tr>";
        });

        html += "</tbody>";
        html += "</table>";

        $("#listado").html(html);

        $("#responsive-datatable").DataTable({ responsive: true });
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

$("#frmgrabar").submit(function(evento) {
  evento.preventDefault();

  if ($("#txttipooperacion").val() === "agregar") {
    let ruta = DIRECCION_WS + "tipo/agregar.php";

    let nombre_es = $("#modal_nombre_es").val();
    let nombre_en = $("#modal_nombre_en").val();

    swal
      .fire({
        title: "¿Desea Registrar?",
        text: "se agregará un nuevo tipo!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Registrar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(ruta, { nombre_es, nombre_en }, function() {}).done(function(
            resultado
          ) {
            let datosJSON = resultado;
            if (datosJSON.estado === 200) {
              swal.fire({
                title: "EXITO!",
                text: datosJSON.mensaje,
                type: "success"
              });

              $("#kt_modal_1").modal("hide");
              listar(); //refrescar los datos
            } else {
              swal.fire("Mensaje del sistema", resultado, "warning");
            }
          });
        }
      });
  } else {
    let ruta = DIRECCION_WS + "tipo/editar.php";

    let id_tipo = $("#modal_id_tipo").val();
    let nombre_es = $("#modal_nombre_es").val();
    let nombre_en = $("#modal_nombre_en").val();

    swal
      .fire({
        title: "¿Desea Modificiar?",
        text: "se modificara el tipo!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Modificar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(ruta, { id_tipo, nombre_es, nombre_en }, function() {}).done(
            function(resultado) {
              let datosJSON = resultado;
              if (datosJSON.estado === 200) {
                swal.fire({
                  title: "EXITO!",
                  text: datosJSON.mensaje,
                  type: "success"
                });

                $("#kt_modal_1").modal("hide");
                listar(); //refrescar los datos
              } else {
                swal.fire("Mensaje del sistema", resultado, "warning");
              }
            }
          );
        }
      });
  }
});

function leerDatos(id_tipo) {
  var ruta = DIRECCION_WS + "tipo/leerdatos.php";

  $.post(ruta, { id_tipo: id_tipo }, function() {})
    .done(function(resultado) {
      var datosJSON = resultado;
      if (datosJSON.estado === 200) {
        $.each(datosJSON.datos, function(i, item) {
          $("#txttipooperacion").val("editar");

          $("#modal_id_tipo").val(item.id_tipo);
          $("#modal_nombre_es").val(item.nombre_es);
          $("#modal_nombre_en").val(item.nombre_en);

          $("#modal_titulo").text("Editar tipo");
        });
      } else {
        swal.fire("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      var datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

function eliminar(id_tipo) {
  var ruta = DIRECCION_WS + "tipo/eliminar.php";

  swal
    .fire({
      title: "¿Desea Eliminar?",
      text: "se eliminará el tipo!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar"
    })
    .then(function(result) {
      if (result.value) {
        $.post(ruta, { id_tipo }, function() {}).done(function(resultado) {
          let datosJSON = resultado;
          if (datosJSON.estado === 200) {
            swal.fire({
              title: "EXITO!",
              text: datosJSON.mensaje,
              type: "success"
            });

            listar(); //refrescar los datos
          } else {
            swal.fire("Mensaje del sistema", resultado, "warning");
          }
        });
      }
    });
}
