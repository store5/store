jQuery(document).ready(function($) {
  listar();
  listarcategoria();
});

function listarcategoria() {
  let ruta = DIRECCION_WS + "categoria/listar.php";
  $.post(ruta, { token: TOKEN }, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        $.each(datosJSON.datos, function(i, item) {
          html += `<option value="${item.id_categoria}">${item.nombre}</option>`;
        });

        $("#combocategoria").html(html);

        $("#combocategoria").select2();
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

$("#btnAgregar").click(function(event) {
  $("#txttipooperacion").val("agregar");

  $("#modal_titulo").text("Agregar Subcategoria");

  $("#modal_nombre").val("");
});

function listar() {
  let ruta = DIRECCION_WS + "subcategoria/listar.php";

  $.post(ruta, function() {})
    .done(function(resultado) {
      let datosJSON = resultado;
      if (datosJSON.estado === 200) {
        let html = "";

        html += `<table id="responsive-datatable" class="table table-striped- table-bordered table-hover table-checkable"><thead>`;
        html += `<tr>`;
        html += `<th>ID</th>`;
        html += `<th>NOMBRE</th>`;
        html += `<th>CATEGOIA</th>`;
        html += `<th>Actions</th>`;
        html += `</tr>`;
        html += `</thead>`;
        html += `<tbody>`;

        $.each(datosJSON.datos, function(i, item) {
          html += "<tr>";
          html += `<td>${item.id_subcategoria}</td>`;
          html += `<td>${item.nombre_subcategoria}</td>`;
          html += `<td>${item.nombre_categoria}</td>`;
          html += `<td>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" data-toggle="modal" data-target="#kt_modal_1" onclick="leerDatos(${item.id_subcategoria})"><i class="la la-edit"></i></button>`;
          html += `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="eliminar(${item.id_subcategoria})"><i class="la la-close"></i></button>`;
          html += `</td>`;
          html += "</tr>";
        });

        html += "</tbody>";
        html += "</table>";

        $("#listado").html(html);

        $("#responsive-datatable").DataTable({ responsive: true });
      } else {
        swal("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      let datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

$("#frmgrabar").submit(function(evento) {
  evento.preventDefault();

  if ($("#txttipooperacion").val() === "agregar") {
    let ruta = DIRECCION_WS + "subcategoria/agregar.php";

    let nombre = $("#modal_nombre").val();
    let id_categoria = $("#combocategoria").val();

    swal
      .fire({
        title: "¿Desea Registrar?",
        text: "se agregará una nueva subcategoria!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Registrar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(ruta, { nombre, id_categoria }, function() {}).done(function(
            resultado
          ) {
            let datosJSON = resultado;
            if (datosJSON.estado === 200) {
              swal.fire({
                title: "EXITO!",
                text: datosJSON.mensaje,
                type: "success"
              });

              $("#kt_modal_1").modal("hide");
              listar(); //refrescar los datos
            } else {
              swal.fire("Mensaje del sistema", resultado, "warning");
            }
          });
        }
      });
  } else {
    let ruta = DIRECCION_WS + "subcategoria/editar.php";
    let id_subcategoria = $("#modal_id_subcategoria").val();
    let id_categoria = $("#combocategoria").val();
    let nombre = $("#modal_nombre").val();

    swal
      .fire({
        title: "¿Desea Modificiar?",
        text: "se modificara la categoria!",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Modificar",
        cancelButtonText: "Cancelar"
      })
      .then(function(result) {
        if (result.value) {
          $.post(
            ruta,
            { id_subcategoria, nombre, id_categoria },
            function() {}
          ).done(function(resultado) {
            let datosJSON = resultado;
            if (datosJSON.estado === 200) {
              swal.fire({
                title: "EXITO!",
                text: datosJSON.mensaje,
                type: "success"
              });

              $("#kt_modal_1").modal("hide");
              listar(); //refrescar los datos
            } else {
              swal.fire("Mensaje del sistema", resultado, "warning");
            }
          });
        }
      });
  }
});

function leerDatos(id_subcategoria) {
  var ruta = DIRECCION_WS + "subcategoria/leerdatos.php";

  $.post(ruta, { id_subcategoria }, function() {})
    .done(function(resultado) {
      var datosJSON = resultado;
      if (datosJSON.estado === 200) {
        $.each(datosJSON.datos, function(i, item) {
          $("#txttipooperacion").val("editar");

          $("#modal_id_subcategoria").val(item.id_subcategoria);
          $("#modal_nombre").val(item.nombre);
          $("#combocategoria")
            .select2()
            .val(item.id_categoria)
            .trigger("change");

          $("#modal_titulo").text("Editar Categoria");
        });
      } else {
        swal.fire("Mensaje del sistema", resultado, "warning");
      }
    })
    .fail(function(error) {
      var datosJSON = $.parseJSON(error.responseText);
      swal("Error", datosJSON.mensaje, "error");
    });
}

function eliminar(id_subcategoria) {
  var ruta = DIRECCION_WS + "subcategoria/eliminar.php";

  swal
    .fire({
      title: "¿Desea Eliminar?",
      text: "se eliminará la subcategoria!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar"
    })
    .then(function(result) {
      if (result.value) {
        $.post(ruta, { id_subcategoria }, function() {}).done(function(
          resultado
        ) {
          let datosJSON = resultado;
          if (datosJSON.estado === 200) {
            swal.fire({
              title: "EXITO!",
              text: datosJSON.mensaje,
              type: "success"
            });

            listar(); //refrescar los datos
          } else {
            swal.fire("Mensaje del sistema", resultado, "warning");
          }
        });
      }
    });
}
